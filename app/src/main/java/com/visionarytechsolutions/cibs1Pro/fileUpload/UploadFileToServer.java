package com.visionarytechsolutions.cibs1Pro.fileUpload;

import java.io.File;
import java.security.KeyStore;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.visionarytechsolutions.cibs1Pro.fileUpload.AndroidMultiPartEntity.ProgressListener;
import com.visionarytechsolutions.cibs1Pro.service.MouthService;
import com.visionarytechsolutions.cibs1Pro.service.ShakeService;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;
import com.visionarytechsolutions.cibs1Pro.utils.NotificationAlertActions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

public class UploadFileToServer extends AsyncTask<Void, Integer, String> {

	String ServerUrl;
	String[] filePaths;
	File selfie,record,info,videoSelfie;
	float totalSize;
	Context context;
	SharedPreferences share;
	boolean success = false, hasRecordFile = true;
   
    public UploadFileToServer(String serverUrl, String[] filePaths,Context context) {
		super();
		this.ServerUrl = serverUrl;
		this.filePaths = filePaths;
		this.context=context;
		if(filePaths[1] == "")  hasRecordFile = false;
		share = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		Log.e("videoo12333","path"+share.getString("snapshotVideoPath", ""));
	     Log.e("imagepath123333","path"+share.getString("snapshotImagePath", ""));		
	     
	     
	}    
	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        
        Log.e("FIRST A","path"+MySharedPreferences.getInstance().getData(context, MySharedPreferences.IMAGE));
		Log.e("FIRST B","path"+MySharedPreferences.getInstance().getData(context, MySharedPreferences.VIDEO));
    }

    @Override
    protected String doInBackground(Void... params) {    	
        return uploadFile();
    }

    public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new CustomSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
   }
    
    private String uploadFile() {   	
    	Log.d("fileSent", "...Started...");
    	success = false;
        String responseString = null;
        try {
        	HttpClient httpclient = getNewHttpClient();
		    final HttpPost httppost = new HttpPost(ServerUrl);

		    //MultipartEntity entity = new MultipartEntity();
		    if(!filePaths[0].equals(""))
		    selfie = new File(filePaths[0]);
		    if(hasRecordFile)
		    	record = new File(filePaths[1]);
		    
              videoSelfie=new File(filePaths[3]);

		    info = new File(filePaths[2]);
		    Log.e("selfiee","hjkjk"+filePaths[0]);
		    Log.e("video","hjkjk"+filePaths[3]);

		    
		    Log.e("info",filePaths[2]);
		        
		    if(hasRecordFile)
		    	totalSize =  record.length() + info.length();
		    else
		    	totalSize =  info.length();
		    
			if(!filePaths[3].equals("")) 	
				totalSize=totalSize+videoSelfie.length();

		    
		    Log.e("totalsizee",""+totalSize);
		    
		    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new ProgressListener() {
                        @Override
                        public void transferred(long num) {
                        	if(isCancelled())
                    			httppost.abort();
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });
              if(!filePaths[0].equals(""))
              {
		    entity.addPart("selfie", new FileBody(selfie));
              }              
		   if(hasRecordFile)
		   {
		    	entity.addPart("record", new FileBody(record));
		   }

		   if(!filePaths[3].equals("")) 	
		   entity.addPart("video", new FileBody(videoSelfie));		    	
		   entity.addPart("info", new FileBody(info));
	    	    //entity.addPart("video", new FileBody(videoSelfie));
		    totalSize = entity.getContentLength();
		    httppost.setEntity(entity);
		    
		    if (android.os.Build.VERSION.SDK_INT > 9) {
		        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		        StrictMode.setThreadPolicy(policy);
		    }

		    // Making server call
		    HttpResponse response = httpclient.execute(httppost);
		    Log.e("fileSent", "...Started...8 "+response);
		    HttpEntity r_entity = response.getEntity();            
		    int statusCode = response.getStatusLine().getStatusCode();
		    
		    if (statusCode == 200) {
		        responseString = EntityUtils.toString(r_entity);
		        success = true;
		    } else {
		        responseString = "Error occurred! Http Status Code: "
		                + statusCode;
		    }
	    } catch (Exception e) {
	        responseString = e.toString();
	        Log.d("fileSent", "...Started...9"+responseString);
	    }	
		    return responseString;
    }
    @Override
    protected void onProgressUpdate(Integer... progress) {   	
    	Log.e("progress", progress[0]+"");      
    }
    
    @Override
    protected void onPostExecute(String result) {
    	Log.e("fileSentResult", result);
    	
    	//share.getString("switchStatus", "false").equals("true")
    	
	if(share.getBoolean("isShakeChecked", false)){

		Intent svc1 =new Intent(context, ShakeService.class);
		context.startService(svc1);
	}
       	
     if(share.getString("switchStatus", "false").equals("true")){
        	
     	 Intent svc1 =new Intent(context, MouthService.class);
     	     context.startService(svc1);
        }
    	
    
    	if(success)
    		NotificationAlertActions.instance.handleUrlResult(result);
        super.onPostExecute(result);
    }
}


