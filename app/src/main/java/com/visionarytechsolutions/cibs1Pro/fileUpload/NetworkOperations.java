package com.visionarytechsolutions.cibs1Pro.fileUpload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

/**
 * Created by krish on 27-10-2015.
 */
public class NetworkOperations extends AsyncTask<String, Void, String> {
        String resultData="",url = "",callingMtd = "";
        Context _ctx;
        List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();

        public NetworkOperations(String url,List<Pair<String, String>> parameters, Context _ctx, String callingMtd) {
                super();
                this.parameters  = parameters;
                this._ctx = _ctx;
                this.url = url;
                this.callingMtd = callingMtd;
        }

        @Override
        protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("Nop_Result", resultData);
            
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            resultData = sendPost(url,parameters).toString();
            Log.e("grttt","gg"+resultData);
            return "";
        }

    @SuppressWarnings("finally")
	public static String sendPost(String _url,List<Pair<String, String>> parameters)  {
        StringBuilder params=new StringBuilder("");
        String result="";
        try {
        	int i = 0;
            for(Pair<String, String> pair:parameters){
            	if(i == 0) {params.append(pair.first+"=");i++;}
      		  	else params.append("&"+pair.first+"=");

                params.append(pair.second);
            }


            String url =_url;
            URL obj = new URL(_url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            con.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();

            int responseCode = con.getResponseCode();
            Log.e("urll",url);
            Log.e("parameters",""+params);
            Log.e("Response",""+responseCode);
            

            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + params);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            Log.e("responsee",""+response);

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            result = response.toString();
            Log.e("resukltt","jj"+result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            return  result;
        }

    }
}











