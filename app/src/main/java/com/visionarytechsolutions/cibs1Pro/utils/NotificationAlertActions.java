package com.visionarytechsolutions.cibs1Pro.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.visionarytechsolutions.cibs1Pro.FakeCallActivity;
import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.fileUpload.UploadFileToServer;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ServerUtilities;
import com.visionarytechsolutions.cibs1Pro.service.MouthService;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class NotificationAlertActions {

	Context context;
	SmsManager smsManager;
	JSONObject contactsJSON = new JSONObject();
	ArrayList<String> contactsNumbersList = new ArrayList<String>();
	ArrayList<String> contactsEmailsList = new ArrayList<String>();
	SharedPreferences sharedPreferences;
	Editor editor;
	Location currentLocation;
	String imagePath,videoPath,shake,threshold,call,text,email;
	LocationActions locationActions;
	JSONObject locationJSON,infoLocationJSON;//mailText = ""
	String msgText = "",msgText2 = "", personalMessage = "", infoText = "" , firstContact="", firstContactNumber="", audioFilePath="",
		   uploadedUrl="", locationString = "", currentLocationString = "--- Not Available ---", lastLocationString = "--- Not Available ---",url="";
	BroadcastReceiver sentReceiver, deliveredReceiver;
	boolean callPhone = false;
	Date notCheckedAt;
	File dataFile = null;
	SimpleDateFormat formatDate;
	String formattedDate;
	String dayOfTheWeek;   
	String emailUser="";
	SharedPreferences share;
	public static NotificationAlertActions instance;
	AsyncTask<Void, Void, Void> sendEmail;
	public NotificationAlertActions(Context context) {
		this.context = context;
		
		smsManager = SmsManager.getDefault();
		locationActions = new LocationActions(context);
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();      
          firstContact = sharedPreferences.getString("firstContact", "");
          instance = this;
	}
	public void sendNotifications(boolean callPhone,String filePath,Context context,String imagePath,String videoPath,String threshold,String shake,String call,String text,String email){		
		audioFilePath = filePath;
		getContactsNumbers();
		this.callPhone = callPhone;
		this.context=context;
		this.imagePath=imagePath;
		this.videoPath=videoPath;
		this.threshold=threshold;
		this.shake=shake;
		this.call=call;
		this.text=text;
		this.email=email;
		//Toast.makeText(context, "call "+call, Toast.LENGTH_SHORT).show();
		//Toast.makeText(context, "text "+text, Toast.LENGTH_SHORT).show();
		//Toast.makeText(context, "email "+email, Toast.LENGTH_SHORT).show();
		generateMsgText();
	}
	public void generateMsgText(){
		personalMessage = sharedPreferences.getString("personalMessage", "");
		
		 formatDate = new SimpleDateFormat("hh:mm aaa");		     
	      formattedDate = formatDate.format(new Date()).toString();			     
	     formattedDate=formattedDate.replace("p.m.", "PM").replace("a.m.", "AM");
	     Log.e("timeee",formattedDate);
	     SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
	     Date d = new Date();
	      dayOfTheWeek = sdf.format(d);
	     Log.e("dayy",dayOfTheWeek.substring(0, 3));
		
		String lastLocation = "", lastLocation1 = "", name = "", lastCheckedTime = "", currentTime = "";		
		Location tempLocation = new Location("dummy");
		infoLocationJSON = new JSONObject();
		try {
			locationJSON = new JSONObject(sharedPreferences.getString("lastCheckedLocationJSON", "{}"));
			infoLocationJSON.put("lastCheckinLocation", locationJSON);
			if(locationJSON.length()>0){
				tempLocation.setLatitude(Double.parseDouble(locationJSON.getString("lat")));
				tempLocation.setLongitude(Double.parseDouble(locationJSON.getString("lon")));
				lastLocation = "Last Checkin Location:\nhttp://maps.google.com/maps?daddr="+locationJSON.getString("lat")+","+locationJSON.getString("lon");
				try {
					lastLocationString = new AddressGeocoderTask().execute(tempLocation).get();
					lastLocation1 = "last checked-in location is "+lastLocationString;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
		}
		
		name = sharedPreferences.getString("firstName", "") +" "+sharedPreferences.getString("lastName", "");
		lastCheckedTime = sharedPreferences.getString("lastCheckedTime", "");
		currentTime = new Date(System.currentTimeMillis()).toString();
		
		currentLocation = locationActions.getCurrentLocation();
		locationActions.cancelListener();
		
		msgText = "CIBS Alert!" +"\n\n"+name+" has not checked in at "+currentTime;
		msgText2 = "CIBS Alert!"+" "+name+" has not checked in at "+currentTime;
		
//		mailText = personalMessage+"\n\n"+name+" has not checked in at "+currentTime;
		
		infoText = "";
		//infoText = name+" has not checked in at "+currentTime;
		//infoText = name+" has not checked in at "+currentTime;
		 Log.e("shake",MySharedPreferences.getInstance().getData(context, "shake"));
		 Log.e("threshold",MySharedPreferences.getInstance().getData(context, "threshold"));	
		 
		//Toast.makeText(context, "SHAKE "+MySharedPreferences.getInstance().getData(context, "shake"), Toast.LENGTH_SHORT).show();
		//Toast.makeText(context, "threshold "+MySharedPreferences.getInstance().getData(context, "threshold"), Toast.LENGTH_SHORT).show();

		if(shake.equals("true"))
		{							
		infoText = "CIBS Shake Alert Detected "+name+" has Missed a Check In at "+formattedDate+" today.";
		}
		else if(threshold.equals("true"))
		{
		infoText = "CIBS Loud Noise Detected "+name+" has Missed a Check In at "+formattedDate+" today.";	
		}
		else
		{		
		infoText = name+" has Missed a Check In at "+formattedDate+" today.";
		}
		if(currentLocation != null){
			//msgText = msgText + "\n\nCurrent Location:\nhttp://maps.google.com/maps?daddr="+currentLocation.getLatitude()+","+currentLocation.getLongitude();
//			mailText = mailText + " His/Her current Location is http://maps.google.com/maps?daddr="+currentLocation.getLatitude()+","+currentLocation.getLongitude();
			try {
				JSONObject temp = new JSONObject();
				temp.put("lat", currentLocation.getLatitude());
				temp.put("lon", currentLocation.getLongitude());
				infoLocationJSON.put("currentLocation", temp);
				currentLocationString = new AddressGeocoderTask().execute(currentLocation).get();
				//infoText = infoText + " His/Her current Location is "+currentLocationString;
				infoText = infoText + " Missed Check In was at "+currentLocationString +"<p class='mapLink'><p>";

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(lastCheckedTime.equalsIgnoreCase(""))
			lastCheckedTime = "*** Not Yet Checked ***";
//		mailText = mailText + "\n\n He/She lastly checked-in at "+lastCheckedTime;
		//infoText = infoText + "\n\n He/She lastly checked-in at "+lastCheckedTime;
		//infoText = infoText + "\n\n He/She lastly checked-in at "+lastCheckedTime;
		infoText = infoText + "\n\n Last check In was at "+lastCheckedTime;

		if(!lastLocation.equalsIgnoreCase("")){
			//msgText = msgText + "\n\n" + lastLocation;
//			mailText = mailText + " and his/her "+lastLocation1;
			//infoText = infoText + " and his/her "+lastLocation1;
			infoText = infoText + " "+lastLocation1+"<p class='lastLocLink'><p>";

		}		
//		mailText = mailText + "\n\nPlease contact "+name+" as soon as possible to ensure he/she is not in any danger before contacting authorities unless you are certain he/she is in danger";
		
		infoText = infoText + "\n\nPlease contact "+name+" as soon as possible.";	
			
		//infoText = infoText + "\n\nPlease contact "+name+" as soon as possible to ensure he/she is not in any danger before contacting authorities unless you are certain he/she is in danger";		
		writeDataToFile();		
	}
	public void writeDataToFile(){
		String fileName = "info.txt";
		JSONObject infoJSON = new JSONObject();
		try {
			
			Log.e("personal message",personalMessage);
			
			Log.e("info",infoText);
			Log.e("locations",""+infoLocationJSON);
			Log.e("current locations",currentLocationString);
			
			Log.e("last locations",lastLocationString);
			
			infoJSON.put("personalMessage", personalMessage);
			infoJSON.put("info", infoText);
			infoJSON.put("locations", infoLocationJSON);
			infoJSON.put("currentLocation", currentLocationString);
			infoJSON.put("lastLocation", lastLocationString);
		} catch (JSONException e) {
			Log.e("JSON", e.toString());
		}
		File myDirectory = new File(Environment.getExternalStorageDirectory(), "CIBS");
		if(!myDirectory.exists()) {                                 
		  myDirectory.mkdirs();
		}		
        File dir = new File(myDirectory,"Records");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        dataFile = new File(dir, fileName);        
		try {
	        dataFile.createNewFile();
		    FileOutputStream fos = new FileOutputStream(dataFile);
		    fos.write(infoJSON.toString().getBytes());
		    fos.close();
		} catch (IOException e) {
			Log.e("file", e.toString());
		}
		//sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          //editor = sharedPreferences.edit();      
		
		// upload files to server - Start	
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();   
          
		String url = "http://www.checkinbesafe.com/webservices/fileupload.php";
		//String imagePath = sharedPreferences.getString("snapshotImagePath", "");
		//Log.e("audiopathss",audioFilePath);
	        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(context);

		//Log.e("first","path"+myPref.getString("snapshotImage", ""));
		//Log.e("second","path"+myPref.getString("snapshotVideo", ""));
		
		Log.e("frist111","path"+imagePath);

		
	     Log.e("second111","path"+videoPath);
	     
	     //Log.e("pp","path"+sharedPreferences.getString("detailsfinals111", ""));
	    
		String[] filePaths = {imagePath,audioFilePath,dataFile.getAbsolutePath(),videoPath};
		
		if(networkAvailable()){						
			UploadFileToServer uploadFileToServer = new UploadFileToServer(url, filePaths,context);
			uploadFileToServer.execute();
			//handleUrlResult("http://www.checkinbesafe.com/webservices/UsersFiles/nu561Gofp");
			//handleResult("http://www.checkinbesafe.com/webservices/UsersFiles/nu561Gofp");

		}else{
			msgText = msgText + "\n\n Sorry he has no internet connection to share details with you";
			//if(sharedPreferences.getBoolean("isTextChecked", false) && callPhone){
			if(text.equals("true") && callPhone){

				if(contactsNumbersList.size()>=1)
				{
					 MediaPlayer mPlayer2;
		     		 mPlayer2= MediaPlayer.create(context, R.raw.beep);
		     		 mPlayer2.start();
					 sendSMS();
				}
			}
			//Toast.makeText(context, "one"+sharedPreferences.getBoolean("isCallChecked", false), Toast.LENGTH_SHORT).show();

			//if(sharedPreferences.getBoolean("isCallChecked", false) && callPhone)
			if(call.equals("true"))
				callPhone();
		}
		
		// upload files to server - End
		
//		if(sharedPreferences.getBoolean("isTextChecked", false) && callPhone)
//			sendSMS();
//		if(sharedPreferences.getBoolean("isEmailChecked", false) && callPhone){
//			if(networkAvailable())
//				sendEmail();		
//		}
//		if(sharedPreferences.getBoolean("isCallChecked", false) && callPhone)
//			callPhone();

	}
	String address="https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyCtS18_qhu2MDBZeG9r3PsDwxZ5pVMclrc";
	//String address="https://www.googleapis.com/urlshortener/v1/url";

  	String longUrl;
	public void handleUrlResult(String result){
		longUrl = result;
//		new URLShort().execute();
		handleResult(longUrl);
	}
	
	public void handleResult(String result){
		msgText = msgText + "\n\nFor more detials please visit the below link\n\n"+result; 
		// = msgText2+" More details click on the link:\n\nLink: "+result;
		msgText2 =msgText2 + ". Get more details from below link\n\n"+result;
		//Toast.makeText(instance.context, result, Toast.LENGTH_LONG).show();
		Log.e("resulttt",result);
		url=result;
		//if(sharedPreferences.getBoolean("isTextChecked", false) && callPhone){
		if(text.equals("true") && callPhone){

			if(contactsNumbersList.size()>=1)
			{
				sendSMS();
				 MediaPlayer mPlayer2;
	     		 mPlayer2= MediaPlayer.create(context, R.raw.beep);
	     		 mPlayer2.start();
               	Log.e("text send","send");

			}
		}
		//if(sharedPreferences.getBoolean("isEmailChecked", false) && callPhone){
		if(email.equals("true") && callPhone){

			if(networkAvailable() && contactsEmailsList.size()>=1)
				try{					
			     for(int i=0;i<contactsEmailsList.size();i++)		
				emailUser=emailUser+contactsEmailsList.get(i)+",";
			     emailUser= emailUser.substring(0, emailUser.length() - 1);		
			     Log.e("emailuser",emailUser);
			     sendEmail = new AsyncTask<Void, Void, Void>() {              	
                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                    // On server creates a new user
     			     msgText2="CIBS Alert Missed Check In "+formattedDate+" "+dayOfTheWeek.substring(0, 3)+ " "+currentLocationString +"<br>"+url;
                    	Log.e("emailText",msgText2);
						sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
						String userEmail = sharedPreferences.getString(GlobalClass.KEY_LOGGED_IN_USER_EMAIL,"");

						Log.e("loggedInuseremail","user email"+userEmail+"  contactsss"+emailUser);
//						Toast.makeText(context,userEmail,Toast.LENGTH_SHORT).show();
          			    ServerUtilities.sendEmail(context, emailUser, msgText2,userEmail);
                        return null;
                    }
                    @Override
                    protected void onPostExecute(Void result) {
                    	sendEmail = null;
                    	Toast.makeText(context, "Email sent successfully", Toast.LENGTH_SHORT).show();
                    }
                };
                sendEmail.execute(null, null, null);
				}
			catch(Exception e)
			{
			e.printStackTrace();	
			}
			
		}
		//Toast.makeText(context, "22"+sharedPreferences.getBoolean("isCallChecked", false), Toast.LENGTH_SHORT).show();
		//if(sharedPreferences.getBoolean("isCallChecked", false) && callPhone)
		if(call.equals("true"))
			callPhone();
	}
	
	public void sendSMS(){			
		for (String PhoneNumber : contactsNumbersList) {
			try{
				
				String SENT = "sent"+PhoneNumber;
			    String DELIVERED = "delivered"+PhoneNumber;
			    final Intent sentIntent = new Intent(SENT);
			    sentIntent.putExtra("phoneNumber", PhoneNumber);
			    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent,PendingIntent.FLAG_CANCEL_CURRENT);
			    
			    final Intent deliveryIntent = new Intent(DELIVERED);
			    deliveryIntent.putExtra("phoneNumber", PhoneNumber);
			    PendingIntent deliverPI = PendingIntent.getBroadcast(context, 0, deliveryIntent,PendingIntent.FLAG_CANCEL_CURRENT);
			    
			    sentReceiver = new BroadcastReceiver() {
			       @Override
			       public void onReceive(Context context, Intent intent) {
				        String result = "";
				        context.getApplicationContext().unregisterReceiver(this);
				        
				        
				        
				   	   /*Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
				        if (alarmUri == null) {
				            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
				        }
				        Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
				        ringtone.play();*/
				        switch (getResultCode()) {
				        	case Activity.RESULT_OK:  result = "Message Sent successfully to "+intent.getStringExtra("phoneNumber");
				        	  				   				        	
				        	/*try
				        	  {
				        	  int resID=context.getResources().getIdentifier("message", "raw", context.getPackageName());
					        MediaPlayer mediaPlayer = MediaPlayer.create(context,resID);
					        mediaPlayer.start();	
				        	  }
				        	  catch(Exception e)
				        	  {
				        		  
				        	  }			*/	        	
				        	break;				          						  	  
				          	default: result = "Message sending failed to "+intent.getStringExtra("phoneNumber");	
				          	
				          	
				          	//Intent svc1 =new Intent(context, MouthService.class);
		               	     //context.stopService(svc1);
		               	     
				          	
				          	
				          	/*MediaPlayer mPlayer2;
				          	
				          	mPlayer2=getMediaPlayer(context);
				          	
				     		mPlayer2= MediaPlayer.create(context, R.raw.beep);
				     		 mPlayer2.start();*/
				     		 
				     		/* if(sharedPreferences.getString("switchStatus", "false").equals("true")){
				          	        	
				               	     context.startService(svc1);
				                  }		*/
				          	
				          	/*try {
				 			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				 			    Ringtone r = RingtoneManager.getRingtone(NotificationAlertActions.this.context, notification);
				 			    r.play();
				 			} catch (Exception e) {
				 			    e.printStackTrace();
				 			}*/
						  	  		 break;
				        }
				        Toast.makeText(context, result,Toast.LENGTH_LONG).show();
				        try {
						playSound(context);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				       /* try {
		 			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		 			    Ringtone r = RingtoneManager.getRingtone(NotificationAlertActions.this.context, notification);
		 			    r.play();
		 			} catch (Exception e) {
		 			    e.printStackTrace();
		 			}*/
				        /*MediaPlayer mPlayer2;
			     		mPlayer2= MediaPlayer.create(context, R.raw.beep);
			     		 mPlayer2.start();*/
				        Log.e("msg", result);
			       }
				};				      
				deliveredReceiver = new BroadcastReceiver() {
			       @Override
			       public void onReceive(Context context, Intent intent) {
			    	       context.getApplicationContext().unregisterReceiver(this);	
			    	     /*MediaPlayer mPlayer2;
		     		mPlayer2= MediaPlayer.create(context, R.raw.beep);
		     		 mPlayer2.start();*/
			    		
				       Toast.makeText(context, "Message delivered to "+intent.getStringExtra("phoneNumber"),Toast.LENGTH_LONG).show();
				       Log.d("msg", "Message Deliverd to "+intent.getStringExtra("phoneNumber"));
			       }
	      		};
	      		
	      		/*try {
					    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					    Ringtone r = RingtoneManager.getRingtone(NotificationAlertActions.this.context, notification);
					    r.play();
					} catch (Exception e) {
					    e.printStackTrace();
					}*/
			    context.getApplicationContext().registerReceiver(sentReceiver, new IntentFilter(SENT));
			     /* Register for Delivery event */
			     context.getApplicationContext().registerReceiver(deliveredReceiver, new IntentFilter(DELIVERED));
//				 smsManager.sendTextMessage(PhoneNumber, null, msgText2, sentPI, deliverPI);
			     Log.d("msg", "Message sending to "+PhoneNumber);
			    
			     
			     msgText2="CIBS Alert Missed Check In "+formattedDate+" "+dayOfTheWeek.substring(0, 3)+ " "+currentLocationString +"\n"+url;
			     
			     /*msgText2="CIBS Alert Missed Check In 4:30 PM Mon
                       115 Columbus Dr. Belleville, IL 62226
                       (Link to web page with more info goes here)"
*/			     ArrayList<String> parts =smsManager.divideMessage(msgText2);
			     int numParts = parts.size();
               	Log.e("messagetext",msgText2);

			     ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
			     ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

			     for (int i = 0; i < numParts; i++) {
				     sentIntents.add(sentPI);
				     deliveryIntents.add(deliverPI);
			     }
			     smsManager.sendMultipartTextMessage(PhoneNumber,null, parts, sentIntents, deliveryIntents);
			}catch(Exception e){
				Toast.makeText(context, "Message sending fialed to "+PhoneNumber, Toast.LENGTH_LONG).show();
				Log.d("msg", "Message sending fialed to "+PhoneNumber);
			}
		}	
	}
	public void sendEmail(){
		SendMailTask sendEmail = new SendMailTask();
		sendEmail.execute();
	}
	
	public void callPhone(){
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		if(firstContactNumber.length()>=10){
	        callIntent.setData(Uri.parse("tel:"+Uri.encode(firstContactNumber)));
	        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try{
		         context.getApplicationContext().startActivity(callIntent);
		    }catch (android.content.ActivityNotFoundException ex){
		    }
		}
	}
	
	public void getContactsNumbers(){
		try {
			contactsJSON = new JSONObject(sharedPreferences.getString("contactsJSON", "{}"));
			Log.v("ContactsJson", contactsJSON.toString());
			@SuppressWarnings("rawtypes")
			Iterator it = contactsJSON.keys();
			while(it.hasNext()){
				String contactName = it.next().toString();
				JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactName));
				if(contactJSON.getString("phone").length()>=7){
					if(firstContact.equalsIgnoreCase(contactName))
						firstContactNumber = contactJSON.getString("phone");
					contactsNumbersList.add(contactJSON.getString("phone"));
				}
				if(contactJSON.getString("email").length()>=7)
					contactsEmailsList.add(contactJSON.getString("email"));
				
			}
		} catch (JSONException e) {
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
			Log.v("getContacts", e.toString());
		}
	}
	
	private class SendMailTask extends AsyncTask<Mail, Void, Void> {	 
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	    }
	 
	    @Override
	    protected void onPostExecute(Void aVoid) {
	        super.onPostExecute(aVoid);
	    }
	 
	    @Override
	    protected Void doInBackground(Mail... messages) {
	        try {
	        	String[] toList = new String[contactsEmailsList.size()];
	        	for(int i = 0; i < contactsEmailsList.size() ; i++){
	        	    toList[i] = contactsEmailsList.get(i);
	        	}
	        	Log.e("Email", "Email sending1"); 
	        	//Mail sender = new Mail("service@checkinbesafe.com", "Service@mail",toList,"service@checkinbesafe.com","CIBS Notification Alert!",msgText,context);

	        	Mail sender = new Mail("service@checkinbesafe.com", "Service@mail",toList,"service@checkinbesafe.com","CIBS Notification Alert!",msgText2,context);
//	        	String imagePath = sharedPreferences.getString("snapshotImagePath", "");
//	        	sender.addAttachment(imagePath);
//	        	sender.addAttachment(audioFilePath);
	        	sender.send();
	        } catch(Exception e) { 
                Log.e("CIBS", "Could not send email", e); 
            }
	        return null;
	    }
	}	
	private boolean networkAvailable(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
	}
	
	
	private class AddressGeocoderTask extends AsyncTask<Location, Void, String>{
		 
        @Override
        protected String doInBackground(Location... params) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(context);
            List<Address> addresses = null; 
            try {
                addresses = geocoder.getFromLocation(params[0].getLatitude(), params[0].getLongitude(), 1);
                if(addresses==null || addresses.size()==0){
                     return "*** Location Not Found ***";
                }
                Address address = (Address) addresses.get(0);
                if(address.getAddressLine(0) != null)
                	locationString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		locationString = locationString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		locationString = locationString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		locationString = locationString + "," + address.getAddressLine(3);
            	if(!locationString.contains(address.getCountryName()))
            		locationString = locationString + "," + address.getCountryName();              	
            } catch (IOException e) {
                Log.d("Exception", e.toString());
            }
            return locationString;
        }
	}	
	private class URLShort extends AsyncTask<String, String, JSONObject> {   	
   	@Override
       protected JSONObject doInBackground(String... args) {
   		URLShortener jParser = new URLShortener(); 		
   		JSONObject json = jParser.getJSONFromUrl(address,longUrl);
   		return json;
   	}
   	 @Override
        protected void onPostExecute(JSONObject json) {
   		 try {
   				if (json != null){
   				String ShortUrl = json.getString("id");
   				handleResult(ShortUrl);
   				}
   			
   		} catch (JSONException e) {
   			e.printStackTrace();
   		}

   		 
   	 }
   	 
   	
   	
   }
	
	public void playSound(Context context) throws IllegalArgumentException, 
     SecurityException, 
     IllegalStateException,
     IOException {

Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
MediaPlayer mMediaPlayer = new MediaPlayer();
mMediaPlayer.setDataSource(context, soundUri);
final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
//mMediaPlayer.setLooping(true);
mMediaPlayer.prepare();
mMediaPlayer.start();
}}
}
