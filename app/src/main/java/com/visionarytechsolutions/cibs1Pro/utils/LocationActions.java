package com.visionarytechsolutions.cibs1Pro.utils;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class LocationActions implements LocationListener {

	LocationManager locationManager;
	Location currentLocation,safeLocation;
	Context context;
	JSONObject safeLocationJSON = new JSONObject();
	JSONObject safeLocationRadiusJSON = new JSONObject();
	SharedPreferences sharedPreferences;
	boolean isUserInRange = false, locationUpdated = false;
	double safeDistance = 0;
	
	
	public LocationActions(Context context) {
		this.context = context;
		safeLocation = new Location("dummyprovider");
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
	}
	
	public void cancelListener(){
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		locationManager.removeUpdates(this);
	}

	public Location getCurrentLocation(){
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(currentLocation == null){
        	try{
	        	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	        	currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);  
        	}catch(Exception e){
        		Log.d("Exception", e.toString());
        	}
        }
        return currentLocation;
	}
	
	public boolean isUserInRange(){	
		try {
			safeLocationJSON = new JSONObject(sharedPreferences.getString("safeLocationJSON", "{}"));
			safeLocationRadiusJSON = new JSONObject(sharedPreferences.getString("safeLocationRadius", "{}"));
			safeDistance = safeLocationRadiusJSON.getInt("value");
		} catch (JSONException e) {
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
		}		
		getCurrentLocation();
		@SuppressWarnings("rawtypes")
		Iterator it = safeLocationJSON.keys();
		if(currentLocation != null){
			 while(it.hasNext()){
		     	try {
		    		String key = it.next().toString();
					JSONObject locationJSON = (JSONObject) safeLocationJSON.get(key);
					safeLocation.setLatitude(Double.parseDouble(locationJSON.getString("lat")));
					safeLocation.setLongitude(Double.parseDouble(locationJSON.getString("lon")));
					Log.e("distancee",""+currentLocation.distanceTo(safeLocation));
					Log.e("safeDistance",""+safeDistance);

					if(currentLocation.distanceTo(safeLocation) <= safeDistance){
						isUserInRange = true;
						break;
					}
					
				} catch (JSONException e) {
					Log.d("Exception", e.toString());
				}        	
		     }
		}
		cancelListener();
		return isUserInRange;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		currentLocation = location;	
		locationUpdated = true;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

}
