package com.visionarytechsolutions.cibs1Pro.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by brst-pc93 on 11/24/16.
 */

public class MySharedPreferences {
	
	
	public static final String IMAGE="image";
	public static final String VIDEO="video";
	
    private static MySharedPreferences instance = null;

    public final String PreferenceName = "dokani_pref";

    public static MySharedPreferences getInstance() {
        if (instance == null) {
            instance = new MySharedPreferences();
        }
        return instance;
    }


    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
    }



   
    public void storeData(Context context,String key,String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();
        
	    Log.e("storedata","storedata");


    }


    public void clearData(Context context,String key) {
        SharedPreferences preferences = context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove(key).apply();

    }
    public String getData(Context context,String key) {
	    
	    Log.e("getdata","getdata");

        return getPreference(context).getString(key, "null");
    }


    }

