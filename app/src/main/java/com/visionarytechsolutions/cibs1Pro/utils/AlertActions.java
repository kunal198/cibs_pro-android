package com.visionarytechsolutions.cibs1Pro.utils;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import com.visionarytechsolutions.cibs1Pro.receiver.FakeCallReceiver;
import com.visionarytechsolutions.cibs1Pro.service.BackgroundVideoRecorder;
import com.visionarytechsolutions.cibs1Pro.service.ShakeService;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class AlertActions {
	
	SharedPreferences sharedPreferences;
	NotificationAlertActions notificationActions;
	Editor editor;	
	Context context;
	JSONObject timerJSON;
	Intent intent;
	LocationActions locationActions;
	boolean userInRange = false;
	
	public int NOTIFICATION_ALERT_TIME = 885;	
	
	public AlertActions(Context context) {
		this.context = context;
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();		
        locationActions = new LocationActions(context.getApplicationContext());
	}

	public void activateFakeCallAlert(boolean disp){
			int time = 0;
			try {
				timerJSON  = new JSONObject(sharedPreferences.getString("checkinTimer", "{}"));
				time = timerJSON.getInt("value");
				setFakeCallAlert(disp, time-15);
			} catch (JSONException e) {
				Log.d("Exc", e.toString());
			}
	}
	
	@SuppressLint("NewApi") public void setFakeCallAlert(boolean disp,int time){
		boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
		
		boolean isShakeChecked = sharedPreferences.getBoolean("isShakeChecked", false);
		
		if(isShakeChecked){		
			if(!sharedPreferences.getBoolean("isShakeServiceStarted", false)){
				context.startService(new Intent(context, ShakeService.class));
				editor.putBoolean("isShakeServiceStarted", true);
				editor.commit();
			}
		}else{
			if(sharedPreferences.getBoolean("isShakeServiceStarted", false)){
				context.stopService(new Intent(context, ShakeService.class));
				editor.putBoolean("isShakeServiceStarted", false);
				editor.commit();
			}
		}		
		if(!alertSet){
			editor.putBoolean("alertSet", true);
			editor.commit();
			Intent fakeCallIntent = new Intent(context, FakeCallReceiver.class);
			fakeCallIntent.setAction("ManualActivation");
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, fakeCallIntent, 0);
			AlarmManager alaramManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Calendar calendar = Calendar.getInstance();
			Log.e("timee",time+"");
		     //calendar.add(Calendar.SECOND, time);
			if(sharedPreferences.getBoolean("minute",false))
				calendar.add(Calendar.SECOND, time);
			else
				calendar.add(Calendar.HOUR,24);
			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
				alaramManager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
			else
				alaramManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), time*1000, pendingIntent);
			if(disp)	Toast.makeText(context, "CIBS activated", Toast.LENGTH_LONG).show();
		}
		else{
			deactivateFakeCallAlert(false);
			setFakeCallAlert(disp, time);
		}
	}
	public void deactivateFakeCallAlert(boolean disp){
		boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
		//Toast.makeText(context, ""+alertSet, Toast.LENGTH_SHORT).show();
		//alertSet=true;
		if(alertSet){
			editor.putBoolean("alertSet", false);
			editor.putBoolean("isFirst", true);
			editor.putBoolean("isPlaySet", false);
			editor.commit();
			Intent fakeCallIntent = new Intent(context, FakeCallReceiver.class);
			fakeCallIntent.setAction("ManualActivation");
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, fakeCallIntent, 0);
			AlarmManager alaramManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alaramManager.cancel(pendingIntent);
			if(disp)	Toast.makeText(context, "CIBS Deactivated", Toast.LENGTH_LONG).show();
		}
	}
	
	@SuppressLint("NewApi") public void activateNotificationAlert(boolean disp){
		boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);		
		if(!notificationAlertSet){
			if(sharedPreferences.getBoolean("isLeaveSafeLocationChecked", false)){
				  userInRange = locationActions.isUserInRange();
			}				
			if(!userInRange){
				editor.putBoolean("notificationAlertSet", true);
				editor.commit();
				
	//			notificationActions = new NotificationAlertActions(context);
	//			notificationActions.sendNotifications(true);
				
				
				sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);

		          Log.e("videoo","path"+sharedPreferences.getString("snapshotVideoPath", ""));
			     Log.e("imagepath","path"+sharedPreferences.getString("snapshotImagePath", ""));

				//MySharedPreferences.getInstance().storeData(getActivity(), "shake", "false");
				//MySharedPreferences.getInstance().storeData(getActivity(), "threshold", "false");
			     
			     Log.e("calll","s"+sharedPreferences.getBoolean("isTextChecked", false));
			     Log.e("emaill","s1"+sharedPreferences.getBoolean("isCallChecked", false));
			     Log.e("text","s2"+sharedPreferences.getBoolean("isEmailChecked", false));
			     //Toast.makeText(context, "s1"+sharedPreferences.getBoolean("isTextChecked", false), Toast.LENGTH_SHORT);
			    // Toast.makeText(context, "s2"+sharedPreferences.getBoolean("isCallChecked", false), Toast.LENGTH_SHORT);
			    // Toast.makeText(context, "s3"+sharedPreferences.getBoolean("isEmailChecked", false), Toast.LENGTH_SHORT);

			     /*editor.putBoolean("isEmailChecked", chkEmail.isChecked());
				editor.putBoolean("isTextChecked", chkText.isChecked());
				editor.putBoolean("isCallChecked", chkCall.isChecked());*/
				
				   Intent intent = new Intent(context.getApplicationContext(), BackgroundVideoRecorder.class);
			        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			        intent.putExtra("callPhone", true);
			        intent.putExtra("snapshotImagePath", sharedPreferences.getString("snapshotImagePath", ""));
			        intent.putExtra("snapshotVideoPath", sharedPreferences.getString("snapshotVideoPath", ""));
			        intent.putExtra("threshold", MySharedPreferences.getInstance().getData(context, "threshold"));
			        intent.putExtra("shake", MySharedPreferences.getInstance().getData(context, "shake"));
			        
			        intent.putExtra("call", ""+sharedPreferences.getBoolean("isCallChecked", false));			   
			        intent.putExtra("text", ""+sharedPreferences.getBoolean("isTextChecked", false));
			        intent.putExtra("email", ""+sharedPreferences.getBoolean("isEmailChecked", false));
			        			    
			        context.startService(intent);	
				/*Thread thread = new Thread() {
					    @Override
					    public void run() {
					        try {
					     	   
					            }
					         catch (Exception e) {
					            e.printStackTrace();
					        }
					    }
					};

					thread.start();*/
				
				/*new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						//Toast.makeText(getApplicationContext(),"CIBS Alert Activated",Toast.LENGTH_LONG).show();
		 	       	     
						   
						   return null;
					}
				}.execute();
				*/
				 /*final Thread t = new Thread() {
				        @Override
				        public void run() {
				            try {
				          	  //Toast.makeText(getApplicationContext(),"CIBS Alert Activated",Toast.LENGTH_LONG).show();
				          				          	  
				            } finally {

				            }
				        }
				    };
				    t.start();*/
				
				
//				Intent notificationAlertIntent = new Intent(context, NotificationAlertReceiver.class);
//				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationAlertIntent, 0);
//				AlarmManager alaramManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//				Calendar calendar = Calendar.getInstance();
//			    calendar.add(Calendar.SECOND, NOTIFICATION_ALERT_TIME);
//				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
//					alaramManager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
//				else
//					alaramManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), NOTIFICATION_ALERT_TIME * 1000, pendingIntent);
		        sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		        editor = sharedPreferences.edit();
				if(sharedPreferences.getBoolean("isNotificationFirst", true) && !(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)){
					editor.putBoolean("isNotificationFirst", false);
					editor.putBoolean("isFirst", true);
					editor.commit();
				}
		        if(disp) Toast.makeText(context, "CIBS Notification Alert Activated", Toast.LENGTH_LONG).show();
			}
			setFakeCallAlert(false, NOTIFICATION_ALERT_TIME);
		}
		else{
			deactivateNotificationAlert(false);
			activateNotificationAlert(false);
		}
	}	
	public void deactivateNotificationAlert(boolean disp){
		boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
		//notificationAlertSet=true;
		if(notificationAlertSet){
			editor.putBoolean("notificationAlertSet", false);
			editor.putBoolean("isNotificationFirst", true);
			editor.commit();
//			Intent notificationAlertIntent = new Intent(context, NotificationAlertReceiver.class);
//			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationAlertIntent, 0);
//			AlarmManager alaramManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//			alaramManager.cancel(pendingIntent);
			if(disp)	Toast.makeText(context, "CIBS Notification Alert Deactivated", Toast.LENGTH_LONG).show();
		}
	}
}
