package com.visionarytechsolutions.cibs1Pro.utils;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
 
 
public class Mail extends javax.mail.Authenticator { 
  private String _user; 
  private String _pass; 
 
  private String[] _to; 
  private String _from; 
 
  private String _port; 
  private String _host; 
 
  private String _subject; 
  private String _body; 
 
  private Multipart _multipart;  
 
  MimeMessage msg;
  
  Context _ctx;
  
  Handler handler;
  
  Session session;
  
  int sendTimes;
 
  public Mail(String user, String pass, String[] to, String from,
		String subject, String body, Context ctx) {
	this();
	_user = user;
	_pass = pass;
	_to = to;
	_from = from;
	_subject = subject;
	_body = body;
	_ctx = ctx;
	handler =  new Handler(_ctx.getMainLooper());
}

public Mail() { 
    //_host = "mail.checkinbesafe.com"; // default smtp server 
     _host = "smtp.gmail.com"; // default smtp server 
	_port = "587"; // default smtp port 
    _user = "jatinder2.syall@gmail.com"; // username 
    _pass = "enbake_jatinder1506"; // password 
    _from = ""; // email sent from 
    _subject = ""; // email subject 
    _body = ""; // email body 
 
    _multipart = new MimeMultipart();     
 
    // There is something wrong with MailCap, javamail can not find a handler for the multipart/mixed part, so this bit needs to be added. 
    MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap(); 
    mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html"); 
    mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml"); 
    mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain"); 
    mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed"); 
    mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822"); 
    CommandMap.setDefaultCommandMap(mc); 
  } 
 
  public Mail(String user, String pass) { 
    this(); 
 
    _user = user; 
    _pass = pass; 
  } 
 
  public boolean send(){ 
    Properties props = _setProperties(); 
    sendTimes = 0;
    if(!_user.equals("") && !_pass.equals("") && !_to.equals("") && !_from.equals("") && !_subject.equals("") && !_body.equals("")) { 
      session = Session.getInstance(props, this); 
      
      try{
      msg = new MimeMessage(session); 
     
      msg.setFrom(new InternetAddress(_from)); 
     
      InternetAddress[] addressTo = new InternetAddress[_to.length]; 
      for (int i = 0; i < _to.length; i++) { 
        addressTo[i] = new InternetAddress(_to[i]); 
      } 
      msg.setRecipients(MimeMessage.RecipientType.TO, addressTo);  
        
      msg.setSubject(_subject); 
      
      msg.setSentDate(new Date()); 
      // setup message body 
      BodyPart messageBodyPart = new MimeBodyPart(); 
      messageBodyPart.setText(_body); 
      _multipart.addBodyPart(messageBodyPart); 
 
      // Put parts in message 
      msg.setContent(_multipart); 
     
      // send email 
      
      Connection c=new Connection();
      c.execute("");
      
     
       
      }catch (Exception e) {
  	}
      return true;
    } else { 
      return false; 
    } 
    
  } 
 
  public void addAttachment(String filename) throws Exception { 
    BodyPart messageBodyPart = new MimeBodyPart(); 
    DataSource source = new FileDataSource(filename); 
    messageBodyPart.setDataHandler(new DataHandler(source)); 
    File f = new File(filename);
    messageBodyPart.setFileName(f.getName()); 
 
    _multipart.addBodyPart(messageBodyPart); 
  } 
 
  private class Connection extends AsyncTask<Object, Object, Object> {
	    @Override

	    protected Object doInBackground(Object... arg0) {
	    	sendTimes++;
	    	Transport transport = null;
	    	 try {
	    		Log.e("Email", "Email sending2"); 
//				Transport.send(msg);
	    		transport = session.getTransport("smtp");
	    		transport.connect(_host, _user, _pass);
	    		transport.sendMessage(msg, msg.getAllRecipients());
	    		transport.close();
			    handler.post( new Runnable(){
			        public void run(){
			            Toast.makeText(_ctx, "Mail Has Been sent Successfully",Toast.LENGTH_LONG).show();
			            Log.e("Email", "Mail Has Been sent Successfully"); 
			        }
			    });
	    	 }catch (Exception e) {
	    		 Log.d("dd", e.toString());	
	    		 if(!this.isCancelled()){
	    			 this.cancel(true);
	    		 }
	    		 if(sendTimes <=1){
	    			 Connection c=new Connection();
	    		     c.execute("");
	    		 }
	    	 }finally{
	    		 if(transport != null){
	    			 try {
						transport.close();
					} catch (MessagingException e) {
						Log.d("dd1", e.toString());
					}
	    			 
	    		 }
	    	 }
	    	 
	        return null;
	    }
	}
  
  
  
  @Override 
  public PasswordAuthentication getPasswordAuthentication() { 
    return new PasswordAuthentication(_user, _pass); 
  } 
 
  private Properties _setProperties() { 
	  Properties props = new Properties();
	  props.put("mail.smtp.user", _user);
	  props.put("mail.smtp.host", _host);
	  props.put("mail.smtp.port", _port);
	  props.put("mail.smtp.starttls.enable","true");
	  props.put("mail.transport.protocol", "smtp");
	  props.put("mail.smtp.debug", "true");
	  props.put("mail.smtp.auth", "true");
	  props.put("mail.smtp.socketFactory.port", "25");
	  props.put("mail.smtp.socketFactory.fallback", "false"); 
      return props; 
  } 
 
  // the getters and setters 
  public String getBody() { 
    return _body; 
  } 
 
  public void setBody(String _body) { 
    this._body = _body; 
  } 
 
  // more of the getters and setters �.. 
} 
