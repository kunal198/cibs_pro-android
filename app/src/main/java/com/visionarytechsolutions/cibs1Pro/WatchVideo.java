package com.visionarytechsolutions.cibs1Pro;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
public class WatchVideo extends Activity {
	VideoView videoView;
	Button btnNext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watch_video);
		videoView=(VideoView)findViewById(R.id.videoView);
		btnNext=(Button)findViewById(R.id.btnNext);
		String path = "android.resource://" + getPackageName() + "/" + R.raw.cibs;
		/*videoView.setLayoutParams(new FrameLayout.LayoutParams(800,800));
		LinearLayout linearLayout = (LinearLayout)findViewById(R.id.layInfo);
		linearLayout.removeAllViews();
		linearLayout.addView(videoView);*/
		videoView.setVideoURI(Uri.parse(path));
		videoView.start();
		videoView.setVisibility(View.VISIBLE);
       
		btnNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(WatchVideo.this, QuickOrCustom.class);
				 startActivity(i);
				 finish();				
			}
		});
	}	
}
