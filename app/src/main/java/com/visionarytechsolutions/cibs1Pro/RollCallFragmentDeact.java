package com.visionarytechsolutions.cibs1Pro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.adapter.ContactItemAdapter;
import com.visionarytechsolutions.cibs1Pro.fileUpload.NetworkOperations;

public class RollCallFragmentDeact extends Fragment {	
	
	View rootView;
	SharedPreferences sharedPreferences;
	Editor editor;
	static RollCallFragmentDeact instance;
	RelativeLayout mainLayout;
	TextView meetingLocationTv;
	Button changeLocationBtn, deactivateBtn;
	Location selectedLocation;
	String addressString;
	ListView contactListView;
	ArrayList<String> contactsArrayList = new ArrayList<String>();
	JSONObject contactsJSON = new JSONObject();
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();			
        instance = this;		
        rootView = inflater.inflate(R.layout.fragment_rollcall_deact, container, false);
		mainLayout = (RelativeLayout) rootView.findViewById(R.id.rollcall_mainLayout);
		meetingLocationTv = (TextView) rootView.findViewById(R.id.rollcall_meetingLocTv);
		meetingLocationTv.setSelected(true);
		changeLocationBtn = (Button) rootView.findViewById(R.id.rollcall_btnChangeLocation);
		deactivateBtn = (Button) rootView.findViewById(R.id.rollcall_btnDeactivate);
		contactListView = (ListView) rootView.findViewById(R.id.rollcall_contactList);
		
		loadLocationsList();
		loadContacts();
		
		changeLocationBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(checkNetwork()){
			    	Intent intent=new Intent(getActivity().getApplicationContext(),MapActivity.class);  
					startActivityForResult(intent, 2);
				}else
					Toast.makeText(getActivity().getApplicationContext(), "Please check your network connection", Toast.LENGTH_LONG).show();
			}
		});
		
		deactivateBtn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
		        sendLocationChanges("DeactivateRollcall");
				JSONObject contactsDeJSON = new JSONObject();
				editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());
		          editor.putBoolean("rollcallActivated", false);
		          editor.commit();
		        if(MainActivity.instance!=null) MainActivity.instance.displayView(8);
			}
		});
         
        return rootView;
    }
	
	public void loadContacts(){
		try {
			contactsJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
			Log.e("jasonn",contactsJSON+"");
	    	@SuppressWarnings("rawtypes")
			Iterator it = contactsJSON.keys();
	    	contactsArrayList.removeAll(contactsArrayList);
	        while(it.hasNext())
	        	contactsArrayList.add(it.next().toString());
	        contactListView.setAdapter(new ContactItemAdapter(contactsJSON, getActivity().getApplicationContext(), RollCallFragmentDeact.instance, contactsArrayList));
	        contactListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					JSONObject contactJSON, locationJSON;
					try {
						Log.e("array",contactsJSON.getString(contactsArrayList.get(position))+"");

						contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
						locationJSON = new JSONObject(contactJSON.getString("location"));
						if(locationJSON.length()>0){
							double lat = locationJSON.getDouble("lat");
							double lon = locationJSON.getDouble("lon");
							String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=17&q=%f,%f", lat,lon,lat,lon);
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
							instance.startActivity(intent);
						}else{
							Toast.makeText(getActivity().getApplicationContext(), "No Location Received Yet", Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						Log.d("Contact:", e.toString());
					}
				}
			});
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Log.e("error", e.toString());
		}
	}	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){  
      super.onActivityResult(requestCode, resultCode, data);  
       // check if the request code is same as what is passed  here it is 2  
      if(data != null)
        if(requestCode==2){  
           selectedLocation=data.getParcelableExtra("LOCATION");
           addressString = data.getStringExtra("addressString");
           if(selectedLocation != null )
        	   saveLocation();
        }
	}
	
	public void saveLocation(){
    	try {
     	   JSONObject locationJSON = new JSONObject();
     	   locationJSON.put("lat", selectedLocation.getLatitude());
     	   locationJSON.put("lon", selectedLocation.getLongitude());
     	   locationJSON.put("addressString", addressString);
		   editor.putString("meetingLocationJSON", locationJSON.toString());
		   editor.commit();
		   Toast.makeText(getActivity().getApplicationContext(), "Meeting Location Changed Successfully", Toast.LENGTH_LONG).show();
		   sendLocationChanges("ChangeLocation");
		   loadLocationsList();
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}          
    }
	
	public void sendLocationChanges(String cmd){
		try {
			JSONObject contactsJSON = new JSONObject();
			JSONObject contactsDeJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
			@SuppressWarnings("rawtypes")
			Iterator it = contactsDeJSON.keys();
	        while(it.hasNext()){
	        	String name = it.next().toString();
	        	JSONObject contactJSON = new JSONObject(contactsDeJSON.getString(name));
	        	String status = contactJSON.getString("status");
	        	if(!status.equalsIgnoreCase("safe") && !status.equalsIgnoreCase("reached") && !status.equalsIgnoreCase("deactivated") && !status.equalsIgnoreCase("InOtherRollCall"))
	        		contactsJSON = contactsJSON.put(name, contactJSON);
	        }
	          String meetingLocation = sharedPreferences.getString("meetingLocationJSON", "{}");
			String phone = sharedPreferences.getString("phone", "");
			String email = sharedPreferences.getString("email", "");
			String name = sharedPreferences.getString("firstName", "")+" "+sharedPreferences.getString("lastName", "");
			List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();
			Log.e("contacts",contactsDeJSON+"");
			Log.e("meetinglocations",meetingLocation+"");

			parameters.add(new Pair<String, String>("meetingLocation", meetingLocation));
			parameters.add(new Pair<String, String>("contacts", contactsJSON.toString()));
			parameters.add(new Pair<String, String>("phone", phone));
			parameters.add(new Pair<String, String>("email", email));
			parameters.add(new Pair<String, String>("name", name));
			parameters.add(new Pair<String, String>("cmd", cmd));				
			String url = "http://www.checkinbesafe.com/webservices/cibs_activaterollcall.php";
			NetworkOperations nop = new NetworkOperations(url, parameters, getActivity(), "change");
			nop.execute();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void loadLocationsList(){
		JSONObject locationJSON = new JSONObject();
		try {
			locationJSON = new JSONObject(sharedPreferences.getString("meetingLocationJSON", "{}"));
			if(locationJSON.length()>0) meetingLocationTv.setText(locationJSON.getString("addressString"));
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	
	
	private boolean checkNetwork(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
    }
	
}
