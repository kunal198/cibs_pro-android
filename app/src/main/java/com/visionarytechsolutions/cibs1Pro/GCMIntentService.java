package com.visionarytechsolutions.cibs1Pro;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.visionarytechsolutions.cibs1Pro.GCM.GCMBaseIntentService;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ServerUtilities;
import com.visionarytechsolutions.cibs1Pro.service.RollcallRequestService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.SENDER_ID;
import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.displayMessage;
 
public class GCMIntentService extends GCMBaseIntentService {
 
    private static final String TAG = "GCMIntentService";
    SharedPreferences sharedPreferences;
    Editor editor;
    String name="",phone="",email="";
 
    public GCMIntentService() {
        super(SENDER_ID);
    }
 
    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("firstName", "")+" "+sharedPreferences.getString("lastName", "");
        phone = sharedPreferences.getString("phone", "");
        email = sharedPreferences.getString("email", "");
        Log.d("NAME", name);
        ServerUtilities.register(context, name, phone, email, registrationId);
    }
 
    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }
 
    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        String message = intent.getExtras().getString("message");    
        Log.e(TAG, "Received message"+message);
        displayMessage(context, message);
        sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        // notifies user
        if(message.equalsIgnoreCase("RollCallRequest")){        	
            boolean rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
            boolean rollcallActivated = sharedPreferences.getBoolean("rollcallActivated", false);
            if(!rollCallRequestAccepted && !rollcallActivated){
	        	String data = intent.getExtras().getString("rollcallData");
	          Log.e(TAG, "datadeatilss"+data);

//	        	String name = "";
//	        	try {
//					JSONObject dataJson = new JSONObject(data);
//					name = dataJson.getString("name");
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}        	
	            editor.putBoolean("rollCallRequestReceived", true);
	            editor.putString("rollCallData", data);
	            editor.commit();
	            Intent rollcallService = new Intent(context,RollcallRequestService.class);
	        	rollcallService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        	context.startService(rollcallService);
//	        	if(MainActivity.instance!=null){
//		        	MainActivity.instance.runOnUiThread(new Runnable() {
//	    	            public void run(){  
//	    	            	MainActivity.instance.showRollCallAlertDialog();
//	    	            	try {
//	    	            	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//	    	            	    Ringtone r = RingtoneManager.getRingtone(MainActivity.instance, notification);
//	    	            	    r.play();
//	    	            	} catch (Exception e) {
//	    	            	    e.printStackTrace();
//	    	            	}
//	    	            }
//	    	        });
//		        }else{
////		        	generateNotification(context, "New RollCall Invitation from "+name);
//		        	Intent rollcallService = new Intent(context,RollcallRequestService.class);
//		        	rollcallService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		        	context.startService(rollcallService);
//		        }
            }else{
            	if(MainActivity.instance!=null){
		        	MainActivity.instance.runOnUiThread(new Runnable() {
	    	            public void run(){  
	    	            	MainActivity.instance.changeRollcallStatus("InOtherRollCall");
	    	            }
	    	        });
		        }
            }
        }else if(message.equalsIgnoreCase("ChangeStatus")){
     	   Log.e("in status","status");
        	String data = intent.getExtras().getString("rollcallData");
        	Log.e("roll call data",data);
        	String phone = "", email = "", status = "", key = "";
        	try {
				JSONObject dataJson = new JSONObject(data);
				phone = dataJson.getString("phone");
				email = dataJson.getString("email");
				status = dataJson.getString("status");				
			} catch (JSONException e) {
				e.printStackTrace();
			}
        	//03-09 10:36:22.452: E/roll call data(24010): {'phone':8888888888,'email':ram@gmail.com,'status':Accepted}

        	//03-09 10:36:22.465: E/jasonn(24010): {"":{"status":"Accepted","color":"red"}}

        	  sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            JSONObject contactsDeJSON = new JSONObject();
            JSONObject contactJSON = new JSONObject();
            try {
            	contactsDeJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
            	Log.e("jsonndata",contactsDeJSON+"");
            	@SuppressWarnings("rawtypes")
    			Iterator it = contactsDeJSON.keys();
    	          while(it.hasNext()){
    	        	key = it.next().toString();
    	        	Log.e("keysss","hjh"+key);
    	        	contactJSON = new JSONObject(contactsDeJSON.getString(key));
    	        	String phone1 = contactJSON.getString("phone");
    	        	String email1 = contactJSON.getString("email");
    	        	if(phone1.equalsIgnoreCase(phone) || email1.equalsIgnoreCase(email)) break;
    	        }            	
            	contactJSON.put("status", status);
            	if(status.equalsIgnoreCase("reached")||status.equalsIgnoreCase("safe"))
            		contactJSON.put("color", "green");
            	else if(status.equalsIgnoreCase("inroute"))
            		contactJSON.put("color", "yellow");
            	else
            		contactJSON.put("color", "red");
            	contactsDeJSON = contactsDeJSON.put(key, contactJSON);
    	          editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());
    	          editor.commit();
    	        if(MainActivity.instance!=null){
    	        	MainActivity.instance.runOnUiThread(new Runnable() {
	    	            public void run(){  
	    	            	if(RollCallFragmentDeact.instance!=null) RollCallFragmentDeact.instance.loadContacts();
	    	            }
	    	        });
    	        }    	        
    		} catch (JSONException e) {
    			e.printStackTrace();
    		}
        }else if(message.equalsIgnoreCase("ChangeUserLocation")){
        	String data = intent.getExtras().getString("rollcallData");
        	Log.e("location daat",data);
        	String phone = "", email = "", status = "", key = "";
        	try {
				JSONObject dataJson = new JSONObject(data);
				phone = dataJson.getString("phone");
				email = dataJson.getString("email");
				status = dataJson.getString("status");			
			} catch (JSONException e) {
				e.printStackTrace();
			}
        	  sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            JSONObject contactsDeJSON = new JSONObject();
            JSONObject contactJSON = new JSONObject();
            try {
            	contactsDeJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
            	@SuppressWarnings("rawtypes")
    			Iterator it = contactsDeJSON.keys();
    	        while(it.hasNext()){
    	        	key = it.next().toString();
    	        	contactJSON = new JSONObject(contactsDeJSON.getString(key));
    	        	String phone1 = contactJSON.getString("phone");
    	        	String email1 = contactJSON.getString("email");
    	        	if(phone1.equalsIgnoreCase(phone) || email1.equalsIgnoreCase(email)) break;
    	        }            	
            	contactJSON.put("location", status);
            	contactsDeJSON = contactsDeJSON.put(key, contactJSON);
    	          editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());
    	          editor.commit();
    	        if(MainActivity.instance!=null){
    	        	MainActivity.instance.runOnUiThread(new Runnable() {
	    	            public void run(){  
	    	            	if(RollCallFragmentDeact.instance!=null) RollCallFragmentDeact.instance.loadContacts();
	    	            }
	    	        });
    	        }    	        
    		} catch (JSONException e) {
    			e.printStackTrace();
    		}
        }else if(message.equalsIgnoreCase("ChangeLocation")){
        	String data = intent.getExtras().getString("rollcallData");
        	editor.putString("rollCallData", data);
            editor.commit();
            try {
	            if(MainActivity.instance!=null){
		        	MainActivity.instance.runOnUiThread(new Runnable() {
	    	            public void run(){  
	    	            	if(RollCallFragmentAccept.instance!=null) RollCallFragmentAccept.instance.setLocation();
	    	            }
	    	        });
		        }
            } catch (Exception e) {    			
    		}
        }else if(message.equalsIgnoreCase("DeactivateRollcall")){
        	try {
	            if(MainActivity.instance!=null){
		        	MainActivity.instance.runOnUiThread(new Runnable() {
	    	            public void run(){  
	    	            	if(RollCallFragmentAccept.instance!=null) RollCallFragmentAccept.instance.deactivateRollcall();
	    	            	editor.putBoolean("rollCallRequestReceived", false);
	    	            	editor.commit();
	    	            	if(MainActivity.instance.alertDialog != null) MainActivity.instance.alertDialog.dismiss();
	    	            }
	    	        });
		        }
            } catch (Exception e) {    			
    		}
        }else
        	generateNotification(context, message);
    }
 
    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }
 
    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }
 
    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }
 
    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
        int icon = R.drawable.app_icon;
        long when = System.currentTimeMillis();
        Intent notificationIntent = new Intent(context, FirstActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        String title = context.getString(R.string.app_name);
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(context)
        .setTicker(message)
        .setSmallIcon(icon)
        .setWhen(when)
        .setContentTitle(title)
        .setContentText(message)
        .setContentIntent(intent).build();
         
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
         
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
         
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);       
    }
 
}
