package com.visionarytechsolutions.cibs1Pro;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.visionarytechsolutions.cibs1Pro.utils.PlaceJSONParser;

public class WhereIam extends Activity implements OnMapReadyCallback,LocationListener {
	MapFragment map;
	GoogleMap googleMap;
	LocationManager locationManager;
	Location location,selectedLocation,addressLocation;
	Bitmap userLocationIcon, selectedLocationIcon,safeLocationIcon;
	Marker selectedLocationMarker,userLocationMarker;
	MarkerOptions markerOptions;
	ProgressDialog dialog;
     LatLng latLng;
     ArrayList<String> safeLocationsNameList=new ArrayList<String>();
     EditText searchLocationEdt;
     ArrayList<Marker> previousSearchMarkers = new ArrayList<Marker>();
     RelativeLayout mainLayout;
	String userAddressString, selectedAddressString, callingMtd;
	SharedPreferences sharedPreferences;
	JSONObject safeLocationJSON;
	TextView tvCurrentLocaton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_where_iam);		 
		    userLocationIcon = BitmapFactory.decodeResource(WhereIam.this.getResources(), R.drawable.user_location_new);
	        userLocationIcon = Bitmap.createScaledBitmap(userLocationIcon, 45, 45, false);
	        safeLocationIcon = BitmapFactory.decodeResource(WhereIam.this.getResources(), R.drawable.user_location_green);
	        safeLocationIcon = Bitmap.createScaledBitmap(safeLocationIcon, 45, 45, false);
	        tvCurrentLocaton=(TextView)findViewById(R.id.tvCurrentLocation);
	        mainLayout = (RelativeLayout) findViewById(R.id.map_screen_MainRelativelay);	                
	        selectedLocationIcon = BitmapFactory.decodeResource(WhereIam.this.getResources(), R.drawable.user_selected_location_new);
	        selectedLocationIcon = Bitmap.createScaledBitmap(selectedLocationIcon, 20, 25, false);	        
	        selectedLocation = new Location("dummyprovider");	
	        dialog=new ProgressDialog(WhereIam.this);
	        dialog.setMessage("Loading...");
	        dialog.show();
	        dialog.setCancelable(false);
	        getActionBar().hide();	        
	        map = (MapFragment) getFragmentManager()
	                .findFragmentById(R.id.map);
	        map.getMapAsync(this);	  	        
			sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);			
			Log.e("maplatt",sharedPreferences.getString("safeLocationJSON","{}"));						  
			
			try {
				safeLocationJSON = new JSONObject(sharedPreferences.getString("safeLocationJSON","{}"));
				Iterator it = safeLocationJSON.keys();
		        while(it.hasNext()){
		        	try {
		        		String key = it.next().toString();
						JSONObject locationJSON = (JSONObject) safeLocationJSON.get(key);
						safeLocationsNameList.add(key);
						Log.e("keyss",key);
					} catch (JSONException e) {
						Log.d("Exception", e.toString());
					}        	
		        }		        
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}							        
	    }
		@Override
		public void onMapReady(GoogleMap googleMap1) {
			googleMap = googleMap1;		
			
			 googleMap.setOnMarkerClickListener(new OnMarkerClickListener()
             {
                 @Override
                 public boolean onMarkerClick(Marker arg0) {
                    // if(arg0.getTitle().equals("MyHome")) // if marker source is clicked
                	 String current_location=location.getLatitude()+","+location.getLongitude();
                	 Log.e("markerdetails",arg0.getTitle());
                	 String title[]=arg0.getTitle().split(":");
                	 Log.e("markerdetails",title[1]);
                	 
                	 //PlacesTask placesTask = new PlacesTask();	
     				 //placesTask.execute(title[0]);
     				

                	 String latlng[]=arg0.getPosition().toString().split(":");
                	 latlng= latlng[1].split(",");
                	 String str; 
                	 str = latlng[0].replace("(", "");
                	 str=str+","+latlng[1].replace(")", "");
                	 
                	 Log.e("detailss",str);
                	 //latlng[0]=latlng[0].replaceAll("("," ");          
                	 
                	 Log.e("latii",latlng[0]);
                	 Log.e("latii",latlng[1]);

                	 Intent i=new Intent(WhereIam.this,MapDetails.class).putExtra("selectedLocation", str).putExtra("currentLocation", current_location).putExtra("placeId", title[0]).putExtra("title", title[1]).putExtra("userAddressString", userAddressString);
                	 startActivity(i);
                          //Toast.makeText(WhereIam.this, arg0.getPosition()+"", Toast.LENGTH_SHORT).show();// display toast
                     return true;
                 }

             });     					
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
	        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	        if(location == null){
	        	try{
		        	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		        	location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);  
	        	}catch(Exception e){
	        		Log.d("Exception", e.toString());
	        	}
	        }
						//if(location != null){
				for(int i=0;i<safeLocationsNameList.size();i++)
		        {
					try {
			        	JSONObject obj=safeLocationJSON.getJSONObject(safeLocationsNameList.get(i));
						//googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(obj.getString("lat")), Double.parseDouble(obj.getString("lon"))), 18));
						userLocationMarker = googleMap.addMarker(new MarkerOptions()
			            .icon(BitmapDescriptorFactory.fromBitmap(safeLocationIcon))
			            .title("Safe Location : "+obj.getString("addressString"))
			            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
			            .position(new LatLng(Double.parseDouble(obj.getString("lat")), Double.parseDouble(obj.getString("lon")))));
						userLocationMarker.showInfoWindow();												
						callingMtd = "userLocation";
						addressLocation = location;
						Log.e("added amrker","marker");
						//new AddressGeocoderTask().execute();
						userLocationMarker.showInfoWindow();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	//Log.e("lattt",obj.getString("lat"));		        			        			    		        	
		        }	
				try
				{
				StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
				sb.append("location="+location.getLatitude()+","+location.getLongitude());
				sb.append("&radius=5000");
				sb.append("&types="+"hospital");
				sb.append("&sensor=true");
				sb.append("&key=AIzaSyDhcOssZC2OzZBSGbUru99FWNPIymGUeQs");
				// Creating a new non-ui thread task to download Google place json data 
				PlacesTask placesTask = new PlacesTask();	
				placesTask.execute(sb.toString());
				Log.e("urll",sb.toString());	
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			//}
		}

		@Override
		public void onLocationChanged(Location location) {		
			double distance = 11.0;
			if(this.location!=null)
				distance = location.distanceTo(this.location);
			
			if(distance>=50.0){
				this.location = location;
				if(userLocationMarker != null)
					userLocationMarker.remove();
				 userLocationMarker = googleMap.addMarker(new MarkerOptions()
		        .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
		        .title("Your Current location")
		        .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
		        .position(new LatLng(location.getLatitude(), location.getLongitude())));
				googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 2));
				callingMtd = "userLocation";
				addressLocation = location;
				new AddressGeocoderTask().execute();
			}
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			
		}
		private class GeocoderTask extends AsyncTask<String, Void, List<Address>>{
			 
	        @Override
	        protected List<Address> doInBackground(String... locationName) {
	            // Creating an instance of Geocoder class
	            Geocoder geocoder = new Geocoder(getBaseContext());
	            List<Address> addresses = null;
	 
	            try {
	                // Getting a maximum of 3 Address that matches the input text
	                addresses = geocoder.getFromLocationName(locationName[0], 3);
	            } catch (IOException e) {
	                Log.d("Exception", e.toString());
	            }
	            return addresses;
	        }
	 
	        @Override
	        protected void onPostExecute(List<Address> addresses) {
	 
	            if(addresses==null || addresses.size()==0){
	                Toast.makeText(getApplicationContext(), "No Location found", Toast.LENGTH_SHORT).show();
	                return;
	            }
	 
	            // Clears all the existing markers on the map
	            
	            for (Marker marker : previousSearchMarkers) {
					marker.remove();
				}            
	            
	            previousSearchMarkers.removeAll(previousSearchMarkers);
	            // Adding Markers on Google Map for each matching address
	            for(int i=0;i<addresses.size();i++){
	 
	                Address address = (Address) addresses.get(i);
	 
	                // Creating an instance of GeoPoint, to display in Google Map
	                latLng = new LatLng(address.getLatitude(), address.getLongitude());
	                
	                selectedLocation.setLatitude(latLng.latitude);
					selectedLocation.setLongitude(latLng.longitude);
	 
//	                String addressText = String.format("%s, %s",
//	                address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
//	                address.getCountryName());
	                
	                selectedAddressString = "";
	            	if(address.getAddressLine(0) != null)
	            		selectedAddressString = address.getAddressLine(0);
	            	if(address.getAddressLine(1) != null)
	            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(1);
	            	if(address.getAddressLine(2) != null)
	            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(2);
	            	if(address.getAddressLine(3) != null)
	            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(3);
	            	if(!selectedAddressString.contains(address.getCountryName()))
	            		selectedAddressString = selectedAddressString + "," + address.getCountryName();
	 
	                markerOptions = new MarkerOptions();
	                markerOptions.position(latLng);
	                markerOptions.title(selectedAddressString);
	 
	                Marker marker = googleMap.addMarker(markerOptions);
	                marker.showInfoWindow();
	                
	                previousSearchMarkers.add(marker);
	 
	                // Locate the first location
	                if(i==0)
	                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
	            }
	        }
	    }
		
		
		private class AddressGeocoderTask extends AsyncTask<Void, Void, List<Address>>{
							 
	        @Override
	        protected List<Address> doInBackground(Void... params) {
	            // Creating an instance of Geocoder class
	            Geocoder geocoder = new Geocoder(getBaseContext());
	            List<Address> addresses = null; 
	            try {

	                //addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
	                addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);

		               // addresses = geocoder.getFromLocation(addressLocation.getLatitude(), addressLocation.getLongitude(), 1);

	                
	            } catch (IOException e) {
	                Log.d("Exception", e.toString());
	            }
	            return addresses;
	        }
	 
	        @Override
	        protected void onPostExecute(List<Address> addresses) {
	 
	            if(addresses==null || addresses.size()==0){
	               // Toast.makeText(getApplicationContext(), "No Location found", Toast.LENGTH_SHORT).show();
	                return;
	            }
	 
	            Address address = (Address) addresses.get(0);
	            try
	            {	
	                Log.e("calling",""+callingMtd);	       
	            	userAddressString = "";
	            	if(address.getAddressLine(0) != null)
	            		userAddressString = address.getAddressLine(0);
	            	if(address.getAddressLine(1) != null)
	            		userAddressString = userAddressString + "," + address.getAddressLine(1);
	            	if(address.getAddressLine(2) != null)
	            		userAddressString = userAddressString + "," + address.getAddressLine(2);
	            	if(address.getAddressLine(3) != null)
	            		userAddressString = userAddressString + "," + address.getAddressLine(3);
	            	if(!userAddressString.contains(address.getCountryName()))
	            		userAddressString = userAddressString + "," + address.getCountryName();            	
	            	
	            	//userLocationMarker.setTitle("YourLoc:"+userAddressString);
	            	
	            	Marker userLocationMarker = googleMap.addMarker(new MarkerOptions()
		            .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
		            .title("Current Location : "+userAddressString)
		            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
		            .position(new LatLng(location.getLatitude(),location.getLongitude())));
					userLocationMarker.showInfoWindow();
					googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),location.getLongitude()), 18));	            	
	            	tvCurrentLocaton.setText("Your Current Location is -> "+userAddressString);  
	            	dialog.dismiss();
	            	
	              }
	            catch(Exception e)
	            {	            	
	            e.printStackTrace();
	            }
	        } 
	    }
		
		@Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			locationManager.removeUpdates(this);
		}
		
		 private String downloadUrl(String strUrl) throws IOException{
		        String data = "";
		        InputStream iStream = null;
		        HttpURLConnection urlConnection = null;
		        try{
		                URL url = new URL(strUrl);                
		                

		                // Creating an http connection to communicate with url 
		                urlConnection = (HttpURLConnection) url.openConnection();                

		                // Connecting to url 
		                urlConnection.connect();                

		                // Reading data from url 
		                iStream = urlConnection.getInputStream();

		                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

		                StringBuffer sb  = new StringBuffer();

		                String line = "";
		                while( ( line = br.readLine())  != null){
		                        sb.append(line);
		                }

		                data = sb.toString();

		                br.close();

		        }catch(Exception e){
		                Log.d("Exception while downloading url", e.toString());
		        }finally{
		                iStream.close();
		                urlConnection.disconnect();
		        }

		        return data;
		    }         

			
			/** A class, to download Google Places */
			private class PlacesTask extends AsyncTask<String, Integer, String>{

				String data = null;
				
				// Invoked by execute() method of this object
				@Override
				protected String doInBackground(String... url) {
					try{
						data = downloadUrl(url[0]);
					}catch(Exception e){
						 Log.e("Background Task",e.toString());
					}
					return data;
				}
				
				// Executed after the complete execution of doInBackground() method
				@Override
				protected void onPostExecute(String result){			
					ParserTask parserTask = new ParserTask();
					
					// Start parsing the Google places in JSON format
					// Invokes the "doInBackground()" method of the class ParseTask
					parserTask.execute(result);
				}
				
			}
			
			/** A class to parse the Google Places in JSON format */
			private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

				JSONObject jObject;
				
				// Invoked by execute() method of this object
				@Override
				protected List<HashMap<String,String>> doInBackground(String... jsonData) {
				
					List<HashMap<String, String>> places = null;			
					PlaceJSONParser placeJsonParser = new PlaceJSONParser();
		        
			        try{
			        	jObject = new JSONObject(jsonData[0]);			        	
			            /** Getting the parsed data as a List construct */
			            places = placeJsonParser.parse(jObject);
			            
			        }catch(Exception e){
			                Log.d("Exception",e.toString());
			        }
			        return places;
				}
				
				// Executed after the complete execution of doInBackground() method
				@Override
				protected void onPostExecute(List<HashMap<String,String>> list){			
					
					// Clears all the existing markers 
					//mGoogleMap.clear();
					
					for(int i=0;i<list.size();i++){
					
						
						// Creating a marker
			            MarkerOptions markerOptions = new MarkerOptions();
			            
			            // Getting a place from the places list
			            HashMap<String, String> hmPlace = list.get(i);
			
			            // Getting latitude of the place
			            double lat = Double.parseDouble(hmPlace.get("lat"));	            
			            
			            // Getting longitude of the place
			            double lng = Double.parseDouble(hmPlace.get("lng"));
			            
			            // Getting name
			            String name = hmPlace.get("place_name");
			            
			            // Getting vicinity
			            String vicinity = hmPlace.get("vicinity");
			            
			            //Getting place_Id;
			            
			            String place_id=hmPlace.get("place_id");
			            
			            LatLng latLng = new LatLng(lat, lng);
			            
			            // Setting the position for the marker
			            markerOptions.position(latLng);
			
			            // Setting the title for the marker. 
			            //This will be displayed on taping the marker
			            markerOptions.title(place_id +" :  "+name + " " + vicinity);				            			            			            
			            Marker userLocationMarker = googleMap.addMarker(new MarkerOptions()
			            .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
			            .title(place_id +" :  "+name + " " + vicinity)
			            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
			            .position(latLng));
						 userLocationMarker.showInfoWindow();											
						//callingMtd = "userLocation";
									
			            // Placing a marker on the touched position
			           //googleMap.addMarker(markerOptions);
					}		
				    new AddressGeocoderTask().execute();
				}			
			}			
	}
