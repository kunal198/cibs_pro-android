package com.visionarytechsolutions.cibs1Pro;

import com.visionarytechsolutions.cibs1Pro.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class aboutFragment extends Fragment {	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) { 
        return inflater.inflate(R.layout.about_application, container, false);
    }	
}
     