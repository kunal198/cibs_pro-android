package com.visionarytechsolutions.cibs1Pro.adapter;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.ContactsListFragment;
import com.visionarytechsolutions.cibs1Pro.RollCallFragment;
import com.visionarytechsolutions.cibs1Pro.RollCallFragmentDeact;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactItemAdapter extends ArrayAdapter<String> {
	 
private JSONObject contactsJSON;
private Context context;
ContactsListFragment contactsListFragment;
RollCallFragment rollCallFragment;
RollCallFragmentDeact rollCallFragmentDeact;
ArrayList<String> contactsArrayList;
 
public ContactItemAdapter(JSONObject contactsJSON, Context ctx,ContactsListFragment contactsListFragment,ArrayList<String> contactsArrayList) {
    super(ctx, R.layout.contact_item, contactsArrayList);
    this.contactsJSON = contactsJSON;
    this.contactsArrayList = contactsArrayList;
    this.context = ctx;
    this.contactsListFragment = contactsListFragment;
}

public ContactItemAdapter(JSONObject contactsJSON, Context ctx,RollCallFragment rollCallFragment,ArrayList<String> contactsArrayList) {
    super(ctx, R.layout.contact_item, contactsArrayList);
    this.contactsJSON = contactsJSON;
    this.contactsArrayList = contactsArrayList;
    this.context = ctx;
    this.rollCallFragment = rollCallFragment;
}

public ContactItemAdapter(JSONObject contactsJSON, Context ctx,RollCallFragmentDeact rollCallFragmentDeact,ArrayList<String> contactsArrayList) {
    super(ctx, R.layout.contact_item, contactsArrayList);
    this.contactsJSON = contactsJSON;
    this.contactsArrayList = contactsArrayList;
    this.context = ctx;
    this.rollCallFragmentDeact = rollCallFragmentDeact;
}
 
public View getView(final int position, View convertView, ViewGroup parent) {
     
    // First let's verify the convertView is not null
    if (convertView == null) {
        // This a new view we inflate the new layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(rollCallFragmentDeact!=null)
        	convertView = inflater.inflate(R.layout.contact_item_roll_dea, parent, false);
        else
        	convertView = inflater.inflate(R.layout.contact_item, parent, false);
    }
        // Now we can fill the layout with the right values
    	if(rollCallFragmentDeact!=null){
    		
    		TextView tv = (TextView) convertView.findViewById(R.id.contact_rollde_name); 
	        tv.setText(contactsArrayList.get(position));
	        
	        TextView tv1 = (TextView) convertView.findViewById(R.id.contact_rollde_phoneNumber); 
	        try {
	        	JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				tv1.setText(contactJSON.getString("phone"));
			} catch (JSONException e) {
				Log.d("ContactItem", e.toString());
			}
	        
	        TextView tv2 = (TextView) convertView.findViewById(R.id.contact_rollde_status); 
	        try {
	        	JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				tv2.setText(contactJSON.getString("status"));
			} catch (JSONException e) {
				Log.d("ContactItem", e.toString());
			}
	        
	        ImageView red = (ImageView) convertView.findViewById(R.id.contact_rollde_red);
	        ImageView green = (ImageView) convertView.findViewById(R.id.contact_rollde_green);
	        ImageView yellow = (ImageView) convertView.findViewById(R.id.contact_rollde_yellow);
	        try {
	        	JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				String color = contactJSON.getString("color");
				if(color.equalsIgnoreCase("red")){ 
					red.setVisibility(View.VISIBLE);
					green.setVisibility(View.GONE);
					yellow.setVisibility(View.GONE);
				}else if(color.equalsIgnoreCase("green")){ 
					green.setVisibility(View.VISIBLE);
					red.setVisibility(View.GONE);
					yellow.setVisibility(View.GONE);
				}else{
					yellow.setVisibility(View.VISIBLE);
					red.setVisibility(View.GONE);
					green.setVisibility(View.GONE);
				}
			} catch (JSONException e) {
				Log.d("ContactItem", e.toString());
			}
	        
    	}else{
	        TextView tv = (TextView) convertView.findViewById(R.id.contact_name); 
	        tv.setText(contactsArrayList.get(position));
	        
	        TextView tv1 = (TextView) convertView.findViewById(R.id.contact_phoneNumber); 
	        try {
	        	JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				tv1.setText(contactJSON.getString("phone"));
			} catch (JSONException e) {
				Log.d("ContactItem", e.toString());
			}
	        
	        ImageView img = (ImageView) convertView.findViewById(R.id.contact_removeBtn);
	        img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(contactsListFragment!=null) contactsListFragment.removeContactInfo(position);
					if(rollCallFragment!=null) rollCallFragment.removeContactInfo(position);
				}
			});
    	}
     
    return convertView;
}
}
