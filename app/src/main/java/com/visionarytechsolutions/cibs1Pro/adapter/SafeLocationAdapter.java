package com.visionarytechsolutions.cibs1Pro.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.SafeLocationsFragment;
import com.visionarytechsolutions.cibs1Pro.utils.IoListener;

public class SafeLocationAdapter extends ArrayAdapter<String> {
	 
private List<String> safeLocationsList;
private Context context;
SafeLocationsFragment safeLocationsFragment;
IoListener mListener;
 
public SafeLocationAdapter(List<String> safeLocationsList, Context ctx,SafeLocationsFragment safeLocationsFragment,IoListener mListener) {
    super(ctx, R.layout.safe_location_item, safeLocationsList);
    this.safeLocationsList = safeLocationsList;
    this.context = ctx;
    this.safeLocationsFragment = safeLocationsFragment;
    this.mListener=mListener;
}
 
public View getView(final int position, View convertView, ViewGroup parent) {
     
    // First let's verify the convertView is not null
    if (convertView == null) {
        // This a new view we inflate the new layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.safe_location_item, parent, false);
    }
        // Now we can fill the layout with the right values
        TextView tv = (TextView) convertView.findViewById(R.id.sf_locationTv); 
        tv.setText(safeLocationsList.get(position));
        tv.setSelected(true);
        
        ImageView img = (ImageView) convertView.findViewById(R.id.sf_removeBtn);
        
        tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mListener.onListClick(position);
			}
		});
        img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				safeLocationsFragment.removeSafeLocation(position);
			}
		});
     
    return convertView;
}
}
