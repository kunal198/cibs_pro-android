package com.visionarytechsolutions.cibs1Pro;

import com.visionarytechsolutions.cibs1Pro.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class AlertDialogConfirm extends Activity{	
	public static AlertDialogConfirm instance;
	EditText confirmPassword;
	SharedPreferences sharedPreferences;
	TextView confirmMsg;
	Editor editor;
	int attempts = 0;
	boolean fromMain = false;
	boolean preset = false;		
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    instance = this;
	    this.setFinishOnTouchOutside(false);
	    sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
	    editor = sharedPreferences.edit();
		setContentView(R.layout.alert_dialog_confirm);
		confirmPassword = (EditText) findViewById(R.id.confirmPassword);
		confirmMsg = (TextView) findViewById(R.id.confirmMsg);
		fromMain = getIntent().getBooleanExtra("fromMain", false);
		preset = getIntent().getBooleanExtra("preset", false);
		Log.e("presett",""+preset);

		if(fromMain)
			confirmMsg.setText("Please enter your password to view the list");
		else
			confirmMsg.setText("Please enter your password to confirm the changes.");
		
		confirmPassword.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if ((actionId & EditorInfo.IME_MASK_ACTION) == EditorInfo.IME_ACTION_DONE) {                	
                	confirmPasswordClicked(null);
                    return true;
                }
                return false;
            }
        });		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}
	
	public void confirmPasswordClicked(View v) {
		String password = confirmPassword.getText().toString();
		String userPwd = sharedPreferences.getString("password", "");
		if(password.equalsIgnoreCase(userPwd)){
			setResult(Activity.RESULT_OK);
			AlertDialogConfirm.instance.finish();
			
			editor.putBoolean("checkIn", false).apply();
			//Toast.makeText(getApplicationContext(), preset+"", Toast.LENGTH_SHORT).show();
			
			if(preset)
			editor.putBoolean("deActivate", false).apply();	
			//comment code
			/*if(preset)
			{
				MainActivity.instance.finish();
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
			}	*/
		}else{
			attempts+=1;
			if(attempts != 3){
				Toast.makeText(getApplicationContext(), "Please Enter Correct Password, "+(3-attempts)+" attempts remaining", Toast.LENGTH_LONG).show();
				confirmPassword.setText("");
			}
			else{				
				setResult(Activity.RESULT_CANCELED);
				AlertDialogConfirm.instance.finish();
			}
		}
	}
	
	public void cancelPasswordClicked(View v) {
		showAlert();
	}
	
	@Override
	public void onBackPressed() {
		//super.onBackPressed();		
		showAlert();
	}
	
	public void showAlert(){
		String msg = "";
		if(fromMain)
			msg = "Don't want to see the list?";
		else
			msg = "Do you want to discard changes?";
			
	   AlertDialog alertDialog = new AlertDialog.Builder(AlertDialogConfirm.this)
        .setMessage(msg)
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {		
				setResult(Activity.RESULT_CANCELED);
				AlertDialogConfirm.instance.finish();
				
				
				//code comment
				/*if(preset)
				{
					MainActivity.instance.finish();
					startActivity(new Intent(getApplicationContext(), MainActivity.class));
				}	*/							
			}	            	
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
        	public void onClick(DialogInterface dialog, int which) {
        		dialog.dismiss();
        	}
        })
        .setCancelable(false)
        .create();
		alertDialog.show();
	}
}
