package com.visionarytechsolutions.cibs1Pro;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.service.MouthService;
import com.visionarytechsolutions.cibs1Pro.service.ShakeService;

//import com.visionarytechsolutions.cibs1Pro.service.ThreasholdService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;


public class PresetSettingsFragment extends Fragment {	

	Spinner checkinTimerSpinner,locationRadiusSpinner;
	CheckBox leaveSafeLocationCheckBox,chkEmail,chkText,chkCall,chkVibrate,chkRingtone,chkShake;
	EditText personalAlertMessageEdt;
	SharedPreferences sharedPreferences;
	Editor editor;
	public static PresetSettingsFragment instance;
	LinearLayout safeLocationLayout,layMeter;
	RelativeLayout mainLayout;
	RadioGroup recordTypeRadio;
	Button btnSave;
	int previousTime = 0, presentTime = 0;
	public static Switch switchAudio;
	RadioButton rdoAudio;
	AlertActions alertActions;
	TextView tvMeter,tvFeet;	
	JSONObject timerJSON,radiusJSON;
	boolean fromFirstActivity = true,deActivate;

	View rootView;
	private static final int POLL_INTERVAL = 300;

	/** running state **/
	private boolean mRunning = false;

	/** config state **/

	//    
	private PowerManager.WakeLock mWakeLock;

	private Handler mHandler = new Handler();
	/*
     References to view elements 

     sound data source */
	private SoundMeter mSensor;
	Boolean switchStatus=false;

	//****************** Define runnable thread again and again detect noise *********//*

	//	private Runnable mSleepTask = new Runnable() {
	//		public void run() {
	//			//Log.i("Noise", "runnable mSleepTask");  
	//			try
	//			{
	//				start();
	//			}
	//			catch(Exception e)
	//			{
	//				e.printStackTrace();
	//			}
	//		}
	//	};   
	//	// Create runnable thread to Monitor Voice
	//	private Runnable mPollTask = new Runnable() {
	//		public void run() {           	
	//			double amp = mSensor.getAmplitudeEMA();
	//			String detail=amp+"";
	//			int index=detail.indexOf(".");
	//
	//			Log.e("amp","hh"+amp);
	//			Log.e("data",detail.substring(0, index));
	//			amp=Double.parseDouble(detail.substring(0, index));
	//			Log.e("amp11","hh"+amp);
	//			//Log.i("Noise", "runnable mPollTask");
	//
	//			if ((amp >= mThreshold)) {
	//				callForHelp();
	//				//Log.i("Noise", "==== onCreate ===");                         
	//			}                   
	//			// Runnable(mPollTask) will again execute after POLL_INTERVAL
	//			mHandler.postDelayed(mPollTask, POLL_INTERVAL);
	//
	//		}
	//	};	
	public PresetSettingsFragment(boolean fromFirstActivity){
		this.fromFirstActivity = fromFirstActivity;
	}
	public PresetSettingsFragment(){
		this.fromFirstActivity = false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//		  stop();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
		mSensor = new SoundMeter();

		PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");
		instance = this; 

		alertActions = new AlertActions(getActivity().getApplicationContext());

		final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);

		// clone the inflater using the ContextThemeWrapper
		LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

		rootView = localInflater.inflate(R.layout.fragment_preset_settings, container, false);        

		mainLayout = (RelativeLayout) rootView.findViewById(R.id.settings_mainLayout);

		checkinTimerSpinner = (Spinner) rootView.findViewById(R.id.time_spinner);
		locationRadiusSpinner = (Spinner) rootView.findViewById(R.id.radius_spinner);
		leaveSafeLocationCheckBox = (CheckBox) rootView.findViewById(R.id.chkLeaveSafeLocation);
		chkCall = (CheckBox) rootView.findViewById(R.id.chkCall);
		chkEmail= (CheckBox) rootView.findViewById(R.id.chkEmail);
		chkText = (CheckBox) rootView.findViewById(R.id.chkText);
		chkRingtone = (CheckBox) rootView.findViewById(R.id.chkRingtone);
		chkVibrate = (CheckBox) rootView.findViewById(R.id.chkVibrate);
		chkShake = (CheckBox) rootView.findViewById(R.id.chkShake);
		personalAlertMessageEdt = (EditText) rootView.findViewById(R.id.edtPersonalAlertMsg);
		safeLocationLayout = (LinearLayout) rootView.findViewById(R.id.safeLocationLinearLayout);
		btnSave = (Button) rootView.findViewById(R.id.btnSave);
		recordTypeRadio = (RadioGroup) rootView.findViewById(R.id.recordTypeRadio);
		layMeter=(LinearLayout)rootView.findViewById(R.id.meterLay);
		tvMeter=(TextView)rootView.findViewById(R.id.tvMeter);
		tvFeet=(TextView)rootView.findViewById(R.id.tvFeet);
		switchAudio=(Switch)rootView.findViewById(R.id.switchAudio);
		rdoAudio=(RadioButton)rootView.findViewById(R.id.radioAudio);

		chkCall.setChecked(true);
		chkEmail.setChecked(true);
		chkText.setChecked(true);
		chkRingtone.setChecked(true);
		chkVibrate.setChecked(true);
		//chkShake.setChecked(true);
		
	//	if(sharedPreferences.getBoolean("checkIn", false)||sharedPreferences.getBoolean("deActivate", false))
	//	{
			//showAlertCheckIn();
			//Toast.makeText(getActivity(), sharedPreferences.getBoolean("deActivate", false)+"", Toast.LENGTH_SHORT).show();
	//	}
         deActivate=sharedPreferences.getBoolean("deActivate", false);
		recordTypeRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub				
				if(checkedId == R.id.radioAudio){
					editor.putBoolean("recordTypeVideo", false);
					editor.putBoolean("recordTypeAudio", true);
				}else if(checkedId == R.id.radioVideo) {
					editor.putBoolean("recordTypeVideo", true);
					editor.putBoolean("recordTypeAudio", false);
				}else if(checkedId == R.id.radioNone){
					editor.putBoolean("recordTypeVideo", false);
					editor.putBoolean("recordTypeAudio", false);
				}												
			}
		});	              
		if(sharedPreferences.getString("switchStatus", "false").equals("true"))
		{
			switchAudio.setChecked(true);
			switchStatus=true;		

			//			initializeApplicationConstants();
			//
			//			if (!mRunning) {
			//				mRunning = true;
			//				start();
			//			}
			//			Intent svc =new Intent(getActivity(), myservice.class);
			//			getActivity().startService(svc);
//			Intent svc =new Intent(getActivity(), myservice.class);
//			getActivity().startService(svc);
			
			editor.putString("previoustime","").apply();

			Intent svc =new Intent(getActivity(), MouthService.class);
			getActivity().startService(svc);

		}
		else
		{
			switchAudio.setChecked(false);
			switchStatus=false;
			
			MouthService.isServiceWorking = false;
			
			//			stop();
//			Intent svc =new Intent(getActivity(), myservice.class);
//			getActivity().stopService(svc);
			Intent svc =new Intent(getActivity(), MouthService.class);
			getActivity().stopService(svc);
		}
		switchAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
				{
					editor.putString("switchStatus", "true").apply();
					switchStatus=true;		
					MouthService.isServiceWorking = true;
					//					//			        
					//					 Thread t = new Thread(){
					//	                        public void run(){
					Intent svc =new Intent(getActivity(), MouthService.class);
					getActivity().startService(svc);
                    editor.putString("previoustime","").apply();

					//	                        }
					//	                    };
					//	                    t.start();
					//					AlertActions alertActions=new AlertActions(getActivity());
					//					alertActions.activateNotificationAlert(true);
				}
				else
				{
					editor.putString("switchStatus", "false").apply();
					switchStatus=false;	
					
					MouthService.isServiceWorking = false;
					
					Intent svc =new Intent(getActivity(), MouthService.class);
					getActivity().stopService(svc);
					//                     
					//               
					//					
				}		        
			}
		});
		@SuppressWarnings("rawtypes")
		ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.check_in_interval_time, R.layout.spinner_item);
		adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		checkinTimerSpinner.setAdapter(adapter);
		checkinTimerSpinner.setSelection(2);		 

		adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.location_radius, R.layout.spinner_item);
		adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		locationRadiusSpinner.setAdapter(adapter);	
		locationRadiusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
			{
				String selectedItem = parent.getItemAtPosition(position).toString();		            
				if(selectedItem.endsWith("1 kms"))
				{
					tvMeter.setText(String.valueOf(1000+" mts"));
					tvFeet.setText(String.valueOf(1000*3 +" feet"));
				}
				else if(selectedItem.equals("2 kms"))
				{
					tvMeter.setText(String.valueOf(2000+" mts"));
					tvFeet.setText(String.valueOf(2000*3 +" feet"));
				}
				else if(selectedItem.equals("5 kms"))
				{
					tvMeter.setText(String.valueOf(5000+" mts"));
					tvFeet.setText(String.valueOf(5000*3 +" feet"));
				}
				else if(selectedItem.equals("10 kms"))
				{
					tvMeter.setText(String.valueOf(10000+" mts"));
					tvFeet.setText(String.valueOf(10000*3 +" feet"));
				}
				else
				{
					tvMeter.setText(String.valueOf(500+" mts"));
					tvFeet.setText(String.valueOf(500*3 +" feet"));
				}		         		            		            		            
			} // to close the onItemSelected
			public void onNothingSelected(AdapterView<?> parent) 
			{

			}           
		});		
		leaveSafeLocationCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
				{
					safeLocationLayout.setVisibility(View.VISIBLE);
					layMeter.setVisibility(View.VISIBLE);
				}
				else
				{
					safeLocationLayout.setVisibility(View.GONE);
					layMeter.setVisibility(View.GONE);
				}
			}
		});
		setValues();		
		btnSave.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				btnSaveClicked();
			}
		});        
		return rootView;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}

	//	private void start() {
	//		//Log.i("Noise", "==== start ===");
	//
	//		try{
	//			if (!mWakeLock.isHeld()) {
	//				mWakeLock.acquire();
	//			}
	//
	//			//Noise monitoring start
	//			// Runnable(mPollTask) will execute after POLL_INTERVAL
	//			mHandler.postDelayed(mPollTask, POLL_INTERVAL);
	//		}catch(Exception e){
	//			e.getCause();
	//		}
	//		mSensor.start();
	//		
	//	}
	//
	//	private void stop() {
	//		Log.i("Noise", "==== Stop Noise Monitoring===");
	//		if (mWakeLock.isHeld()) {
	//			mWakeLock.release();
	//		}
	//		mHandler.removeCallbacks(mSleepTask);
	//		mHandler.removeCallbacks(mPollTask);
	//		mSensor.stop();
	//
	//		mRunning = false;
	//
	//	}


	public void btnSaveClicked(){
		
		/*if(switchAudio.isChecked())
		{
			editor.putString("switchStatus", "true").apply();
			switchStatus=true;		

			MouthService.isServiceWorking = true;
			//					//			        
			//					 Thread t = new Thread(){
			//	                        public void run(){

			Intent svc =new Intent(getActivity(), MouthService.class);
			getActivity().startService(svc);	
		}*/

		if(fromFirstActivity){
			saveValues();
			alertActions.activateFakeCallAlert(true);
			FirstActivity.instance.setScreen();			
			Toast.makeText(getActivity().getApplicationContext(), "Settings saved Successfully", Toast.LENGTH_LONG).show();
			editor.putBoolean("checkIn", false).apply();
			editor.putBoolean("deActivate", false).apply();			
		}
		else{
			Intent confirmActivity = new Intent(getActivity().getApplicationContext(),AlertDialogConfirm.class);
			confirmActivity.putExtra("fromMain", false);			
			confirmActivity.putExtra("preset", true);			
			startActivityForResult(confirmActivity, 1);			
		}		
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == Activity.RESULT_OK){
			saveValues();
			boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
			boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
			
			//Toast.makeText(getActivity(), sharedPreferences.getBoolean("deActivate", false)+"", Toast.LENGTH_SHORT).show();

			if(((!notificationAlertSet && !alertSet) || !alertSet)&&!deActivate)
			{
				alertActions.activateFakeCallAlert(false);
			}
			else if((alertSet && presentTime != previousTime)&&!deActivate)
			{
				alertActions.activateFakeCallAlert(false);
			}
			//MainActivity.instance.update();
			Toast.makeText(getActivity().getApplicationContext(), "Settings saved Successfully", Toast.LENGTH_LONG).show();
		}else if(resultCode == Activity.RESULT_CANCELED){
			Toast.makeText(getActivity().getApplicationContext(), "Your change(s) has been discarded", Toast.LENGTH_LONG).show();
			setValues();
		}
	}
	public void saveValues(){
		try{
			String checkinTime = checkinTimerSpinner.getSelectedItem().toString();
			timerJSON = new JSONObject();
			editor=sharedPreferences.edit();

			if(checkinTime.equals("15 mins")){
				timerJSON.put("position",0);
				timerJSON.put("value", 15*60);
				//timerJSON.put("value", 10);
				presentTime = 15*60;
				editor.putBoolean("minute",true).apply();
			}
			else if(checkinTime.equals("30 mins")){
				timerJSON.put("position",1);
				timerJSON.put("value", 30*60);  
				presentTime = 15*60;
				editor.putBoolean("minute",true).apply();

			}
			else if(checkinTime.equals("1 hr")){

				timerJSON.put("position",2);
				timerJSON.put("value", 60*60);
				presentTime = 15*60;
				editor.putBoolean("minute",true).apply();

			}
			else if(checkinTime.equals("8 hrs")){
				timerJSON.put("position",3);
				timerJSON.put("value", 8*60*60);
				presentTime = 15*60;
				editor.putBoolean("minute",true).apply();
			}
			else{
				timerJSON.put("position",4);
				timerJSON.put("value", 24*60*60);
				presentTime = 15*60;
				editor.putBoolean("minute",false).apply();

			}			
			editor.putString("checkinTimer", timerJSON.toString());			
			if(leaveSafeLocationCheckBox.isChecked()){
				String safeLocationRadius = locationRadiusSpinner.getSelectedItem().toString();
				radiusJSON = new JSONObject();
				if(safeLocationRadius.equals("0 mts")){
					radiusJSON.put("position",0);
					radiusJSON.put("value", 5);
				}
				else if(safeLocationRadius.equals("500 mts")){
					radiusJSON.put("position",1);
					radiusJSON.put("value", 500);
				}
				else if(safeLocationRadius.equals("1 kms")){
					radiusJSON.put("position",2);

					radiusJSON.put("value", 1000);
				}
				else if(safeLocationRadius.equals("2 kms")){
					radiusJSON.put("position",3);
					radiusJSON.put("value", 2000);
				}
				else if(safeLocationRadius.equals("5 kms")){
					radiusJSON.put("position",4);
					radiusJSON.put("value", 5000);
				}
				else{
					radiusJSON.put("position",5);
					radiusJSON.put("value", 10000);
				}
				editor.putString("safeLocationRadius", radiusJSON.toString());
			}
			editor.putBoolean("isLeaveSafeLocationChecked", leaveSafeLocationCheckBox.isChecked());			
			editor.putBoolean("isEmailChecked", chkEmail.isChecked());
			editor.putBoolean("isTextChecked", chkText.isChecked());
			editor.putBoolean("isCallChecked", chkCall.isChecked());
			editor.putBoolean("isVibrateChecked", chkVibrate.isChecked());
			editor.putBoolean("isRingtoneChecked", chkRingtone.isChecked());
			editor.putBoolean("isShakeChecked", chkShake.isChecked());			

			editor.putString("personalMessage", personalAlertMessageEdt.getText().toString());

			editor.putBoolean("isSettingsCompleted", true);


			if(recordTypeRadio.getCheckedRadioButtonId() == R.id.radioAudio){
				editor.putBoolean("recordTypeVideo", false);
				editor.putBoolean("recordTypeAudio", true);
			}
			else if(recordTypeRadio.getCheckedRadioButtonId() == R.id.radioVideo){
				editor.putBoolean("recordTypeVideo", true);
				editor.putBoolean("recordTypeAudio", false);
			}else{
				editor.putBoolean("recordTypeVideo", false);
				editor.putBoolean("recordTypeAudio", false);
			}				
			/*if(switchStatus)
			{
				if(!sharedPreferences.getBoolean("isThresholdServiceStarted", false))
				{
				getActivity().getApplicationContext().startService(new Intent(getActivity().getApplicationContext(), ThreasholdService.class));
				editor.putBoolean("isThresholdServiceStarted", true);	
				}		
			}
			else
			{
				getActivity().getApplicationContext().stopService(new Intent(getActivity().getApplicationContext(), ThreasholdService.class));
				editor.putBoolean("isThresholdServiceStarted", false);									
			}*/
			if(chkShake.isChecked()){
				if(!sharedPreferences.getBoolean("isShakeServiceStarted", false)){
					getActivity().getApplicationContext().startService(new Intent(getActivity().getApplicationContext(), ShakeService.class));
					editor.putBoolean("isShakeServiceStarted", true);
				}
			}else{
				if(sharedPreferences.getBoolean("isShakeServiceStarted", false)){
					getActivity().getApplicationContext().stopService(new Intent(getActivity().getApplicationContext(), ShakeService.class));
					editor.putBoolean("isShakeServiceStarted", false);
				}
			}

			editor.commit();
		}catch(Exception e){
			Log.d("Settings Exception", e.toString());
		}
	}

	public void setValues(){
		personalAlertMessageEdt.setText(sharedPreferences.getString("personalMessage", ""));
		try {
			timerJSON  = new JSONObject(sharedPreferences.getString("checkinTimer", "{}"));

			Log.d("timerJson", timerJSON.toString()+"\n"+timerJSON.length()+"\n"+timerJSON.getInt("position"));

			if(timerJSON.length() == 2){
				checkinTimerSpinner.setSelection(timerJSON.getInt("position"));
				previousTime = timerJSON.getInt("value");
			}

			chkEmail.setChecked(sharedPreferences.getBoolean("isEmailChecked", false));
			chkText.setChecked(sharedPreferences.getBoolean("isTextChecked", false));
			chkCall.setChecked(sharedPreferences.getBoolean("isCallChecked", false));
			chkVibrate.setChecked(sharedPreferences.getBoolean("isVibrateChecked", false));
			chkRingtone.setChecked(sharedPreferences.getBoolean("isRingtoneChecked", false));
			chkShake.setChecked(sharedPreferences.getBoolean("isShakeChecked", false));

			if(sharedPreferences.getBoolean("recordTypeVideo", false))
				recordTypeRadio.check(R.id.radioVideo);
			else if(sharedPreferences.getBoolean("recordTypeAudio", false))
				recordTypeRadio.check(R.id.radioAudio);
			else
				recordTypeRadio.check(R.id.radioNone);

			if(sharedPreferences.getBoolean("isLeaveSafeLocationChecked", false)){
				leaveSafeLocationCheckBox.setChecked(true);

				radiusJSON = new JSONObject(sharedPreferences.getString("safeLocationRadius", "{}"));

				if(radiusJSON.length() == 2)
					locationRadiusSpinner.setSelection(radiusJSON.getInt("position")-1);
			}			
			personalAlertMessageEdt.setText(sharedPreferences.getString("personalMessage", ""));				
			if(sharedPreferences.getBoolean("QuickMeeting", false))
			{
				checkinTimerSpinner.setSelection(1);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);	
				rdoAudio.setChecked(true);
				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}
			}
			if(sharedPreferences.getBoolean("Trip", false))
			{
				checkinTimerSpinner.setSelection(2);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);					
				rdoAudio.setChecked(true);
				//rdoAudio.setEnabled(false);
				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}				
			}
			if(sharedPreferences.getBoolean("OnceDay", false))
			{
				checkinTimerSpinner.setSelection(4);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);	
				rdoAudio.setChecked(true);
				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}
			}												
		} catch (JSONException e) {
			Log.d("Exception Settings", e.toString());

			if(sharedPreferences.getBoolean("QuickMeeting", false))
			{
				checkinTimerSpinner.setSelection(1);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);	
				rdoAudio.setChecked(true);

				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}
			}
			if(sharedPreferences.getBoolean("Trip", false))
			{
				checkinTimerSpinner.setSelection(2);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);					
				rdoAudio.setChecked(true);
				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);

				//rdoAudio.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}

			}
			if(sharedPreferences.getBoolean("OnceDay", false))
			{
				checkinTimerSpinner.setSelection(4);
				checkinTimerSpinner.setEnabled(false);
				chkShake.setChecked(true);
				chkShake.setEnabled(false);	
				rdoAudio.setChecked(true);
				leaveSafeLocationCheckBox.setChecked(true);
				locationRadiusSpinner.setSelection(1);
				locationRadiusSpinner.setEnabled(false);					
				leaveSafeLocationCheckBox.setEnabled(false);
				for(int i = 0; i < recordTypeRadio.getChildCount(); i++){
					((RadioButton)recordTypeRadio.getChildAt(i)).setEnabled(false);
				}
			}}		
	}	
	//	public void onResume() {
	//		super.onResume();
	//		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	//		 initializeApplicationConstants();
	//         
	//         if (!mRunning) {
	//             mRunning = true;
	//             start();
	//         }
	//	}

	//	 private void start() {
	// 	    //Log.i("Noise", "==== start ===");
	//		 try
	//		 {
	//         mSensor.start();
	//         if (!mWakeLock.isHeld()) {
	//                 mWakeLock.acquire();
	//         }
	//         
	//         //Noise monitoring start
	//         // Runnable(mPollTask) will execute after POLL_INTERVAL
	//         mHandler.postDelayed(mPollTask, POLL_INTERVAL);
	//		 }
	//		 catch(Exception e)
	//		 {
	//			 
	//		 }
	// }
	//	 private void callForHelp() {
	//        
	//         //stop();
	//   	 // Show alert when noise thersold crossed
	//		 if(switchStatus)
	//		 {			 
	//	     alertActions.activateNotificationAlert(true);
	//          Toast.makeText(getActivity(), "CIBS Alert Activated", 
	//   			  Toast.LENGTH_LONG).show();
	//          if (mWakeLock.isHeld()) {
	//               mWakeLock.release();
	//           }
	//       mHandler.removeCallbacks(mSleepTask);
	//       mHandler.removeCallbacks(mPollTask);             
	//       mSensor.stop();
	//       mRunning = false;   
	//		 }		          
	//		 
	//   }
	//	 private void initializeApplicationConstants() {
	//         // Set Noise Threshold
	// 	    mThreshold = 9.0;
	//         
	// }
	//	 private void stop() {
	//     	Log.i("Noise", "==== Stop Noise Monitoring===");
	//             if (mWakeLock.isHeld()) {
	//                     mWakeLock.release();
	//             }
	//             mHandler.removeCallbacks(mSleepTask);
	//             mHandler.removeCallbacks(mPollTask);
	//             mSensor.stop();
	//             mRunning = false;
	//            
	//     } 
	//	 @Override
	//     public void onStop() {
	//             super.onStop();
	//            // Log.i("Noise", "==== onStop ===");
	//             //Stop noise monitoring
	//             stop();
	//            
	//     }
	public void getContactsNumbers(){
		try {
			ArrayList<String> contactsNumbersList=new ArrayList<String>();
			JSONObject contactsJSON = new JSONObject(sharedPreferences.getString("contactsJSON", "{}"));
			Log.v("ContactsJson", contactsJSON.toString());
			@SuppressWarnings("rawtypes")
			Iterator it = contactsJSON.keys();
			while(it.hasNext()){
				String contactName = it.next().toString();
				JSONObject contactJSON = new JSONObject(contactsJSON.getString(contactName));
				if(contactJSON.getString("phone").length()>=7){
					//if(firstContact.equalsIgnoreCase(contactName))
					//firstContactNumber = contactJSON.getString("phone");
					contactsNumbersList.add(contactJSON.getString("phone"));
				}

			}
		} catch (JSONException e) {
			Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
			Log.v("getContacts", e.toString());
		}
	}

	public void showAlertCheckIn(){			
		AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
		.setTitle("CIBS not activated!!")
       .setMessage("Do you want to activate CIBS?")
       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,int which) {
				Intent alertActivity = new Intent(getActivity(),AlertDialogActivity.class);
				alertActivity.putExtra("callFrom", "activateAlertBtn");
				alertActivity.putExtra("showMsg", true);
				editor.putBoolean("deActivate", false).apply();
				editor.putBoolean("checkIn", false).apply();
				//startActivity(alertActivity);
				
				startActivityForResult(alertActivity, 5);
				//Fragment fragment = new PresetSettingsFragment(false);

				//FragmentManager fragmentManager = getFragmentManager();
				//fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
				//Intent i=new Intent(getActivity(),MainActivity.class);
				//startActivity(i);
				//mDrawerList.setItemChecked(position, true);
				//mDrawerList.setSelection(position);
				//setTitle(navMenuTitles[position]);	
				
				/*Fragment fragment = new PresetSettingsFragment(false);

				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);	
				*/
				
			}	            	
       })
       .setNegativeButton("No", new DialogInterface.OnClickListener() {
       	public void onClick(DialogInterface dialog, int which) {
       		Toast.makeText(getActivity(), "Hint: Please Check In to Re-Activate CIBS", Toast.LENGTH_LONG).show();
       		dialog.dismiss();
       	}
       })
       .setCancelable(false)
       .create();
		alertDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
//				showRollCallAlertDialog();
			}
		});
		alertDialog.show();			
	
}
	
}


