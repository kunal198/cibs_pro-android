package com.visionarytechsolutions.cibs1Pro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class QuickSetupOptions extends Activity {
   Button btnNext;
   SharedPreferences sharedPreferences;
   Editor editor;
   CheckBox chkQuickMeeting,chkTrip,chkOnceDay;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quick_setup_options);
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();
		btnNext=(Button)findViewById(R.id.btnNext);
		chkQuickMeeting=(CheckBox)findViewById(R.id.chkQuickMeeting);
		chkTrip=(CheckBox)findViewById(R.id.chkTrip);
		chkOnceDay=(CheckBox)findViewById(R.id.chkOnce);			
		chkQuickMeeting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkTrip.setChecked(false);
		    		   chkOnceDay.setChecked(false);
		    		   editor.putBoolean("QuickMeeting", true).apply();
		    		   editor.putBoolean("Trip", false).apply();
		    		   editor.putBoolean("OnceDay", false).apply();		    		   
		    	   }
		       }
		   }
		);     
		chkTrip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkQuickMeeting.setChecked(false);
		    		   chkOnceDay.setChecked(false);		    		   
		    		   editor.putBoolean("QuickMeeting", false).apply();
		    		   editor.putBoolean("Trip", true).apply();
		    		   editor.putBoolean("OnceDay", false).apply();
		    	   }
		       }
		   }
		);     		
		chkOnceDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkTrip.setChecked(false);
		    		   chkQuickMeeting.setChecked(false);		    		   
		    		   editor.putBoolean("QuickMeeting", false).apply();
		    		   editor.putBoolean("Trip", false).apply();
		    		   editor.putBoolean("OnceDay", true).apply();
		    	   }
		       }
		   }
		);     						
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(QuickSetupOptions.this,FirstActivity.class);
				startActivity(i);
				finish();
				sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		        editor = sharedPreferences.edit();
		        editor.putString("watchDemo", "true").apply();		        
		        Log.e("QuickSetup",""+sharedPreferences.getBoolean("QuickMeeting", false));
		        Log.e("QuickSetup",""+sharedPreferences.getBoolean("Trip", false));
		        Log.e("QuickSetup",""+sharedPreferences.getBoolean("OnceDay", false));		        
		        
			}
		});
	}
}
