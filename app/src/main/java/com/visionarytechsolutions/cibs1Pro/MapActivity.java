package com.visionarytechsolutions.cibs1Pro;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.visionarytechsolutions.cibs1Pro.R;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {
	Button mapBtn, hybridBtn, satelliteBtn, setLocation, myLocation, findLocationBtn;
	MapFragment map;
	GoogleMap googleMap;
	LocationManager locationManager;
	Location location, selectedLocation, addressLocation;
	Bitmap userLocationIcon, selectedLocationIcon;
	Marker selectedLocationMarker, userLocationMarker;
	MarkerOptions markerOptions;
	LatLng latLng;
	EditText searchLocationEdt;
	ArrayList<Marker> previousSearchMarkers = new ArrayList<Marker>();
	RelativeLayout mainLayout;
	SharedPreferences sharedPreferences;
	Editor editor;
	String userAddressString, selectedAddressString, callingMtd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_screen);
		userLocationIcon = BitmapFactory.decodeResource(MapActivity.this.getResources(), R.drawable.user_location_new);
		userLocationIcon = Bitmap.createScaledBitmap(userLocationIcon, 20, 25, false);
		mainLayout = (RelativeLayout) findViewById(R.id.map_screen_MainRelativelay);

		selectedLocationIcon = BitmapFactory.decodeResource(MapActivity.this.getResources(), R.drawable.user_selected_location_new);
		selectedLocationIcon = Bitmap.createScaledBitmap(selectedLocationIcon, 20, 25, false);

		selectedLocation = new Location("dummyprovider");

		getActionBar().hide();

		map = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		map.getMapAsync(this);

		mapBtn = (Button) findViewById(R.id.mapType);
		hybridBtn = (Button) findViewById(R.id.hybridType);
		satelliteBtn = (Button) findViewById(R.id.satelliteType);
		setLocation = (Button) findViewById(R.id.setLocation);
		myLocation = (Button) findViewById(R.id.myLocation);
		findLocationBtn = (Button) findViewById(R.id.btn_find);
		searchLocationEdt = (EditText) findViewById(R.id.et_location);
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		if (sharedPreferences.getBoolean("Trip", false) || sharedPreferences.getBoolean("OnceDay", false)) {
			showlocationDialog();
		}
		searchLocationEdt.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if ((actionId & EditorInfo.IME_MASK_ACTION) == EditorInfo.IME_ACTION_DONE) {

					String location = searchLocationEdt.getText().toString();

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);

					if (location != null && !location.equals("")) {
						new GeocoderTask().execute(location);
					} else
						Toast.makeText(getApplicationContext(), "Enter location to search", Toast.LENGTH_LONG).show();
					return true;
				}
				return false;
			}
		});

		findLocationBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Getting user input location
				String location = searchLocationEdt.getText().toString();

				if (location != null && !location.equals("")) {
					new GeocoderTask().execute(location);
				} else
					Toast.makeText(getApplicationContext(), "Enter location to search", Toast.LENGTH_LONG).show();
			}
		});

		mapBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			}
		});
		hybridBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			}
		});
		satelliteBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			}
		});
		setLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				Toast.makeText(getApplicationContext(), "Lat:"+selectedLocation.latitude+"\nLon:"+selectedLocation.longitude, Toast.LENGTH_LONG).show();
//				String location="Lat:"+selectedLocation.latitude+"\nLon:"+selectedLocation.longitude;
				if (selectedLocation != null && selectedLocation.getLatitude() != 0.0) {
					Intent intent = getIntent();
					intent.putExtra("LOCATION", selectedLocation);
					intent.putExtra("addressString", selectedAddressString);
					intent.putExtra("NoClick", "no");
					setResult(2, intent);
					finish();//finishing activity
				} else
					Toast.makeText(getApplicationContext(), "Please select a location", Toast.LENGTH_LONG).show();
			}
		});
		myLocation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (location != null) {
					Intent intent = getIntent();
					intent.putExtra("LOCATION", location);
					intent.putExtra("addressString", userAddressString);
					intent.putExtra("NoClick", "no");
					setResult(2, intent);
					finish();//finishing activity
				} else
					Toast.makeText(getApplicationContext(), "Sorry, your location in not found, please turn on Location(GPS) services!", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onMapReady(GoogleMap googleMap1) {
		googleMap = googleMap1;

		//googleMap.getUiSettings().setZoomControlsEnabled(true);
		googleMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng point) {
				selectedLocation.setLatitude(point.latitude);
				selectedLocation.setLongitude(point.longitude);
				googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, googleMap.getCameraPosition().zoom));
				if (selectedLocationMarker != null) {
					selectedLocationMarker.remove();
				}
				selectedLocationMarker = googleMap.addMarker(new MarkerOptions()
						.icon(BitmapDescriptorFactory.fromBitmap(selectedLocationIcon))
						.anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
						.title("Selected Safe Location")
						.position(point));
				selectedLocationMarker.showInfoWindow();

				callingMtd = "selectedLocation";
				addressLocation = selectedLocation;
				new AddressGeocoderTask().execute();
				selectedLocationMarker.showInfoWindow();
			}
		});

//		googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
//			
//			@Override
//			public boolean onMarkerClick(Marker marker) {
//				return true;
//			}
//		});
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location == null){
        	try{
	        	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	        	location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER); 
        	}catch(Exception e){
        		Log.d("Exception", e.toString());
        	}
        }		
		if(location != null){
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 18));
			userLocationMarker = googleMap.addMarker(new MarkerOptions()
            .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
            .title("Your Current location")
            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
            .position(new LatLng(location.getLatitude(), location.getLongitude())));
			userLocationMarker.showInfoWindow();									
			callingMtd = "userLocation";
			addressLocation = location;
			new AddressGeocoderTask().execute();
			userLocationMarker.showInfoWindow();
			
		}
		else
		{
			showDefaultDialog();
		}
		
	}

	@Override
	public void onLocationChanged(Location location) {		
		double distance = 11.0;
		if(this.location!=null)
			distance = location.distanceTo(this.location);
		
		if(distance>=50.0){
			this.location = location;
			if(userLocationMarker != null)
				userLocationMarker.remove();
			userLocationMarker = googleMap.addMarker(new MarkerOptions()
	        .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
	        .title("Your Current location")
	        .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
	        .position(new LatLng(location.getLatitude(), location.getLongitude())));
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), googleMap.getCameraPosition().zoom));
			callingMtd = "userLocation";
			addressLocation = location;
			new AddressGeocoderTask().execute();
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}
	private class GeocoderTask extends AsyncTask<String, Void, List<Address>>{
		 
        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;
 
            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                Log.d("Exception", e.toString());
            }
            return addresses;
        }
 
        @Override
        protected void onPostExecute(List<Address> addresses) {
 
            if(addresses==null || addresses.size()==0){
                Toast.makeText(getApplicationContext(), "No Location found", Toast.LENGTH_SHORT).show();
                return;
            }
 
            // Clears all the existing markers on the map
            
            for (Marker marker : previousSearchMarkers) {
				marker.remove();
			}            
            
            previousSearchMarkers.removeAll(previousSearchMarkers);
            // Adding Markers on Google Map for each matching address
            for(int i=0;i<addresses.size();i++){
 
                Address address = (Address) addresses.get(i);
 
                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());
                
                selectedLocation.setLatitude(latLng.latitude);
				selectedLocation.setLongitude(latLng.longitude);
 
//                String addressText = String.format("%s, %s",
//                address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
//                address.getCountryName());
                
                selectedAddressString = "";
            	if(address.getAddressLine(0) != null)
            		selectedAddressString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(3);
            	if(!selectedAddressString.contains(address.getCountryName()))
            		selectedAddressString = selectedAddressString + "," + address.getCountryName();
 
                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(selectedAddressString);
 
                Marker marker = googleMap.addMarker(markerOptions);
                marker.showInfoWindow();
                
                previousSearchMarkers.add(marker);
 
                // Locate the first location
                if(i==0)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        }
    }
	
	
	private class AddressGeocoderTask extends AsyncTask<Void, Void, List<Address>>{
						 
        @Override
        protected List<Address> doInBackground(Void... params) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null; 
            try {
                addresses = geocoder.getFromLocation(addressLocation.getLatitude(), addressLocation.getLongitude(), 1);
            } catch (IOException e) {
                Log.d("Exception", e.toString());
            }
            return addresses;
        }
 
        @Override
        protected void onPostExecute(List<Address> addresses) {
 
            if(addresses==null || addresses.size()==0){                    		
               // Toast.makeText(getApplicationContext(), "No Location found", Toast.LENGTH_SHORT).show();
                return;
            }
            Address address = (Address) addresses.get(0);                       
            if(callingMtd.equalsIgnoreCase("userLocation")){
            	
            	userAddressString = "";
            	if(address.getAddressLine(0) != null)
            		userAddressString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(3);
            	if(!userAddressString.contains(address.getCountryName()))
            		userAddressString = userAddressString + "," + address.getCountryName();            	
            	
            	userLocationMarker.setTitle("Your Location:"+userAddressString);
            	
            	userLocationMarker.showInfoWindow();
            }	
            else if(callingMtd.equalsIgnoreCase("selectedLocation")){
            	//selectedAddressString = String.format("%s, %s",address.getAddressLine(0)+","+address.getAddressLine(1)+","+address.getAddressLine(2),address.getMaxAddressLineIndex() > 2 ? ","+address.getCountryName() : "");
            
            	selectedAddressString = "";
            	if(address.getAddressLine(0) != null)
            		selectedAddressString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(3);
            	if(!selectedAddressString.contains(address.getCountryName()))
            		selectedAddressString = selectedAddressString + "," + address.getCountryName();
            	
            	selectedLocationMarker.setTitle("SelectedLoc:"+selectedAddressString);
         
            	selectedLocationMarker.showInfoWindow();
            }
        } 
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.removeUpdates(this);
	}	
	void showDefaultDialog()
	{
		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setMessage("Please on your GPS from phone setting !");
		builder1.setCancelable(true);
		builder1.setPositiveButton(
		    "Ok",
		    new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int id) {
		            dialog.cancel();
		        }
		    });	
		AlertDialog alert11 = builder1.create();
		alert11.show();
      }
	void showlocationDialog()
	{
		new AlertDialog.Builder(this)
        .setTitle("CIBS PRO").setCancelable(false)
        .setMessage("Are you sure that you want to save current location as safe location ?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            	Log.e("yess","yess");
            	myLocation.performClick();            	
            }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            	Log.e("no","yess");
            	/*Intent intent=getIntent();
                intent.putExtra("LOCATION",location);
                intent.putExtra("addressString", userAddressString);
                intent.putExtra("NoClick", "yes");
                setResult(2,intent);  
                finish();*///finishing activity   		
            }
        })
        .show();	
	}
	
}