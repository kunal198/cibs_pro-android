package com.visionarytechsolutions.cibs1Pro;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.LocationActions;

public class AlertDialogActivity extends Activity{	
	public static AlertDialogActivity instance;
	EditText alertPassword;
	SharedPreferences sharedPreferences;
	Editor editor;
	int attempts = 0;
	AlertActions alertActions;
	String callFrom = "", imgPath = "";
	boolean checkinSuccess = false, deactivateSuccess = false,takeImage = false, showMsg = false, isAlreadyActivated = false;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	LocationActions locationActions;
	Handler handler;
	Runnable runnable;		
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    instance = this;	    
	    this.setFinishOnTouchOutside(false);
	    sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
	    editor = sharedPreferences.edit();
	    callFrom = getIntent().getStringExtra("callFrom"); 
	    showMsg = getIntent().getBooleanExtra("showMsg", false);
	    alertActions = new AlertActions(getApplicationContext());  
		setContentView(R.layout.alert_dialog);
		alertPassword = (EditText) findViewById(R.id.alertPassword);		
		locationActions = new LocationActions(getApplicationContext());		
		alertPassword.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId & EditorInfo.IME_MASK_ACTION) == EditorInfo.IME_ACTION_DONE) {                	
                	alertPasswordClicked(null);
                    return true;
                }
                return false;
            }
        });		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		autoCancelAlert();
	}
	public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/CIBS/", "CIBS_Selfie.jpg");
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
	
	public void alertPasswordClicked(View v) {
		String password = alertPassword.getText().toString();
		String userPwd = sharedPreferences.getString("password", "");
		if(password.equalsIgnoreCase(userPwd)){			
			if(callFrom.equalsIgnoreCase("checkinBtn")){
				checkinSuccess = true;
				AlertDialog alertDialog = new AlertDialog.Builder(AlertDialogActivity.this)
	            .setMessage("Do you want to take new picture now?")
	            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,
							int which) {			
						takeImage = true;
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
					    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
					        if(handler != null){ 
								handler.removeCallbacks(runnable);
								handler = null;
							}
					    }
					}	            	
	            })    
	            .setNegativeButton("No", new DialogInterface.OnClickListener() {
	            	public void onClick(DialogInterface dialog, int which) {
	            		doTasks();
	            	}
	            })
	            .setCancelable(false)
	            .create();
	    		alertDialog.show();
			}else if(callFrom.equalsIgnoreCase("deactivateBtn")){
				deactivateSuccess = true;
				editor.putBoolean("deActivate", true).commit();
				deactivateAlert();
			}else if(callFrom.equalsIgnoreCase("activateAlertBtn")){
				
				//Intent returnIntent = new Intent();
			     //setResult(Activity.RESULT_OK,returnIntent);
				alertActions.activateFakeCallAlert(showMsg);
				finish();
			}    		
		}else{
			attempts+=1;
			if(attempts != 3){
				Toast.makeText(getApplicationContext(), "Please Enter Correct Password, "+(3-attempts)+" attempts remaining", Toast.LENGTH_LONG).show();
				alertPassword.setText("");
			}
			else{
				if(!isAlreadyActivated){
					alertActions.activateNotificationAlert(true);
					isAlreadyActivated = true;
				}
				finish();
				if(FakeCallActivity.instance != null)
					FakeCallActivity.instance.finish();
			}
		}
	}	
	public void doTasks(){		
		if(!showMsg)
			Toast.makeText(getApplicationContext(), "Thanks for Check In", Toast.LENGTH_LONG).show();		
		Location checkedLocation = locationActions.getCurrentLocation();
		locationActions.cancelListener();		
		JSONObject locationJSON = new JSONObject();
		editor.putBoolean("deActivate", false).apply();

		
		//editor.putBoolean("checkIn", true).apply();
		//editor.putBoolean("showDialog", true).apply();
		
		//MainActivity obj=new MainActivity();
		//obj.update();
		
		/*MainActivity.instance.finish();
		Intent i=new Intent(getApplicationContext(),MainActivity.class);
		startActivity(i);*/

  	    try {
  	    	if(checkedLocation != null){
				locationJSON.put("lon", checkedLocation.getLongitude());
				locationJSON.put("lat", checkedLocation.getLatitude());
				editor.putString("lastCheckedLocationJSON", locationJSON.toString());
				SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm aaa");		     
			      String formattedDate = formatDate.format(new Date()).toString();			     
			     formattedDate=formattedDate.replace("p.m.", "PM").replace("a.m.", "AM");
			     Log.e("timeee",formattedDate);
				editor.putString("lastCheckedTime", formattedDate);
				//editor.putString("lastCheckedTime", new Date(System.currentTimeMillis()).toString());
				editor.commit();
  	    	}
		} catch (Exception e) {
			Log.d("Exception",e.toString());
		}
  	    
		alertActions.activateFakeCallAlert(showMsg);
		alertActions.deactivateNotificationAlert(false);
		finish();
		if(FakeCallActivity.instance != null){
			FakeCallActivity.instance.finish();
		}
		if(MainActivity.instance != null){
			MainActivity.instance.finish();
		}
	}
	
	public void deactivateAlert(){
		boolean notificationAlert = sharedPreferences.getBoolean("notificationAlertSet", false);				
		//alertActions.deactivateNotificationAlert(true);
		//alertActions.activateFakeCallAlert(true);		
		alertActions.deactivateNotificationAlert(notificationAlert);
		alertActions.deactivateFakeCallAlert(!notificationAlert);
		if(FakeCallActivity.instance != null){
			FakeCallActivity.instance.finish();
		}
		if(MainActivity.instance != null){
			MainActivity.instance.finish();
		}		
		finish();
	}	
	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		if(callFrom.equalsIgnoreCase("activateAlertBtn")){
			super.onBackPressed();
		}
	}	
	@Override
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();		
		if(!callFrom.equalsIgnoreCase("activateAlertBtn")){	
			if(!checkinSuccess && !deactivateSuccess){
				if(!isAlreadyActivated){
					alertActions.activateNotificationAlert(true);
					isAlreadyActivated = true;
				}
				finish();
				if(FakeCallActivity.instance != null)
					FakeCallActivity.instance.finish();
			}else if(checkinSuccess && !takeImage){
				doTasks();
			}
		}		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(handler != null){ 
			handler.removeCallbacks(runnable);
		}
	}	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {	
	        editor.putString("snapshotImagePath", imgPath);
	        editor.putString("snapshotVideoPath", "");

	        
	        editor.commit();
	    }
	    doTasks();
	}	
	public void autoCancelAlert(){
		
		runnable = new  Runnable() {
            @Override
            public void run() {
            	if(!callFrom.equalsIgnoreCase("activateAlertBtn")){	
        			if(!checkinSuccess && !deactivateSuccess){
        				if(!isAlreadyActivated){
        					alertActions.activateNotificationAlert(true);
        					isAlreadyActivated = true;      						
        				}
        				finish();
        				if(FakeCallActivity.instance != null)
        					FakeCallActivity.instance.finish();
        			}else if(checkinSuccess && !takeImage){
        				doTasks();
        			}
        		}
            }
        };      
		runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	handler = new Handler();
		    	handler.postDelayed(runnable, 30000);
		    }
		});
	}
}
