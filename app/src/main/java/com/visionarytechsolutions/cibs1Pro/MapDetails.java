package com.visionarytechsolutions.cibs1Pro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.visionarytechsolutions.cibs1Pro.utils.HttpConnection;
import com.visionarytechsolutions.cibs1Pro.utils.PathJSONParser;
public class MapDetails extends FragmentActivity implements OnMapReadyCallback {

	private static  LatLng CURRENT_LOCATION;
	private static  LatLng SELECTED_LOCATION; 
	String currentLocation,selectedLocation,placeId,title,addressString;
	Location currentLocationLat,selectedLocationLat,addressLocation,selectedLction;
	MapFragment map;

	GoogleMap googleMap;
	ProgressDialog dialog;
	TextView tvDistance,tvMobileNumber;
	Bitmap userLocationIcon,selectedLocationIcon,safeLocationIcon;
	String contactNumber="";
	Button btnShare,btnLocation;
	SharedPreferences sharedPreferences;
	Editor editor;
	String currentLat,currentLng; 
	CharSequence optionsText[] = new CharSequence[] {"Default Message", "Custom Message"};
	String message="";
	Marker selectedLocationMarker;
     String callingMtd,userAddressString,selectedAddressString;
     Marker userLocationMarker;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_details);
		map = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		map.getMapAsync(this);
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();
		tvDistance=(TextView)findViewById(R.id.tvDistance);		
		btnShare=(Button)findViewById(R.id.btnShare);			   	  
		tvMobileNumber=(TextView)findViewById(R.id.tvMobileNumber);
		btnLocation=(Button)findViewById(R.id.btnLocation);
          selectedLction = new Location("dummyprovider");

	    userLocationIcon = BitmapFactory.decodeResource(MapDetails.this.getResources(), R.drawable.user_location_new);
        userLocationIcon = Bitmap.createScaledBitmap(userLocationIcon, 30, 30, false);
        
        selectedLocationIcon = BitmapFactory.decodeResource(MapDetails.this.getResources(), R.drawable.user_selected_location_new);
        selectedLocationIcon = Bitmap.createScaledBitmap(selectedLocationIcon, 20, 25, false);
        
        safeLocationIcon = BitmapFactory.decodeResource(MapDetails.this.getResources(), R.drawable.user_location_green);
        safeLocationIcon = Bitmap.createScaledBitmap(safeLocationIcon, 20, 25, false);
        

		currentLocation=getIntent().getStringExtra("currentLocation").trim();
		selectedLocation=getIntent().getStringExtra("selectedLocation").trim();
		placeId=getIntent().getStringExtra("placeId").trim();
		title=getIntent().getStringExtra("title").trim();
		addressString=getIntent().getStringExtra("userAddressString").trim();

		

		dialog=new ProgressDialog(MapDetails.this);
		dialog.setMessage("Loading...");
		dialog.setCancelable(false);
		dialog.show();
		new PlaceDetail().execute();

		Log.e("current loca",currentLocation);
		Log.e("selectedLocation",""+selectedLocation);

		String location[]=currentLocation.split(",");		
		currentLocationLat=new Location("");
		currentLocationLat.setLatitude(Double.parseDouble(location[0].trim()));
		currentLocationLat.setLongitude(Double.parseDouble(location[1].trim()));
		currentLat=location[0];
		currentLng=location[1];
		
		CURRENT_LOCATION=new LatLng(Double.parseDouble(location[0].trim()), Double.parseDouble(location[1].trim()));		
		location=selectedLocation.split(",");	
		
		SELECTED_LOCATION=new LatLng(Double.parseDouble(location[0].trim()), Double.parseDouble(location[1].trim()));	
		MarkerOptions options = new MarkerOptions();
		selectedLocationLat=new Location("");
		selectedLocationLat.setLatitude(Double.parseDouble(location[0].trim()));
		selectedLocationLat.setLongitude(Double.parseDouble(location[1].trim()));
		//options.position(CURRENT_LOCATION);
		//options.position(SELECTED_LOCATION);
		//googleMap.addMarker(options);
		tvDistance.setText("Distance from safe location is -> "+currentLocationLat.distanceTo(selectedLocationLat)/1000+" kms");
		Log.e("totaldistance",""+currentLocationLat.distanceTo(selectedLocationLat)/1000);
		String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+currentLocation+"&destination="+selectedLocation+"&%20waypoints=optimize:true&sensor=false";
		ReadTask downloadTask = new ReadTask();
		downloadTask.execute(url);
		/*googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_LOCATION,
				13));
		addMarkers();*/
		btnShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				AlertDialog.Builder builder = new AlertDialog.Builder(MapDetails.this);
				builder.setTitle("Select Choice");
				builder.setItems(optionsText, new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        // the user clicked on colors[which]
				    	Log.e("detailss",""+which);
				    	if(which==0)
				    		showDefaultDialog();
				    	else
				    		showCustomDialog();
				    	
				    }
				});
				builder.show();
			}
		});
		btnLocation.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveLocation();				
			}
		});				
	}
	private void addMarkers() {
		if (googleMap != null) {
			
			/*
			googleMap.setOnMapClickListener(new OnMapClickListener() {
				
				@Override
				public void onMapClick(LatLng point) {			
					selectedLction.setLatitude(point.latitude);
					selectedLction.setLongitude(point.longitude);
					currentLat=String.valueOf(point.latitude);
					currentLng=String.valueOf(point.longitude);
					googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, googleMap.getCameraPosition().zoom));
					if(selectedLocationMarker != null){
						selectedLocationMarker.remove();					
					}
					selectedLocationMarker = googleMap.addMarker(new MarkerOptions()
			        .icon(BitmapDescriptorFactory.fromBitmap(safeLocationIcon))
			        .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
			        .title("Selected Safe Location")
			        .position(point));				
					selectedLocationMarker.showInfoWindow();
					
					callingMtd = "selectedLocation";
					addressLocation = selectedLction;
					new AddressGeocoderTask().execute();
					selectedLocationMarker.showInfoWindow();
				}
			});*/
			
			  userLocationMarker = googleMap.addMarker(new MarkerOptions()
	            .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
	            .title("Current Location")
	            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
	            .position(CURRENT_LOCATION));
				 userLocationMarker.showInfoWindow();			
			
			//googleMap.addMarker(new MarkerOptions().position(CURRENT_LOCATION)
					//.title("Current Location"));
				  userLocationMarker = googleMap.addMarker(new MarkerOptions()
		            .icon(BitmapDescriptorFactory.fromBitmap(userLocationIcon))
		            .title(title)
		            .anchor(0.0f, 0.0f) // Anchors the marker on the bottom left
		            .position(SELECTED_LOCATION));
					 userLocationMarker.showInfoWindow();
				 
			//googleMap.addMarker(new MarkerOptions().position(SELECTED_LOCATION)
					//.title("Safe or hospital location"));
			//googleMap.addMarker(new MarkerOptions().position(WALL_STREET)
					//.title("Third Point"));
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMp) {
		googleMap=googleMp;

		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_LOCATION,
				13));
		addMarkers();
	}

	private class ReadTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... url) {
			String data = "";
			try {
				HttpConnection http = new HttpConnection();
				data = http.readUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			new ParserTask().execute(result);
		}
	}

	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				PathJSONParser parser = new PathJSONParser();
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
			ArrayList<LatLng> points = null;
			PolylineOptions polyLineOptions = null;
			// traversing through routes
			for (int i = 0; i < routes.size(); i++) {
				points = new ArrayList<LatLng>();
				polyLineOptions = new PolylineOptions();
				List<HashMap<String, String>> path = routes.get(i);
				for (int j = 0; j < path.size(); j++) {
					HashMap<String, String> point = path.get(j);

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);
					points.add(position);
					Log.e("polyyylinessss",position+"");

				}
				polyLineOptions.addAll(points);
				polyLineOptions.width(6);
				polyLineOptions.color(Color.BLUE);
			}

			googleMap.addPolyline(polyLineOptions);
		}
	}
	class PlaceDetail extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			 StringBuilder response  = new StringBuilder();
			    URL url;
				try {
				url = new URL("https://maps.googleapis.com/maps/api/place/details/json?placeid="+placeId+"&key=AIzaSyAbdJ0J4G5REpnIBTbruMReqSpajpWJC2I");						
			    HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
			    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
			    {
			        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
			        String strLine = null;
			        while ((strLine = input.readLine()) != null)
			        {
			            response.append(strLine);
			        }
			        input.close();
			        Log.e("detailss",response+"");
			        JSONObject obj=new JSONObject(response.toString());
			        final JSONObject result=obj.getJSONObject("result");
			        if(result.has("formatted_phone_number"))
			        {
			        	contactNumber=result.getString("formatted_phone_number");			        	
			        }
			        Log.e("numberere",result.getString("formatted_phone_number"));
			    }}
			     catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				
			    }
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			if(!contactNumber.equals("")){
	        	tvMobileNumber.setText("Contact Number is -> "+contactNumber);
			}
			else
			{
	        	tvMobileNumber.setText("Contact Number is not available");	
			}
			tvMobileNumber.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub								
					 String uri;
					try {
						if(!contactNumber.equals(""))
						{
					 uri = "tel:" + contactNumber;								
					 Intent intent = new Intent(Intent.ACTION_CALL);
					 intent.setData(Uri.parse(uri));
					 startActivity(intent);
					}} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});	
			showlocationDialog();
		}		
	}
	public void saveLocation(){
    	try {		   
    	   JSONObject safeLocationJSON = new JSONObject(sharedPreferences.getString("safeLocationJSON", "{}"));					
     	   JSONObject locationJSON = new JSONObject();
     	   locationJSON.put("lat", currentLat);
     	   locationJSON.put("lon", currentLng);
     	   locationJSON.put("addressString", addressString);
     	   //Log.e("detailss",selectedLocation.getLatitude()+"");
     	   //Log.e("detailss1",selectedLocation.getLongitude()+"");
		   safeLocationJSON.put("address"+safeLocationJSON.length(), locationJSON);
		   editor.putString("safeLocationJSON", safeLocationJSON.toString());
		   editor.commit();
		   Toast.makeText(getApplicationContext(), "Current Location is saved as safe location", Toast.LENGTH_SHORT).show();
		} catch (JSONException e) {
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
		}    	
        
    }
	void showDefaultDialog()
	{
		final CharSequence[] defaultMessage = {"I don’t feel safe","Someone is following me","I need help","Can you come by","I’m not feeling well"};
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Select Default Message");
		alert.setItems(defaultMessage, new 
		DialogInterface.OnClickListener()
		{
		    @Override
		    public void onClick(DialogInterface dialog, int which) 
		    {
		    	dialog.dismiss();
		    	message=String.valueOf(defaultMessage[which]);
		    	Log.e("message",""+message);	
		    	shareDialog();
		    }
		});
		alert.show();
	}
	void showCustomDialog()
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		 alertDialog.setTitle("Custom Message");
		 alertDialog.setMessage("Enter Message");
		 final EditText input = new EditText(this);
		 LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
		     LinearLayout.LayoutParams.MATCH_PARENT,
		     LinearLayout.LayoutParams.MATCH_PARENT);
		 input.setLayoutParams(lp);		 
		 alertDialog.setView(input);
		 alertDialog.setPositiveButton("YES",
		     new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int which) {		        	 
		        	Log.e("message", input.getText().toString());
		        	message=input.getText().toString();	
		        	if(message.equals(" ")||message.length()==0)
		        	{
		        	Toast.makeText(MapDetails.this, "Please enter message", Toast.LENGTH_SHORT).show();
		        	ViewGroup parentViewGroup = (ViewGroup) input.getParent();
		            if (parentViewGroup != null) {
		                parentViewGroup.removeAllViews();
		            }
		        	alertDialog.show();
		        	}
		        	else
		        	{
		        		shareDialog();	
		        	}
		         }
		     });
		 alertDialog.setNegativeButton("NO",
		     new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int which) {
		             dialog.cancel();
		         }
		     });
		 alertDialog.show();
		 }
	void shareDialog()
	{
		 String[] TO = {""};
	        String[] CC = {""};
	        Intent emailIntent = new Intent(Intent.ACTION_SEND);
	        emailIntent.setData(Uri.parse("mailto:"));
	        emailIntent.setType("text/plain");
	        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
	        emailIntent.putExtra(Intent.EXTRA_CC, CC);
	        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Share Location");
	        message="Message -> "+message+"\n\n"+"Current Location -> "+addressString;
	        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
	        try {
	            startActivityForResult(Intent.createChooser(emailIntent, "Share Location..."), 110);
	            Log.d("Finished sending.", "");
	        } catch (ActivityNotFoundException ex) {
	            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
	        }
	}
	
	void showlocationDialog()
	{
		new AlertDialog.Builder(this)
        .setTitle("CIBS PRO")
        .setMessage("Are you sure that you want to save current location as safe location ?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            	Log.e("yess","yess");
            	saveLocation();
            }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            	Log.e("no","yess");

            }
        })
        .show();	
	}
	private class AddressGeocoderTask extends AsyncTask<Void, Void, List<Address>>{
		 
        @Override
        protected List<Address> doInBackground(Void... params) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null; 
            try {
                addresses = geocoder.getFromLocation(addressLocation.getLatitude(), addressLocation.getLongitude(), 1);
            } catch (IOException e) {
                Log.d("Exception", e.toString());
            }
            return addresses;
        }
 
        @Override
        protected void onPostExecute(List<Address> addresses) {
 
            if(addresses==null || addresses.size()==0){
               // Toast.makeText(getApplicationContext(), "No Location found", Toast.LENGTH_SHORT).show();
                return;
            }
 
            Address address = (Address) addresses.get(0);
             
            
            if(callingMtd.equalsIgnoreCase("userLocation")){
            	
            	userAddressString = "";
            	if(address.getAddressLine(0) != null)
            		userAddressString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		userAddressString = userAddressString + "," + address.getAddressLine(3);
            	if(!userAddressString.contains(address.getCountryName()))
            		userAddressString = userAddressString + "," + address.getCountryName();            	
            	
            	userLocationMarker.setTitle("YourLoc:"+userAddressString);
            	
            	userLocationMarker.showInfoWindow();
            }	
            else if(callingMtd.equalsIgnoreCase("selectedLocation")){
            	//selectedAddressString = String.format("%s, %s",address.getAddressLine(0)+","+address.getAddressLine(1)+","+address.getAddressLine(2),address.getMaxAddressLineIndex() > 2 ? ","+address.getCountryName() : "");
            
            	selectedAddressString = "";
            	if(address.getAddressLine(0) != null)
            		selectedAddressString = address.getAddressLine(0);
            	if(address.getAddressLine(1) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(1);
            	if(address.getAddressLine(2) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(2);
            	if(address.getAddressLine(3) != null)
            		selectedAddressString = selectedAddressString + "," + address.getAddressLine(3);
            	if(!selectedAddressString.contains(address.getCountryName()))
            		selectedAddressString = selectedAddressString + "," + address.getCountryName();            	
            	selectedLocationMarker.setTitle("SelectedLoc:"+selectedAddressString);            	
            	addressString=selectedAddressString;            	        
            	selectedLocationMarker.showInfoWindow();
            	showlocationDialog();
            }
        } 
    }
}
