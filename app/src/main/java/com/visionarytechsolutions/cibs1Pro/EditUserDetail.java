package com.visionarytechsolutions.cibs1Pro;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.accounts.Account;
import android.accounts.AccountManager;

public class EditUserDetail extends Activity {
	EditText etxUserName, etxDeviceName;
	Button btnSave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_user_detail);
		etxUserName = (EditText) findViewById(R.id.extuserName);
		etxDeviceName = (EditText) findViewById(R.id.etxDevice);
		btnSave = (Button) findViewById(R.id.btnSave);

		try {
			AccountManager manager = AccountManager.get(EditUserDetail.this);

			Account[] accounts = manager.getAccountsByType("com.google");
			List<String> username = new LinkedList<String>();

			for (Account account : accounts) {
				username.add(account.name);
			}
			String name[] = username.get(0).split("@");
			Log.e("detailss", username.get(0));
			etxUserName.setText(name[0]);
			etxDeviceName.setText(getPhoneName());
			btnSave.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(EditUserDetail.this, QuickOrCustom.class);
					startActivity(i);
					finish();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPhoneName() {
		BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
		String deviceName = myDevice.getName();
		return deviceName;
	}
}