package com.visionarytechsolutions.cibs1Pro;

import java.io.File;
import java.text.SimpleDateFormat;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.service.MouthService;
import com.visionarytechsolutions.cibs1Pro.utils.GlobalClass;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MySelfieFragment extends Fragment {	
	
	View rootView;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	ImageView snapshotImage;
	SharedPreferences sharedPreferences;
	Editor editor;
	static MySelfieFragment instance;
	RelativeLayout mainLayout;
	boolean snapshotTaken = false,fromFirstActivity = false;
	Button btnTakeNew,btnContinue,btnAddPhoto,btnTakeVideo;
	Intent intent;
	String imgPath;
	Button btnInfo;
	ImageView btnPlay,btnPause;
	static VideoView myVideoView;
	RelativeLayout relLayout;
    private static int RESULT_LOAD_IMAGE = 2;
    private Uri fileUri;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    public static TextView output;
    public static MySelfieFragment ActivityContext =null; 
	public MySelfieFragment(boolean fromFirstActivity){
		this.fromFirstActivity = fromFirstActivity;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();				
          instance = this;		
          rootView = inflater.inflate(R.layout.fragment_selfie, container, false);       
          snapshotImage = (ImageView) rootView.findViewById(R.id.snapshotImage);		
		snapshotTaken = sharedPreferences.getBoolean("isSnapShotTaken", false);		
		mainLayout = (RelativeLayout) rootView.findViewById(R.id.snapshot_mainLayout);
		btnTakeNew = (Button) rootView.findViewById(R.id.btnTakeNew);
		btnContinue= (Button) rootView.findViewById(R.id.btnContinue);
		btnInfo= (Button) rootView.findViewById(R.id.btnInfo);
		btnAddPhoto=(Button)rootView.findViewById(R.id.btnAddPhoto);
		btnTakeVideo=(Button)rootView.findViewById(R.id.btnTakeVideo);
		myVideoView=(VideoView)rootView.findViewById(R.id.videoView);
		btnPlay=(ImageView)rootView.findViewById(R.id.btnPlay);
		btnPause=(ImageView)rootView.findViewById(R.id.btnPause);
		relLayout=(RelativeLayout)rootView.findViewById(R.id.relLayout);
          Log.e("detailss","k"+sharedPreferences.getString("snapshotImagePath", ""));
          Log.e("detailss","k"+sharedPreferences.getString("snapshotVideoPath", ""));
          
          
          Log.e("FIRST A","path"+MySharedPreferences.getInstance().getData(getActivity(), MySharedPreferences.IMAGE));
		Log.e("FIRST B","path"+MySharedPreferences.getInstance().getData(getActivity(), MySharedPreferences.VIDEO));
			
		/*android.widget.MediaController media_Controller = new android.widget.MediaController (getActivity(),null);
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = dm.heightPixels;
		int width = dm.widthPixels;
		myVideoView.setMinimumWidth(width);
		myVideoView.setMinimumHeight(height);
		myVideoView.setMediaController(media_Controller);
		myVideoView.setVideoPath("/storage/sdcard0/Pictures/CIBS_VIDEO/CIBS_VIDEO.mp4");
		myVideoView.start();*/
		
				

		//02-20 22:41:10.963: E/savedd(32133): file:///storage/sdcard0/Pictures/CIBS_VIDEO/CIBS_VIDEO.mp4


		
		/*MediaController mediaController= new MediaController(getActivity());  
          mediaController.setAnchorView(myVideoView);          
		VideoView video = (VideoView) rootView.findViewById(R.id.videoView); 
		*/
		
         /* Uri uri=Uri.parse("/storage/Pictures/CIBS_Video/CIBS_VIDEO.mp4"); 
          Log.e("videopath",uri.toString());
          
          
		video.setVideoURI(uri);
		video.start();
		video.requestFocus();*/

		if(!fromFirstActivity)
			btnContinue.setVisibility(View.GONE);
		if(fromFirstActivity){
			File myDirectory = new File(Environment.getExternalStorageDirectory(), "CIBS");
			if(!myDirectory.exists()) {                                 
			  myDirectory.mkdirs();
			}
		}
		btnPlay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub			
		            myVideoView.start();
		            btnPlay.setVisibility(View.GONE);
		            btnPause.setVisibility(View.VISIBLE);				
			}
		});
        btnPause.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  myVideoView.pause();;
		            btnPlay.setVisibility(View.VISIBLE);
		            btnPause.setVisibility(View.GONE);
			}
		});
        myVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
     	    @Override
     	    public void onCompletion(MediaPlayer mediaPlayer) {
     	        btnPlay.setVisibility(View.VISIBLE);
     	        btnPause.setVisibility(View.GONE);
     	    }
     	});
        
         if(!sharedPreferences.getString("snapshotImagePath", "").equals(""))
         {
     	    snapshotImage.setVisibility(View.VISIBLE);
         }
         if(!sharedPreferences.getString("snapshotVideoPath", "").equals(""))
         {
     	    relLayout.setVisibility(View.VISIBLE);
     	    myVideoView.setVideoPath(sharedPreferences.getString("snapshotVideoPath", ""));
              myVideoView.requestFocus();
     	    
         }
         
		String imagePath = sharedPreferences.getString("snapshotImagePath", "");
		if(imagePath != ""){
			Bitmap b = BitmapFactory.decodeFile(imagePath);
	          snapshotImage.setImageBitmap(b);
	          snapshotTaken = true;
	          snapshotImage.setVisibility(View.VISIBLE);
		}
		
		btnTakeNew.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
			}
		});
		
		btnContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(sharedPreferences.getBoolean("Trip", false))
				{
				if(snapshotTaken){
					editor.putBoolean("isSnapShotTaken", true);
			    	editor.commit();			    	
			    	if(fromFirstActivity)
			    		FirstActivity.instance.setScreen();
				}else{
					Toast.makeText(getActivity().getApplicationContext(), "Please Take Snapshot", Toast.LENGTH_LONG).show();
				}}
				else
				{
					editor.putBoolean("isSnapShotTaken", true);
			    	editor.commit();			    	
			    	if(fromFirstActivity)
			    		FirstActivity.instance.setScreen();	
				}				
			}
		});
		btnInfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog();
			}
		});
         btnAddPhoto.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent i = new Intent(
	                        Intent.ACTION_PICK,
	                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});
         btnTakeVideo.setOnClickListener(new View.OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				
 				if(MouthService.isServiceWorking){
 					Intent svc =new Intent(getActivity(), MouthService.class);
 					getActivity().stopService(svc);
 				}
 				
 				Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
 				 
                // create a file to save the video
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO); 
                 
                // set the image file name  
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);  
                 
                // set the video image quality to high
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1); 
                
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
                
                // start the Video Capture Intent
                startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
 			}
 		});
        return rootView;
    }
	
	public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/CIBS/", "CIBS_Selfie.jpg");
        Uri imgUri = Uri.fromFile(file);
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
	        Editor edit=myPref.edit();
	        
	    super.onActivityResult(requestCode, resultCode, data);
	    	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {	
	    	   snapshotTaken = true;
	    	   Log.e("pathh",imgPath);
	    	  // MySharedPreferences.getInstance().storeData(getActivity(), "ImagePath", imgPath);
	    	  // MySharedPreferences.getInstance().storeData(getActivity(), "VideoPath", "");
	    	  // GlobalClass.imgpath=imgPath;
	    	   //GlobalClass.videoPath="";	    	   
	    	   
	    	   MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.IMAGE, imgPath);
	    	   MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.VIDEO, " ");
	    	   
	    	edit.putString("snapshotImage", imgPath);
	    	edit.putString("snapshotVideo", "");
	    	edit.commit();
	    	   
	        editor.putString("snapshotImagePath", imgPath);
	        editor.putString("snapshotVideoPath", "");
	        
	        
	        editor.apply();
	        relLayout.setVisibility(View.GONE);
	        snapshotImage.setVisibility(View.VISIBLE);
	        Bitmap b = BitmapFactory.decodeFile(imgPath);
	        snapshotImage.setImageBitmap(b);
	        Log.e("videoo","path"+sharedPreferences.getString("snapshotVideoPath", ""));
	        Log.e("imagepath","path"+sharedPreferences.getString("snapshotImagePath", ""));	        
	    }
	    else if(requestCode==RESULT_LOAD_IMAGE&&resultCode==Activity.RESULT_OK)
	    {
	    	  Uri selectedImageUri = data.getData();
            if (null != selectedImageUri) {
                String path = getPathFromURI(selectedImageUri);
                
                
           	edit.putString("snapshotImage", path);
     	    	//edit.putString("snapshotVideo", "");
     	    	edit.commit();
                editor.putString("snapshotImagePath", path);
   	           editor.putString("snapshotVideoPath", "");
   	           
   	        MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.IMAGE, path);
	    	   MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.VIDEO, " ");
   	           
   	           
   	       // MySharedPreferences.getInstance().storeData(getActivity(), "ImagePath", path);
	    	   //MySharedPreferences.getInstance().storeData(getActivity(), "VideoPath", "");
   	           relLayout.setVisibility(View.GONE);
   	           snapshotImage.setVisibility(View.VISIBLE);
   	         // GlobalClass.imgpath=path;
	    	    // GlobalClass.videoPath="";
    	           editor.apply();
                snapshotImage.setImageURI(selectedImageUri); 
                snapshotTaken=true;
                Log.e("videoo","path"+sharedPreferences.getString("snapshotVideoPath", ""));
   	           Log.e("imagepath","path"+sharedPreferences.getString("snapshotImagePath", ""));

	    }  }
        else if(requestCode==CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE&&resultCode==Activity.RESULT_OK)
        {
        	//output.setText("Video File : " +data.getData());
            
            // Video captured and saved to fileUri specified in the Intent
            /*Toast.makeText(getActivity(),
                     "Video is saved"+data.getDataString(), Toast.LENGTH_LONG).show();*/
     	     //myVideoView.setVideoPath(data.getExtras().get("data"));
 
     	   //  myVideoView.setVideoPath(data.getData().toString());
             myVideoView.requestFocus();
            
             String path=Environment.getExternalStorageDirectory() + "/CIBS/VideoRecords"+"/CIBS_Selfie_Video.mp4"; 
             Log.e("videopath",path);
     	     myVideoView.setVideoPath(path);

             MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.IMAGE, " ");
	    	   MySharedPreferences.getInstance().storeData(getActivity(), MySharedPreferences.VIDEO, path);
	    	
	    	 // edit.putString("snapshotImage", "");
	      // edit.putString("snapshotVideo", path);
	    	   //edit.commit();
    	        editor.putString("snapshotVideoPath",path);
    	        editor.putString("snapshotImagePath", "");
	        //MySharedPreferences.getInstance().storeData(getActivity(), "ImagePath", "");
	    	  // MySharedPreferences.getInstance().storeData(getActivity(), "VideoPath", path);
	        editor.apply();	        
	        relLayout.setVisibility(View.VISIBLE);
	        snapshotImage.setVisibility(View.GONE);   
	        Log.e("videoo","path"+sharedPreferences.getString("snapshotVideoPath", ""));
	        Log.e("imagepath","path"+sharedPreferences.getString("snapshotImagePath", ""));
	        if(MouthService.isServiceWorking){
		        	
		        	Intent svc =new Intent(getActivity(), MouthService.class);
					getActivity().startService(svc);
		        }	                   
             
        } /*else if (resultCode == RESULT_CANCELED) {
             
            output.setText("User cancelled the video capture.");
             
            // User cancelled the video capture
            Toast.makeText(this, "User cancelled the video capture.", 
                    Toast.LENGTH_LONG).show();
             
        }*/ //else {
             
            //output.setText("Video capture failed.");
             
            // Video capture failed, advise user
            //Toast.makeText(getActivity(), "Video capture failed.", 
                   // Toast.LENGTH_LONG).show();
        
   // }
}  	  
	void showDialog()
	{
		new AlertDialog.Builder(getActivity())
        .setTitle("CIBS PRO")
        .setMessage("Keeping an up to date photos is important to ensure that if someone has to look for you they have the most recent photo to identify you by.  You can also take a video of, yourself, locations and those who are with you throughout the day.  This will give emergency respondents an idea of where to look and who to talk to.  For example, if you are going on a date with a new person you can take a selfie of you and them together.  This picture will then be sent to your emergency contacts if you miss a check in interval.")
        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
        })
        .show();	
	}

	public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
	
	/** Create a file Uri for saving an image or video */
    private  Uri getOutputMediaFileUri(int type){
         
          return Uri.fromFile(getOutputMediaFile(type));
    }
 
    /** Create a File for saving an image or video */
    private   File getOutputMediaFile(int type){
         
        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                  Environment.DIRECTORY_PICTURES), "CIBS_VIDEO");
         
         
        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()){
             
            if (! mediaStorageDir.mkdirs()){
                 
                output.setText("Failed to create directory MyCameraVideo.");
                 
               // Toast.makeText(instance, "Failed to create directory MyCameraVideo.", 
                        //Toast.LENGTH_LONG).show();
                 
               // Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }          
        // Create a media file name
         
        // For unique file name appending current timeStamp with file name
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                             .format(date.getTime());
         
        File mediaFile;
         
        if(type == MEDIA_TYPE_VIDEO) { 
     	   File myDirectory = new File(Environment.getExternalStorageDirectory(), "CIBS");
   		if(!myDirectory.exists()) {                                 
   		  myDirectory.mkdirs();
   		}
           File dir = new File(myDirectory,"VideoRecords");
           if (dir.exists()) {
               dir.delete();
           }
           dir.mkdirs();     	   
            // For unique video file name appending current timeStamp with file name
            //mediaFile = new File(mediaStorageDir.getPath() + File.separator +
           // "CIBS_VIDEO" + ".mp4");     
     	   mediaFile = new File(Environment.getExternalStorageDirectory() + "/CIBS/VideoRecords", "CIBS_Selfie_Video.mp4"); 
     	      
            //String path=mediaStorageDir.getPath() + File.separator +
                     // "CIBS_VIDEO" + ".mp4";                                    
        } else {
            return null;
        }
        return mediaFile;
    }
   
     
}
