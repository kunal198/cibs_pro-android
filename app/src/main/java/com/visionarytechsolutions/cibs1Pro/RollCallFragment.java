package com.visionarytechsolutions.cibs1Pro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.adapter.ContactItemAdapter;
import com.visionarytechsolutions.cibs1Pro.fileUpload.NetworkOperations;

public class RollCallFragment extends Fragment {	
	
	View rootView;
	SharedPreferences sharedPreferences;
	Editor editor;
	static RollCallFragment instance;	
	RelativeLayout mainLayout;
	TextView meetingLocationTv;
	Button changeLocationBtn, addContactListBtn, activateBtn;
	CheckBox chkWithoutSafe;
	Location selectedLocation;
	String addressString;
	ListView contactListView;
	ArrayList<String> contactsArrayList = new ArrayList<String>();
	JSONObject contactsJSON = new JSONObject();
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();			
          instance = this;		
          rootView = inflater.inflate(R.layout.fragment_rollcall, container, false);
		mainLayout = (RelativeLayout) rootView.findViewById(R.id.rollcall_mainLayout);
		meetingLocationTv = (TextView) rootView.findViewById(R.id.rollcall_meetingLocTv);
		meetingLocationTv.setSelected(true);
		changeLocationBtn = (Button) rootView.findViewById(R.id.rollcall_btnChangeLocation);
		addContactListBtn = (Button) rootView.findViewById(R.id.rollcall_btnAddContact);
		activateBtn = (Button) rootView.findViewById(R.id.rollcall_btnActivate);
		contactListView = (ListView) rootView.findViewById(R.id.rollcall_contactList);
		chkWithoutSafe = (CheckBox) rootView.findViewById(R.id.rollcall_chkwithoutsafeLocation);
		
		loadLocationsList();
		loadContacts();
		
		changeLocationBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(checkNetwork()){
			    	Intent intent=new Intent(getActivity().getApplicationContext(),MapActivity.class);  
					startActivityForResult(intent, 2);
				}else
					Toast.makeText(getActivity().getApplicationContext(), "Please check your network connection", Toast.LENGTH_LONG).show();
			}
		});
		
		addContactListBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.instance)
	            .setMessage("How do you want to ADD?")
	            .setPositiveButton("Manually", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) {
	                	showContactAlert(-1);
	                }	                    
	            })
	            .setNegativeButton("From Phone List", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	Intent intent = new Intent(Intent.ACTION_PICK,Uri.parse("content://contacts"));
        				intent.setType(Phone.CONTENT_TYPE);
        		          startActivityForResult(intent, 1);
                    }
	             })
	            .setCancelable(false)
	            .create();			
				alertDialog.show();
			}
		});		
		activateBtn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				String meetingLocation = sharedPreferences.getString("meetingLocationJSON", "{}");
				String contacts = sharedPreferences.getString("rollcallContactsJSON", "{}");				
				Log.e("meeting locations","jkl"+meetingLocation);
				Log.e("contacts","kl"+contacts);
				Log.e("phone",sharedPreferences.getString("phone", ""));
				String phone = sharedPreferences.getString("phone", "");
				String email = sharedPreferences.getString("email", "");
				String name = sharedPreferences.getString("firstName", "")+" "+sharedPreferences.getString("lastName", "");
				List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();
				if(chkWithoutSafe.isChecked())
					parameters.add(new Pair<String, String>("meetingLocation", "{}"));
				else
					parameters.add(new Pair<String, String>("meetingLocation", meetingLocation));
				parameters.add(new Pair<String, String>("contacts", contacts));
				parameters.add(new Pair<String, String>("phone", phone));
				parameters.add(new Pair<String, String>("email", email));
				parameters.add(new Pair<String, String>("name", name));
				parameters.add(new Pair<String, String>("cmd", "RollCallRequest"));				
				String url = "http://www.checkinbesafe.com/webservices/cibs_activaterollcall.php";
				NetworkOperations nop = new NetworkOperations(url, parameters, getActivity(), "activate");
				nop.execute();
				Toast.makeText(MainActivity.instance, "Invitation(s) Has been sent", Toast.LENGTH_LONG).show();
				copyRollCalldeContacts();
			}
		});		
        return rootView;
    }
	
	public void copyRollCalldeContacts(){
		JSONObject contactsDeJSON = new JSONObject();
		try {
			contactsJSON = new JSONObject(sharedPreferences.getString("rollcallContactsJSON", "{}"));
			@SuppressWarnings("rawtypes")
			Iterator it = contactsJSON.keys();
	        while(it.hasNext()){
	        	String name = it.next().toString();
	        	JSONObject contactJSON = new JSONObject(contactsJSON.getString(name));
	        	contactJSON.put("status", "invitation sent");
	        	contactJSON.put("location", "{}");
	        	contactJSON.put("red", true);
	        	contactsDeJSON = contactsDeJSON.put(name, contactJSON);
	        }
	        editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());	          			        	        
	        Log.e("activateeeee",""+contactsDeJSON);
	        editor.putBoolean("rollcallActivated", true);
	        editor.apply();
	        if(MainActivity.instance!=null) MainActivity.instance.displayView(8);
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	
	@SuppressLint("InflateParams") public void showContactAlert(final int position){
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());		
		alert.setTitle("Contact Information");
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contactAlert = inflater.inflate(R.layout.contact_form, null);
		final EditText fullNameEdt = (EditText) contactAlert.findViewById(R.id.contact_fullNameEdt);
		final EditText emailAddressEdt = (EditText) contactAlert.findViewById(R.id.contact_emailEdt);
		final EditText PhoneNumberEdt = (EditText) contactAlert.findViewById(R.id.contact_phoneNumberEdt);
		alert.setView(contactAlert);
		
		if(position>=0){
			fullNameEdt.setText(contactsArrayList.get(position));
			fullNameEdt.setEnabled(false);
			JSONObject contactJSON;
			try {
				contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				emailAddressEdt.setText(contactJSON.getString("email"));
				PhoneNumberEdt.setText(contactJSON.getString("phone"));
			} catch (JSONException e) {
				Log.d("Contact:", e.toString());
			}			
		}

		alert.setNeutralButton("Save Contact", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String fullName = fullNameEdt.getText().toString();
				String emailAddress = emailAddressEdt.getText().toString();
				String PhoneNumber = PhoneNumberEdt.getText().toString();
				if(fullName.equalsIgnoreCase("")){
					Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();					
				}
				else if(emailAddress.equalsIgnoreCase("") && PhoneNumber.equalsIgnoreCase("")){
					Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();
				}
				else{
		           try {
		        	   JSONObject contactJSON = new JSONObject();
		        	   contactJSON.put("email", emailAddress);
		        	   contactJSON.put("phone", PhoneNumber);
		        	   contactsJSON = contactsJSON.put(fullName, contactJSON);
					   editor.putString("rollcallContactsJSON", contactsJSON.toString());
					   editor.commit();
		           } catch (JSONException e) {
						Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		           }
		           updateContactList();
		           if(position == -1)
		        	   Toast.makeText(getActivity().getApplicationContext(), "Contact Added Successfully", Toast.LENGTH_LONG).show();			        
					dialog.cancel();
				}
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		alert.setCancelable(false);
		alert.show();
	}
	
	public void loadContacts(){
		try {
			contactsJSON = new JSONObject(sharedPreferences.getString("rollcallContactsJSON", "{}"));
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
    	@SuppressWarnings("rawtypes")
		Iterator it = contactsJSON.keys();
        while(it.hasNext())
        	contactsArrayList.add(it.next().toString());
        contactListView.setAdapter(new ContactItemAdapter(contactsJSON, getActivity().getApplicationContext(), RollCallFragment.this, contactsArrayList));
        
        contactListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {				
				showContactAlert(position);
			}
		});
	}
	
	public void removeContactInfo(int position){
		contactsJSON.remove(contactsArrayList.get(position));
    	editor.putString("rollcallContactsJSON", contactsJSON.toString());
		editor.commit();
		updateContactList();
    	Toast.makeText(getActivity().getApplicationContext(), "Contact removed Successfully", Toast.LENGTH_LONG).show();
    }
	
	public void updateContactList(){
		@SuppressWarnings("rawtypes")
		Iterator it = contactsJSON.keys();
    	contactsArrayList.removeAll(contactsArrayList);
        while(it.hasNext())
        	contactsArrayList.add(it.next().toString());
        contactListView.setAdapter(new ContactItemAdapter(contactsJSON, getActivity().getApplicationContext(), RollCallFragment.this, contactsArrayList));    	
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){  
      super.onActivityResult(requestCode, resultCode, data);  
       // check if the request code is same as what is passed  here it is 2  
      if(data != null)
        if(requestCode==2){  
           selectedLocation=data.getParcelableExtra("LOCATION");
           addressString = data.getStringExtra("addressString");
           if(selectedLocation != null )
        	   saveLocation();
        }else if(requestCode==1){
          Uri uri = data.getData();
          if (uri != null) {
              Cursor c = null;
              try {
                  c = getActivity().getContentResolver().query(uri, new String[]{ 
                              ContactsContract.CommonDataKinds.Phone.NUMBER,ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
                          null, null, null);         

                  if (c != null && c.moveToFirst()) {
                      String PhoneNumber = c.getString(0);
                      String fullName = c.getString(1);
                      String emailAddress= "";                      
                      
                      Cursor emails = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,
                    		  ContactsContract.CommonDataKinds.Email.CONTACT_ID+ " = " + c.getString(2), null, null);
                      while (emails.moveToNext()) {
                            emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                         }                           
                        emails.close();	                      
                      
                      PhoneNumber= PhoneNumber.replaceAll("\\s+","");
                      PhoneNumber= PhoneNumber.replaceAll("\\(","");
                      PhoneNumber= PhoneNumber.replaceAll("\\)","");
                      PhoneNumber= PhoneNumber.replaceAll("\\-","");
                      if(fullName.equalsIgnoreCase("")){
      					  Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();					
      				  }
      				  else if(emailAddress.equalsIgnoreCase("") && PhoneNumber.equalsIgnoreCase("")){
      					  Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();
      				  }else{
     		            try {
     		        	   JSONObject contactJSON = new JSONObject();
     		        	   contactJSON.put("email", emailAddress);
     		        	   PhoneNumber=PhoneNumber.substring(PhoneNumber.length()-10);
     		        	   contactJSON.put("phone", PhoneNumber);
     		        	   contactsJSON = contactsJSON.put(fullName, contactJSON);
     					   editor.putString("rollcallContactsJSON", contactsJSON.toString());
     					   editor.commit();
     					} catch (JSONException e) {
     						Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
     					}
     		            updateContactList();
     		            Toast.makeText(getActivity().getApplicationContext(), "Contact Added Successfully", Toast.LENGTH_LONG).show();
     				}	                      
                  }
              } finally {
                  if (c != null) {
                      c.close();
                  }
              }
          }
        }
	}
	
	public void saveLocation(){
    	try {
     	   JSONObject locationJSON = new JSONObject();
     	   locationJSON.put("lat", selectedLocation.getLatitude());
     	   locationJSON.put("lon", selectedLocation.getLongitude());
     	   locationJSON.put("addressString", addressString);
		   editor.putString("meetingLocationJSON", locationJSON.toString());
		   editor.commit();
		   Toast.makeText(getActivity().getApplicationContext(), "Meeting Location Changed Successfully", Toast.LENGTH_LONG).show();
		   loadLocationsList();
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}          
    }
	
	public void loadLocationsList(){
		JSONObject locationJSON = new JSONObject();
		try {
			locationJSON = new JSONObject(sharedPreferences.getString("meetingLocationJSON", "{}"));
			if(locationJSON.length()>0) meetingLocationTv.setText(locationJSON.getString("addressString"));
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	
	
	private boolean checkNetwork(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
    }
	
}
