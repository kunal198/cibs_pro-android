package com.visionarytechsolutions.cibs1Pro;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import com.visionarytechsolutions.cibs1Pro.utils.AbsRuntimeMarshmallowPermission;

public class WatchVideoOption extends AbsRuntimeMarshmallowPermission implements View.OnClickListener {
	Button btnYes,btnNo;
	VideoView videoView;
	SharedPreferences sharedPreferences;
	SharedPreferences.Editor editor;
	Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		editor=sharedPreferences.edit();


		requestAppPermissions(new String[]{"android.permission.CAMERA","android.permission.READ_CONTACTS","android.permission.ACCESS_FINE_LOCATION","android.permission.ACCESS_COARSE_LOCATION","android.permission.READ_EXTERNAL_STORAGE","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.INTERNAL_SYSTEM_WINDOW","android.permission.CALL_PHONE","android.permission.SYSTEM_ALERT_WINDOW"}, R.string.message, ALL_PERMISSIONS,-1);

		if(!sharedPreferences.getBoolean("showPopUp",false))
			showDialog();
		if(sharedPreferences.getString("watchDemo", "").equals("true"))
				{
			 Intent i=new Intent(WatchVideoOption.this,FirstActivity.class);
			 startActivity(i);	
			 finish();
				}
		setContentView(R.layout.activity_watch_video_option);
		btnYes=(Button)findViewById(R.id.btnYes);
		btnNo=(Button)findViewById(R.id.btnNo);
		//videoView=(VideoView)findViewById(R.id.videoView);
		btnYes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestAppPermissions(new String[]{"android.permission.CAMERA","android.permission.READ_CONTACTS","android.permission.ACCESS_FINE_LOCATION","android.permission.ACCESS_COARSE_LOCATION","android.permission.READ_EXTERNAL_STORAGE","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.INTERNAL_SYSTEM_WINDOW","android.permission.CALL_PHONE","android.permission.SYSTEM_ALERT_WINDOW"}, R.string.message, ALL_PERMISSIONS,0);

				/*Intent i=new Intent(WatchVideoOption.this,WatchVideo.class);
				 startActivity(i);
				 finish();	*/
			}
		});
	
        btnNo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requestAppPermissions(new String[]{"android.permission.CAMERA","android.permission.READ_CONTACTS","android.permission.ACCESS_FINE_LOCATION","android.permission.ACCESS_COARSE_LOCATION","android.permission.READ_EXTERNAL_STORAGE","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.INTERNAL_SYSTEM_WINDOW","android.permission.CALL_PHONE","android.permission.SYSTEM_ALERT_WINDOW"}, R.string.message, ALL_PERMISSIONS,1);

				/*Intent i=new Intent(WatchVideoOption.this,QuickOrCustom.class);
				 startActivity(i);
				 finish();*/
			}
		});
	}
	@Override
	public void onPermissionGranted(int requestCode) {
		if (requestCode == ALL_PERMISSIONS) {

		}
	}
	void showDialog()
	{

		 dialog = new Dialog(WatchVideoOption.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.error_dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		TextView msg_tv = (TextView) dialog.findViewById(R.id.msg_tv);
		TextView dialogdismiss_tv = (TextView) dialog.findViewById(R.id.dialogdismiss_tv);
		dialogdismiss_tv.setOnClickListener(this);
		dialog.setCancelable(false);
		dialog.show();
/*		new AlertDialog.Builder(WatchVideoOption.this)
				.setTitle("CIBS PRO").setCancelable(false)
				.setMessage("You have 7 day's free trial..")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						editor.putBoolean("showPopUp",true).apply();
					}
				})
				.show();*/
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.dialogdismiss_tv:
				dialog.dismiss();
				editor.putBoolean("showPopUp",true).apply();
				break;

		}


	}
}
