package com.visionarytechsolutions.cibs1Pro;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.adapter.SafeLocationAdapter;
import com.visionarytechsolutions.cibs1Pro.utils.IoListener;

public class SafeLocationsFragment extends Fragment implements IoListener {
		
	ListView sf_listView;
	SharedPreferences sharedPreferences;
	Editor editor;
	Location selectedLocation;
	ArrayList<String> safeLocationsArrayList = new ArrayList<String>();
	ArrayList<String> safeLocationsNameList = new ArrayList<String>();
	JSONObject safeLocationJSON = new JSONObject();
	JSONObject mapLocationJSON = new JSONObject();

	public static SafeLocationsFragment instance = null;
	String addressString;
	LinearLayout mainLayout;
	Button safelocation_btnContinue,safelocation_btnaddlocation;
	Intent intent;
	View rootView;
	boolean fromFirstActivity = false;
	Button btnInfo;
	IoListener mListener;
	
	public SafeLocationsFragment(boolean fromFirstActivity) {
		this.fromFirstActivity = fromFirstActivity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {		
		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        mListener=this;
        
        instance = this;
        
        rootView = inflater.inflate(R.layout.fragment_safe_locations,container, false);
        
        sf_listView = (ListView) rootView.findViewById(R.id.safelocation_listView);
    	mainLayout = (LinearLayout) rootView.findViewById(R.id.safeLocations_mainLayout);
    	safelocation_btnContinue = (Button) rootView.findViewById(R.id.safelocation_btnContinue);
    	safelocation_btnaddlocation = (Button) rootView.findViewById(R.id.safelocation_btnaddlocation);
    	btnInfo = (Button) rootView.findViewById(R.id.btnInfo);
    	
    	
    	
    	
    	
    	if (!fromFirstActivity) {
			safelocation_btnContinue.setVisibility(View.GONE);
		}   	
    	try {
			safeLocationJSON = new JSONObject(sharedPreferences.getString("safeLocationJSON", "{}"));					
			
			Log.e("safelocationjasonn",safeLocationJSON+"");
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
    	   	
    	safelocation_btnaddlocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(checkNetwork()){
			    	Intent intent=new Intent(getActivity().getApplicationContext(),MapActivity.class).putExtra("listClick", -1);  
					startActivityForResult(intent, 2);
				}else
					Toast.makeText(getActivity().getApplicationContext(), "Please check your network connection", Toast.LENGTH_LONG).show();
			}
		});
    	
    	safelocation_btnContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
								
				//if(sharedPreferences.getBoolean("Trip", false)==true)
				//{
				//	if(safeLocationJSON.length()<=0)
				//	{
						//Toast.makeText(getActivity(), "Please add location first", Toast.LENGTH_SHORT).show();
					//}
				//	else
					//{
						editor.putBoolean("isSafeLocationCompleted", true);
				    	editor.commit();		    	
				    	if(fromFirstActivity)
				    		FirstActivity.instance.setScreen();
							
					//}
				//}
				/*else
				{				
				editor.putBoolean("isSafeLocationCompleted", true);
		    	editor.commit();		    	
		    	if(fromFirstActivity)
		    		FirstActivity.instance.setScreen();
				}*/
			}
		});   
    	
    	btnInfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog();
				
			}
		});
    	
    	loadLocationsList();

		return rootView;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){  
      super.onActivityResult(requestCode, resultCode, data);  
       // check if the request code is same as what is passed  here it is 2 
      try
      {
      if(data != null)
        if(requestCode==2){         	
        	 /*if(data.getStringExtra("NoClick").equals("yes"))
        	 {
          	   safelocation_btnContinue.performClick(); 
        	 }*/
        	 //else
        	 //{
           selectedLocation=data.getParcelableExtra("LOCATION");
           addressString = data.getStringExtra("addressString");          
           if(selectedLocation != null )
        	   saveLocation();
        	// }
        }}
      catch(Exception e)
      {
    	  
      }
	}
	
	public void saveLocation(){
    	try {
     	   JSONObject locationJSON = new JSONObject();
     	   locationJSON.put("lat", selectedLocation.getLatitude());
     	   locationJSON.put("lon", selectedLocation.getLongitude());
     	   locationJSON.put("addressString", addressString);
     	   //Log.e("detailss",selectedLocation.getLatitude()+"");
     	   //Log.e("detailss1",selectedLocation.getLongitude()+"");
		   safeLocationJSON.put("address"+safeLocationJSON.length(), locationJSON);
		   editor.putString("safeLocationJSON", safeLocationJSON.toString());
		   editor.commit();
		   Toast.makeText(getActivity().getApplicationContext(), "Location Added Successfully", Toast.LENGTH_LONG).show();
	       loadLocationsList();
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}    	
        
    }
	
	public void loadLocationsList(){
    	safeLocationsArrayList.removeAll(safeLocationsArrayList);
    	safeLocationsNameList.removeAll(safeLocationsNameList);
        @SuppressWarnings("rawtypes")
		Iterator it = safeLocationJSON.keys();
        while(it.hasNext()){
        	try {
        		String key = it.next().toString();
				JSONObject locationJSON = (JSONObject) safeLocationJSON.get(key);
				safeLocationsNameList.add(key);
				safeLocationsArrayList.add(locationJSON.getString("addressString"));
			} catch (JSONException e) {
				Log.d("Exception", e.toString());
			}        	
        }
        sf_listView.setAdapter(new SafeLocationAdapter(safeLocationsArrayList, getActivity().getApplicationContext(), SafeLocationsFragment.this,mListener)); 
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
    
    public void removeSafeLocation(int position){    	
    	safeLocationJSON.remove(safeLocationsNameList.get(position));    	
    	safeLocationsNameList.remove(position);
    	JSONObject newSafeLocationJSON = new JSONObject(); 
    	mapLocationJSON=new JSONObject();
    	for (int i = 0; i < safeLocationsNameList.size(); i++) {
			try {
				newSafeLocationJSON.put("address"+i, safeLocationJSON.get(safeLocationsNameList.get(i)));
				Log.e("detailsss",""+safeLocationsNameList.get(i));
			} catch (JSONException e) {
				Log.d("Exception", e.toString());
			}
		}
    	safeLocationJSON = newSafeLocationJSON;
    	editor.putString("safeLocationJSON", safeLocationJSON.toString());
    	//editor.putString("mapLocationJSON", mapLocationJSON.toString());
		editor.commit();
		
		Toast.makeText(getActivity().getApplicationContext(), "Location removed Successfully", Toast.LENGTH_LONG).show();
    	loadLocationsList();    	
    }
	
	private boolean checkNetwork(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
    }
	void showDialog()
	{
		new AlertDialog.Builder(getActivity())
        .setTitle("CIBS PRO")
        .setMessage("The location page is where you can setup your safe locations.  For example, you can add your home address as a safe location and while you are at home you will receive no check in alerts.")
        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
        })
        .show();	
	}
	@Override
	public void onListClick(int position) {
		// TODO Auto-generated method stub
		//Toast.makeText(getActivity(), "call "+position, Toast.LENGTH_SHORT).show();
		//Intent intent=new Intent(getActivity().getApplicationContext(),MapActivity.class).putExtra("listClick", ""+position);  
		//startActivityForResult(intent, 2);		
	}
}
