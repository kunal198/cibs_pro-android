package com.visionarytechsolutions.cibs1Pro;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.service.RollcallAcceptService;
import com.visionarytechsolutions.cibs1Pro.service.RollcallLocationUpdateService;

public class RollCallFragmentAccept extends Fragment implements LocationListener{	
	
	View rootView;
	SharedPreferences sharedPreferences;
	Editor editor;
	public static RollCallFragmentAccept instance;	
	RelativeLayout mainLayout;
	TextView meetingLocationTv,initiatorNameTv,initiatorEmailTv,initiatorPhoneTv;
	Button deactivateBtn,showOnMapBtn;
	Location selectedLocation;
	String addressString,initiatorName,initiatorEmail,initiatorPhone;
	CheckBox safeChk,helpChk,inRouteChk;
	LocationManager locationManager;
	Location meetingLocation,currentLocation;
	boolean helpChecked = false, inRouteChecked = false;
	@Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();			
        instance = this;		
        meetingLocation = new Location("dummyprovider");
        currentLocation = new Location("dummyprovider");
        rootView = inflater.inflate(R.layout.fragment_rollcall_accept, container, false);
		mainLayout = (RelativeLayout) rootView.findViewById(R.id.rollcall_mainLayout);
		meetingLocationTv = (TextView) rootView.findViewById(R.id.rollcall_meetingLocTv);
		meetingLocationTv.setSelected(true);
		deactivateBtn = (Button) rootView.findViewById(R.id.rollcall_btnDeactivate);
		showOnMapBtn  = (Button) rootView.findViewById(R.id.rollcall_btnShowOnMap);
		initiatorNameTv = (TextView) rootView.findViewById(R.id.rollcall_name);
		initiatorEmailTv = (TextView) rootView.findViewById(R.id.rollcall_email);
		initiatorPhoneTv = (TextView) rootView.findViewById(R.id.rollcall_phone);
		safeChk = (CheckBox) rootView.findViewById(R.id.rollcall_chksafeLocation);
		helpChk = (CheckBox) rootView.findViewById(R.id.rollcall_chkneedHelp);
		inRouteChk = (CheckBox) rootView.findViewById(R.id.rollcall_chkinroute);
		
		if(sharedPreferences.getBoolean("helpCheck", false)){
			helpChk.setChecked(true);
		}		
		if(sharedPreferences.getBoolean("inRouteCheck", false)){
			inRouteChk.setChecked(true);
		}			
		String rollCallData = sharedPreferences.getString("rollCallData", "{}");
		try {
			JSONObject dataJson = new JSONObject(rollCallData);
			initiatorName = dataJson.getString("name");
			initiatorPhone  = dataJson.getString("phone");
			initiatorEmail = dataJson.getString("email");
			initiatorNameTv.setText(initiatorName);
			initiatorEmailTv.setText(initiatorEmail);
			initiatorPhoneTv.setText(initiatorPhone);
			JSONObject addressJson = new JSONObject(dataJson.getString("meetingLocation"));
			if(addressJson.length()>0){
				addressString = addressJson.getString("addressString");
				meetingLocationTv.setText(addressString);
				meetingLocation.setLatitude(Double.parseDouble(addressJson.getString("lat")));
				meetingLocation.setLongitude(Double.parseDouble(addressJson.getString("lon")));
				showOnMapBtn.setVisibility(View.VISIBLE);
			}else{
				showOnMapBtn.setVisibility(View.GONE);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		getCurrentLocation();
		deactivateBtn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
		        showPasswordAlert("deactivate");
			}
		});
		
		showOnMapBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=17&q=%f,%f", meetingLocation.getLatitude(),meetingLocation.getLongitude(),meetingLocation.getLatitude(),meetingLocation.getLongitude());
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				instance.startActivity(intent);
			}
		});
		
		safeChk.setOnCheckedChangeListener(new OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){ 				
					showPasswordAlert("safechk");
				}
			}
		});
		
		inRouteChk.setOnCheckedChangeListener(new OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){ 				
					showPasswordAlert("inroutechk");					
				}else if(inRouteChecked){
					if(!inRouteChk.isChecked()) inRouteChk.setChecked(true);
					if(dialog!=null){
						dialog.dismiss();
						dialog = null;
					}
					Toast.makeText(MainActivity.instance, "Please Select Another Choice/Option", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		helpChk.setOnCheckedChangeListener(new OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){ 
					showPasswordAlert("helpchk");
				}
				else if(helpChecked){
					if(!helpChk.isChecked()) helpChk.setChecked(true);
					if(dialog!=null){
						dialog.dismiss();
						dialog = null;
					}
					Toast.makeText(MainActivity.instance, "Please Select Another Choice/Option", Toast.LENGTH_LONG).show();
				}
			}
		});         
        return rootView;
    }	
	
	public void setAutoHelp(){
		if(!helpChk.isChecked()){
			helpChk.setChecked(true);
			helpChecked = true;
			editor.putBoolean("helpCheck", true);
			editor.commit();
		}
		if(dialog!=null){
			dialog.dismiss();
			dialog = null;
		}
		if(MainActivity.instance!=null){
			MainActivity.instance.changeRollcallStatus("NeedHelp");
			Intent locationUpdate = new Intent(MainActivity.instance, RollcallLocationUpdateService.class);
			MainActivity.instance.startService(locationUpdate);
		}		
	}
	AlertDialog dialog = null;
	int attempts = 0;
	public void showPasswordAlert(final String calledMtd){		
		if(dialog!=null) return;
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View pwdAlert = inflater.inflate(R.layout.alert_dialog_rollcall, null);
		final EditText password = (EditText) pwdAlert.findViewById(R.id.rollcall_confirmPasswordEdt);
		Button okBtn = (Button) pwdAlert.findViewById(R.id.rollcall_confirmPasswordBtn);
		Button cancelBtn = (Button) pwdAlert.findViewById(R.id.rollcall_cancelBtn);
		cancelBtn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(dialog!=null){
					dialog.dismiss();
					dialog = null;
				}
				if(calledMtd.equalsIgnoreCase("helpchk")){
					helpChk.setChecked(false);
				}else if(calledMtd.equalsIgnoreCase("safechk")){
					safeChk.setChecked(false);
				}else if(calledMtd.equalsIgnoreCase("inroutechk")){
					inRouteChk.setChecked(false);
				}
			}
		});
		okBtn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {				
				String pwd = password.getText().toString();
				String userPwd = sharedPreferences.getString("password", "");
				if(pwd.equalsIgnoreCase(userPwd)){
//					setResult(Activity.RESULT_OK);
					if(dialog!=null){
						dialog.dismiss();                             
						dialog = null;
					}
					if(RollcallAcceptService.instance != null){
						RollcallAcceptService.instance.stopSelf();
					}
					if(calledMtd.equalsIgnoreCase("deactivate")){
						editor.putBoolean("rollCallRequestAccepted", false);
				        editor.commit();
				        cancelListener();
				        if(MainActivity.instance!=null){ 
				        	MainActivity.instance.changeRollcallStatus("Deactivated");
				        	MainActivity.instance.displayView(8);
				        }
				        if(RollcallLocationUpdateService.instance!=null){
				        	RollcallLocationUpdateService.instance.stopSelf();
				        }
					}else if(calledMtd.equalsIgnoreCase("helpchk")){
						if(inRouteChk.isChecked()){
							inRouteChecked = false;
							inRouteChk.setChecked(false);
							editor.putBoolean("inRouteCheck", false);
							editor.commit();
						}
						helpChecked = true;
						editor.putBoolean("helpCheck", true);
						editor.commit();
						setAutoHelp();
					}else if(calledMtd.equalsIgnoreCase("inroutechk")){
						if(helpChk.isChecked()){
							helpChecked = false;
							helpChk.setChecked(false);
							editor.putBoolean("helpCheck", false);
							editor.commit();
						}
						inRouteChecked = true;
						editor.putBoolean("inRouteCheck", true);
						editor.commit();
						if(MainActivity.instance!=null){
				        	MainActivity.instance.changeRollcallStatus("InRoute");
				        }
					}else if(calledMtd.equalsIgnoreCase("safechk")){
						editor.putBoolean("rollCallRequestAccepted", false);
				        editor.commit();
				        cancelListener();
				        Toast.makeText(MainActivity.instance, "Selected Safe/Meeting Location", Toast.LENGTH_LONG).show();
				        if(MainActivity.instance!=null){
				        	MainActivity.instance.changeRollcallStatus("Safe");
				        	MainActivity.instance.displayView(8);
				        }
				        if(RollcallLocationUpdateService.instance!=null){
				        	RollcallLocationUpdateService.instance.stopSelf();
				        }
					}else if(calledMtd.equalsIgnoreCase("reachedsafe")){
						editor.putBoolean("rollCallRequestAccepted", false);
				        editor.commit();
				        cancelListener();
				        if(MainActivity.instance!=null){
				        	MainActivity.instance.changeRollcallStatus("Reached");
				        	MainActivity.instance.displayView(8);
				        }
				        if(RollcallLocationUpdateService.instance!=null){
				        	RollcallLocationUpdateService.instance.stopSelf();
				        }
					}
				}else{
					attempts+=1;
					if(attempts != 3){
						Toast.makeText(getActivity(), "Please Enter Correct Password, "+(3-attempts)+" attempts remaining", Toast.LENGTH_LONG).show();
						password.setText("");
					}
					else{				
//						setResult(Activity.RESULT_CANCELED);
						if(calledMtd.equalsIgnoreCase("helpchk")){
							helpChk.setChecked(false);
						}else if(calledMtd.equalsIgnoreCase("safechk")){
							safeChk.setChecked(false);
						}else if(calledMtd.equalsIgnoreCase("inroutechk")){
							inRouteChk.setChecked(false);
						}
						setAutoHelp();
					}
				}
			}
		});
		alert.setView(pwdAlert);		
		alert.setCancelable(false);
		dialog = alert.show();
	}
	
	public void setLocation(){
		String rollCallData = sharedPreferences.getString("rollCallData", "{}");
		try {
			JSONObject dataJson = new JSONObject(rollCallData);
			JSONObject addressJson = new JSONObject(dataJson.getString("meetingLocation"));
			if(addressJson.length()>0){
				addressString = addressJson.getString("addressString");
				meetingLocationTv.setText(addressString);
				meetingLocation.setLatitude(Double.parseDouble(addressJson.getString("lat")));
				meetingLocation.setLongitude(Double.parseDouble(addressJson.getString("lon")));
				showOnMapBtn.setVisibility(View.VISIBLE);
			}else{
				showOnMapBtn.setVisibility(View.GONE);
			}
		} catch (JSONException e) {
			Log.v("error", e.toString());
		}
	}
	
	public void deactivateRollcall(){
		try
		{
		editor.putBoolean("rollCallRequestAccepted", false);
        editor.commit();
        cancelListener();
        if(MainActivity.instance!=null){
        	Toast.makeText(MainActivity.instance, "RollCall Deactivated by Initiator", Toast.LENGTH_LONG).show();
        	MainActivity.instance.displayView(8);
        }}
        catch(Exception e)
        {
     	   
        
        }
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	
	public void getCurrentLocation(){
		locationManager = (LocationManager) MainActivity.instance.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(currentLocation == null){
        	try{
	        	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	        	currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);  
        	}catch(Exception e){
        		Log.d("Exception", e.toString());
        	}
        }
        if(currentLocation!=null){
	        if(meetingLocation.distanceTo(currentLocation)<=100){
	        	if(dialog==null){
	        		Uri choice = Uri.parse("android.resource://" + MainActivity.instance.getPackageName() + "/"+R.raw.reachedsafe);
    	    	    Ringtone r = RingtoneManager.getRingtone(MainActivity.instance, choice);
    	    	    r.play();
	        		Toast.makeText(MainActivity.instance, "Reached Meeting Location", Toast.LENGTH_LONG).show();	        	
	        	}
	        	showPasswordAlert("reachedsafe");	        	
	        }
        }else{
        	Toast.makeText(MainActivity.instance, "Please Turn On GPS", Toast.LENGTH_LONG).show();
        }
	}
	
	public void cancelListener(){
		if(locationManager!=null){
			locationManager = (LocationManager) MainActivity.instance.getSystemService(Context.LOCATION_SERVICE);
			locationManager.removeUpdates(this);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		currentLocation = location;	
		if(meetingLocation.distanceTo(currentLocation)<=100){
        	if(dialog==null){
        		Uri choice = Uri.parse("android.resource://" + MainActivity.instance.getPackageName() + "/"+R.raw.reachedsafe);
//	    	    Ringtone r = RingtoneManager.getRingtone(MainActivity.instance, choice);
//	    	    r.play();
        		MediaPlayer mediaPlayer= MediaPlayer.create(MainActivity.instance, R.raw.reachedsafe);
        		mediaPlayer.start();
        		Toast.makeText(MainActivity.instance, "Reached Meeting Location", Toast.LENGTH_LONG).show();	        	
        	}
        	showPasswordAlert("reachedsafe");	    
        }
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}	
}
