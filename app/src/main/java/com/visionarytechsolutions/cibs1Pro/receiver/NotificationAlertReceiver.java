package com.visionarytechsolutions.cibs1Pro.receiver;

//import com.visionarytechsolutions.cibs1Pro.utils.NotificationAlertActions;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;

public class NotificationAlertReceiver extends BroadcastReceiver {
	
	
	//NotificationAlertActions notificationActions;
	SharedPreferences sharedPreferences;
	Editor editor;
	
	@Override
	public void onReceive(Context context, Intent intent) {		
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
		if(sharedPreferences.getBoolean("isNotificationFirst", true) && !(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)){
			editor.putBoolean("isNotificationFirst", false);
			editor.putBoolean("isFirst", true);
			editor.commit();
		}else{
//			notificationActions = new NotificationAlertActions(context);
//			notificationActions.sendNotifications(false);
			
//			Intent intent1 = new Intent(context.getApplicationContext(), BackgroundVideoRecorder.class);
//	        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//	        intent1.putExtra("callPhone", false);
//	        context.startService(intent1);
		}
	}

}
