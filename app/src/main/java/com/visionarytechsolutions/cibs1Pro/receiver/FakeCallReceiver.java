package com.visionarytechsolutions.cibs1Pro.receiver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.FakeCallActivity;
import com.visionarytechsolutions.cibs1Pro.service.SimpleWakefulService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.LocationActions;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FakeCallReceiver extends WakefulBroadcastReceiver  {

	SharedPreferences sharedPreferences;
	Editor editor;
	AlertActions alertActions;
	public PowerManager.WakeLock wl;
	public static FakeCallReceiver instance;
	LocationActions locationActions;
	public boolean userInRange = false, interacted = false;
	Handler handler;
	Runnable runnable;
	
	@Override
	public void onReceive(final Context context, final Intent intent) {	 
		sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit(); 
        instance = this;
		locationActions = new LocationActions(context.getApplicationContext());
		alertActions = new AlertActions(context.getApplicationContext());
		locationActions.getCurrentLocation();
		runnable = new  Runnable() {
            @Override
            public void run() {
            	locationActions.cancelListener();
            	if(!interacted) callFakeCallActivity(context, intent);
            }
        };      
    	handler = new Handler();
    	handler.postDelayed(runnable, 15000);
	}	
	public void callFakeCallActivity(Context context, Intent intent){		      
       
     // This is the Intent to deliver to our service.
        Intent service = new Intent(context, SimpleWakefulService.class);
        
        Log.e("result11",""+sharedPreferences.getBoolean("deActivate", false));
        
        Log.e("result12",""+sharedPreferences.getBoolean("checkIn", false));

        startWakefulService(context, service);

			        /*                           to check subscription plan                                             */
		Boolean subscription=false;

		try{
			//DateFormat df1=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");//foramt date

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
			String str1=formatter.format(Calendar.getInstance().getTime());
			Log.e("datee",str1);

			//String str1 = "2017-06-06 11:35:36";
			Date date1 = formatter.parse(str1);

			String str2 = sharedPreferences.getString("subscriptionDateTime","");
			Date date2 = formatter.parse(str2);

			if(date1.before(date2))
			{
				Log.e("dateee1111","date2 is Greater than my date1");
				subscription=true;
			}
			else
			{
				Log.e("dateee1111","date2 is less than my date1");
				subscription=false;

			}
		}catch (ParseException e1){
			e1.printStackTrace();
		}
       /* if(subscription)
		{
			Toast.makeText(context,"Subs active",Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(context,"Subs expire",Toast.LENGTH_SHORT).show();

		}*/
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){			
			boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
			boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
			if(notificationAlertSet){
				alertActions.activateNotificationAlert(false);
			}else if(alertSet){
				alertActions.activateFakeCallAlert(false);
			}
	        }
	        else if(intent.getAction().equals("ManualActivation")&&!sharedPreferences.getBoolean("deActivate", false)&&subscription){

			MySharedPreferences.getInstance().storeData(context, "shake", "false");
			MySharedPreferences.getInstance().storeData(context, "threshold", "false");

        //}else if(intent.getAction().equals("ManualActivation")&&!sharedPreferences.getBoolean("deActivate", false)&&!sharedPreferences.getBoolean("checkIn", false)){
			if(sharedPreferences.getBoolean("isFirst", true) && !(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)){
				editor.putBoolean("isFirst", false);
				editor.commit();
			}else{						
				if(sharedPreferences.getBoolean("isLeaveSafeLocationChecked", false)){
					  userInRange = locationActions.isUserInRange();					  
						Log.e("userlocationsss123",userInRange+"");

				}
				Log.e("userlocationsss",userInRange+"");
				//boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);	&& !notificationAlertSet				
				if(!userInRange){
					//Toast.makeText(context, "User Not In Range", Toast.LENGTH_LONG).show();
		              PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		              boolean isScreenOn = pm.isScreenOn();
	            	  editor.putBoolean("isScreenOn", isScreenOn);
	            	  editor.commit();
		              if (!isScreenOn) {
		            	  wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Alarm");
		            	  wl.acquire();
		              }	 
					 if(!sharedPreferences.getBoolean("isPlaySet", false)){						 
						  Intent intentone = new Intent(context.getApplicationContext(), FakeCallActivity.class);
						  intentone.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						  context.startActivity(intentone);
					 }
				}else{
					alertActions.deactivateNotificationAlert(false);
					alertActions.activateFakeCallAlert(false);
					//Toast.makeText(context, "User In Range", Toast.LENGTH_LONG).show();
				}
			}
        }
	}

}
