package com.visionarytechsolutions.cibs1Pro.receiver;

import com.visionarytechsolutions.cibs1Pro.FakeCallActivity;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ScreenEventReceiver extends BroadcastReceiver {

	FakeCallActivity fakeCallActivity;
	AlertActions alertActions;
	
	public ScreenEventReceiver(FakeCallActivity fakeCallActivity) {
		this.fakeCallActivity = fakeCallActivity;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		fakeCallActivity.doActions();
		alertActions = new AlertActions(context);
		alertActions.activateNotificationAlert(true);
	}
}
