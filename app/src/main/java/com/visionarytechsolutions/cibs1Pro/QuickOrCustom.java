package com.visionarytechsolutions.cibs1Pro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class QuickOrCustom extends Activity {
    Button btnQuickSetup,btnCustomSetup;
    SharedPreferences sharedPreferences;
    Editor editor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quick_or_custom);
		btnQuickSetup=(Button)findViewById(R.id.btnQuickSetup);
		btnCustomSetup=(Button)findViewById(R.id.btnCustomSetup);
		btnQuickSetup.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(QuickOrCustom.this,QuickSetupOptions.class);
				startActivity(i);
				 finish();				
				
			}
		});
		btnCustomSetup.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(QuickOrCustom.this,FirstActivity.class);
				startActivity(i);
				 finish();

				sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		        editor = sharedPreferences.edit();
		        editor.putString("watchDemo", "true").apply();
			}
		});
	}}

	
