package com.visionarytechsolutions.cibs1Pro;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.adapter.NavDrawerListAdapter;
import com.visionarytechsolutions.cibs1Pro.fileUpload.NetworkOperations;
import com.visionarytechsolutions.cibs1Pro.model.NavDrawerItem;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ServerUtilities;
import com.visionarytechsolutions.cibs1Pro.service.RollcallAcceptService;
import com.visionarytechsolutions.cibs1Pro.service.ShakeService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.LocationActions;

public class MainActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	
	AlertActions alertActions;
	
	SharedPreferences sharedPreferences;
	Editor editor;
	
	public static MainActivity instance;
	
	Fragment fragment = null;
	
	public int position = 0;
	public AlertDialog alertDialog = null;
	public PowerManager.WakeLock wl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);		
		}catch(Exception e){
			super.onCreate(null);
			e.printStackTrace();
		}
		setContentView(R.layout.activity_main);		
		
		mTitle = mDrawerTitle = getTitle();
		
		instance = this;
		
		getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
		getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
		
//		alertActions = new AlertActions(getApplicationContext());
//		alertActions.activateFakeCallAlert(true);
		
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();	
		
		if(sharedPreferences.getBoolean("isShakeServiceStarted", false)){
			startService(new Intent(getApplicationContext(), ShakeService.class));
			editor.putBoolean("isShakeServiceStarted", true);
			editor.commit();
		}

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		
		// Check In Request
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));		
		// My Profile Data
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));		
		// PRESET SETTINGS
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		// SAFETY LOCATIONS
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		// EMERGENCY CONTACTS
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		// SAFETY SELFIE
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		// WEBSITE
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
		//INSTRUCTION MANUAL
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		//ROLL CALL
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));
		//QUICK SETUP
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(10, -1)));
		//ABOUT
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons.getResourceId(9, -1)));
				

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setBackgroundDrawable(new ColorDrawable(0xFF11A451));
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer_closed, //Nap menu toggle icon
				R.string.app_name,
				R.string.app_name
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
		
		showRollCallAlertDialog();
		/*if(sharedPreferences.getBoolean("checkIn", false))
		{
			showAlertCheckIn();
		}*/
		
		if(alertDialog == null)
			showAlertDialog();
		
		Log.e("dialogg",""+sharedPreferences.getBoolean("checkIn", false));
		//if(sharedPreferences.getBoolean("checkIn", false)||sharedPreferences.getBoolean("deActivate", false))
		//{
			//showAlertCheckIn();
			//displayView(2);
		//}
		
		/*if(alertDialog == null||sharedPreferences.getBoolean("checkIn", false))
			showAlertDialog();*/
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	/**
	 * Slide menu item click listener
	 * */
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		instance = null;
	}
	
	public void showAlertDialog(){
		boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
		boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
		if(!notificationAlertSet && !alertSet){		
			
			AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
			.setTitle("CIBS not activated!!")
            .setMessage("Do you want to activate CIBS?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,int which) {
					Intent alertActivity = new Intent(getApplicationContext(),AlertDialogActivity.class);
					alertActivity.putExtra("callFrom", "activateAlertBtn");
					alertActivity.putExtra("showMsg", true);
					editor.putBoolean("deActivate", false).apply();
					//editor.putBoolean("checkIn", false).apply();
					startActivity(alertActivity);
				}	            	
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
            	public void onClick(DialogInterface dialog, int which) {
            		Toast.makeText(getApplicationContext(), "Hint: Please Check In to Re-Activate CIBS", Toast.LENGTH_LONG).show();
            		dialog.dismiss();
            	}
            })
            .setCancelable(false)
            .create();
			alertDialog.setOnDismissListener(new OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {
//					showRollCallAlertDialog();
				}
			});
    		alertDialog.show();			
		}
	}
	
	public void changeRollcallStatus(String status){
		if(checkNetwork()){		
			String rollCallData = sharedPreferences.getString("rollCallData", "{}");
			try {
				JSONObject dataJson = new JSONObject(rollCallData);
				deviceId = dataJson.getString("deviceId");
				phone = sharedPreferences.getString("phone", "");
				email = sharedPreferences.getString("email", "");
				List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();				
				Log.e("deviceeid","jk"+deviceId);
				Log.e("phone",phone);
				Log.e("email",email);
				Log.e("status",status);
				Log.e("cmd","ChangeStatus");
				parameters.add(new Pair<String, String>("regId", deviceId));
				parameters.add(new Pair<String, String>("phone", phone));
				parameters.add(new Pair<String, String>("email", email));
				parameters.add(new Pair<String, String>("status", status));
				parameters.add(new Pair<String, String>("cmd", "ChangeStatus"));
				String url = "http://www.checkinbesafe.com/webservices/cibs_changestatus.php";
				NetworkOperations nop = new NetworkOperations(url, parameters, MainActivity.instance, "changestatus");
				nop.execute();				
				editor.putBoolean("rollCallRequestReceived", false);
				editor.commit();
				LocationActions locationActions = new LocationActions(instance);
				Location location = locationActions.getCurrentLocation();				
				try {
					JSONObject locationJson = new JSONObject("{}");
					if(location != null){
						locationJson.put("lat", location.getLatitude());					
						locationJson.put("lon", location.getLongitude());
					}
					changeRollcallUserLocation(locationJson.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				locationActions.cancelListener();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else
			Toast.makeText(MainActivity.instance, "Please check your network connection", Toast.LENGTH_LONG).show();	
	}
	
	public void changeRollcallUserLocation(String status){
		if(checkNetwork()){		
			String rollCallData = sharedPreferences.getString("rollCallData", "{}");
			try {
				JSONObject dataJson = new JSONObject(rollCallData);
				Log.e("status",status);
				status=status.replace("\"", "'");	
				deviceId = dataJson.getString("deviceId");
				phone = sharedPreferences.getString("phone", "");
				email = sharedPreferences.getString("email", "");
				List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();
				parameters.add(new Pair<String, String>("regId", deviceId));
				parameters.add(new Pair<String, String>("phone", phone));
				parameters.add(new Pair<String, String>("email", email));
				parameters.add(new Pair<String, String>("status", status));
				parameters.add(new Pair<String, String>("cmd", "ChangeUserLocation"));
				String url = "http://www.checkinbesafe.com/webservices/cibs_changestatus.php";
				NetworkOperations nop = new NetworkOperations(url, parameters, MainActivity.instance, "changeuserlocation");
				nop.execute();
				 
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else
			Toast.makeText(MainActivity.instance, "Please check your network connection", Toast.LENGTH_LONG).show();	
	}
	
	private boolean checkNetwork(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
    }
	
	String rollCallName = "", deviceId = "", phone = "", email = "";
	public void showRollCallAlertDialog(){		
		boolean rollCallRequestReceived = sharedPreferences.getBoolean("rollCallRequestReceived", false);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		boolean isScreenOn = pm.isScreenOn();
		if (!isScreenOn) {
			wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Alarm");
			wl.acquire();
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        }
		String rollCallData = sharedPreferences.getString("rollCallData", "{}");		
		if(rollCallRequestReceived){				
        	try {
				JSONObject dataJson = new JSONObject(rollCallData);
				rollCallName = dataJson.getString("name");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			alertDialog = new AlertDialog.Builder(MainActivity.instance)
			.setTitle("New RollCall Invitation Received from "+rollCallName)
            .setMessage("Do you want to participate?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,int which) {										
					changeRollcallStatus("Accepted");
					editor.putBoolean("rollCallRequestAccepted", true);
					editor.putBoolean("rollCallRequestReceived", false);
					editor.putBoolean("helpCheck", false);
					editor.putBoolean("inRouteCheck", false);
		               editor.commit();
		               Intent rollcallService = new Intent(instance,RollcallAcceptService.class);
		           	startService(rollcallService);
					displayView(8);
				}	            	
            })
            .setNegativeButton("No, Ignore", new DialogInterface.OnClickListener() {
            	public void onClick(DialogInterface dialog, int which) {
            		Toast.makeText(getApplicationContext(), "RollCall invitation from "+rollCallName+" ignored", Toast.LENGTH_LONG).show();
    				editor.putBoolean("rollCallRequestReceived", false);
    				editor.commit();
            		dialog.dismiss();
            		changeRollcallStatus("Rejected");
            	}
            })
            .setCancelable(false)
            .create();
			alertDialog.setOnDismissListener(new OnDismissListener() {				
				@Override
				public void onDismiss(DialogInterface dialog) {
					alertDialog = null;
				}
			});
    		alertDialog.show();			
		}
	}
	
	
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			mDrawerLayout.openDrawer(mDrawerList);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	public void displayView(int position) {
		// update the main content by replacing fragments
		fragment = null;
		this.position = position;
		switch (position) {
		case 0:
			fragment = new CheckInRequestFragment();
			break;
		case 1:
			fragment = new SignUpFragment(false);
			break;
		case 2:
			fragment = new PresetSettingsFragment(false);
			break;
		case 3:
			fragment = new SafeLocationsFragment(false);
			break;
		case 4:
			fragment = new ContactsListFragment(false);
			break;
		case 5:
			fragment = new MySelfieFragment(false);
			break;
		case 6:
			String url1 = "http://visionarytechsolutions.us/";
			Intent i1 = new Intent(Intent.ACTION_VIEW);
			i1.setData(Uri.parse(url1));
			startActivity(i1);
			break;
		case 7:
			openManual();
			break;
		case 8:
			boolean rollcallActivated = sharedPreferences.getBoolean("rollcallActivated", false);
			boolean rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
			
			Log.e("details11",rollcallActivated+"");
			Log.e("details11123",rollCallRequestAccepted+"");

			if(rollCallRequestAccepted) {
				Log.e("details11","1"+"");

				fragment = new RollCallFragmentAccept();
			}			
			else if(!rollcallActivated) 
			{
				Log.e("details11","2"+"");

				fragment = new RollCallFragment();
			}
			else 
			{	
				Log.e("details11","3"+"");

				fragment = new RollCallFragmentDeact();
			}			
			break;			
		case 9:
			fragment = new QuickSetupFragment();
			break;
		case 10:
			fragment = new aboutFragment();
			break;
		default:
			break;
		}
		if(position == 3 || position == 4){
			Intent confirmActivity = new Intent(getApplicationContext(),AlertDialogConfirm.class);
    		     confirmActivity.putExtra("fromMain", true);
			startActivityForResult(confirmActivity, 1);	
		}else if (fragment != null) {
			try
			{
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);	
			}
			catch(Exception e)
			{
				
			}
			
		} else {
			Log.e("MainActivity", "Error in creating fragment");
		}
		mDrawerLayout.closeDrawer(mDrawerList);
	}

    private void openManual() {
		// TODO Auto-generated method stub
    	File fileBrochure = new File(Environment.getExternalStorageDirectory() + "/CIBS/CIBS_Instructions_pro_v1.pdf");
        if (!fileBrochure.exists())
        {
             CopyAssetsbrochure();
        } 

        /** PDF reader code */
        File file = new File(Environment.getExternalStorageDirectory() + "/CIBS/CIBS_Instructions_pro_v1.pdf");        

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try 
        {
            getApplicationContext().startActivity(intent);
        } 
        catch (ActivityNotFoundException e) 
        {
             Toast.makeText(MainActivity.this, "NO Pdf Viewer", Toast.LENGTH_SHORT).show();
        }
	}
    
    private void CopyAssetsbrochure() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try 
        {
            files = assetManager.list("");
        } 
        catch (IOException e)
        {
            Log.e("tag", e.getMessage());
        }
        for(int i=0; i<files.length; i++)
        {
            String fStr = files[i];
            if(fStr.equalsIgnoreCase("CIBS_Instructions.pdf"))
            {
                InputStream in = null;
                OutputStream out = null;
                try 
                {
                  in = assetManager.open(files[i]);
                  out = new FileOutputStream(Environment.getExternalStorageDirectory() + "/CIBS/CIBS_Instructions_pro_v1.pdf");
                  copyFile(in, out);
                  in.close();
                  in = null;
                  out.flush();
                  out.close();
                  out = null;
                  break;
                } 
                catch(Exception e)
                {
                    Log.e("tag", e.getMessage());
                } 
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

         Boolean flag=true;


		if(resultCode == Activity.RESULT_OK){
			if (fragment != null) {
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);
				mDrawerLayout.closeDrawer(mDrawerList);
				//Toast.makeText(getApplicationContext(), "drawerr", Toast.LENGTH_LONG).show();

			} else {
				Log.e("MainActivity", "Error in creating fragment");
			}
		}else if(resultCode == Activity.RESULT_CANCELED){
			Toast.makeText(getApplicationContext(), "Permission Denied to view the list", Toast.LENGTH_LONG).show();
			displayView(0);
			flag=false;
		}
		//Toast.makeText(getApplicationContext(), flag+"", Toast.LENGTH_LONG).show();
		if(flag&&CheckInRequestFragment.booelanPackageUpdate)
		{
			updatePackage(CheckInRequestFragment.packageUpdate);
			CheckInRequestFragment.booelanPackageUpdate=false;
		}
	}
	
	public void showAlertCheckIn(){			
			AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
			.setTitle("CIBS not activated!!")
            .setMessage("Do you want to activate CIBS?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,int which) {
					Intent alertActivity = new Intent(getApplicationContext(),AlertDialogActivity.class);
					alertActivity.putExtra("callFrom", "activateAlertBtn");
					alertActivity.putExtra("showMsg", true);
					editor.putBoolean("deActivate", false).apply();
					editor.putBoolean("checkIn", false).apply();
					startActivity(alertActivity);
				}	            	
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
            	public void onClick(DialogInterface dialog, int which) {
            		Toast.makeText(getApplicationContext(), "Hint: Please Check In to Re-Activate CIBS", Toast.LENGTH_LONG).show();
            		dialog.dismiss();
            	}
            })
            .setCancelable(false)
            .create();
			alertDialog.setOnDismissListener(new OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {
//					showRollCallAlertDialog();
				}
			});
    		alertDialog.show();			
		
	}
	void update()
	{
		displayView(0);
	}
	void updatePackage(final String packageUpdate) {
		try {

			AsyncTask<Void, Void, Void> updatePackage = new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					// Register on our server
					// On server creates a new user
					String response = ServerUtilities.updatePackage(MainActivity.this, sharedPreferences.getString("userId", ""), packageUpdate);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {

					editor=sharedPreferences.edit();
					editor.putBoolean("subscription",true).apply();


					//Toast.makeText(MainActivity.this, "Package Updated", Toast.LENGTH_SHORT).show();
					/*try {
						JSONObject obj = new JSONObject(response);
						if (obj.getString("success").equals("0")) {
							showInAppPurchaseDialog();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}*/
				}
			};
			updatePackage.execute(null, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
