package com.visionarytechsolutions.cibs1Pro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.receiver.FakeCallReceiver;
import com.visionarytechsolutions.cibs1Pro.receiver.ScreenEventReceiver;
import com.visionarytechsolutions.cibs1Pro.service.MouthService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

public class FakeCallActivity extends Activity {

	SharedPreferences sharedPreferences;
	Editor editor;
	Vibrator vibrator;
	Ringtone ringtone;
	AlertActions alertActions;
	boolean isUserTouched = false, isScreenOn = true, isAlreadyActivated = false;
	public static FakeCallActivity instance;
	ScreenEventReceiver screenEventReceiver;
	TextView callingMsgTV;
	Button btnCheckin,btnDeactivate,btnSendAlert;
	Handler handler;
	Runnable runnable;
	MediaPlayer mediaPlayer;	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		
	   sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        alertActions = new AlertActions(getApplicationContext());     
        isScreenOn = sharedPreferences.getBoolean("isScreenOn", true);
        
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        
		setContentView(R.layout.fake_screen);
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
		
        screenEventReceiver = new ScreenEventReceiver (instance);
        registerReceiver(screenEventReceiver, filter);		
		
        editor.putBoolean("isPlaySet", true);
        editor.commit();
        if(sharedPreferences.getBoolean("isRingtoneChecked", false))
        	playSound();
        if(sharedPreferences.getBoolean("isVibrateChecked", false))
			playVibration();
		alertActions.deactivateFakeCallAlert(false);
		autoCancelAlert();
	}
	
	public void userTouchedScreen(View v){
		isUserTouched = true;		
		cancelAlert();		
		setContentView(R.layout.fragment_checkin);
		callingMsgTV = (TextView) findViewById(R.id.callingMsgTV);
		btnCheckin = (Button) findViewById(R.id.buttonCheckin);
		btnDeactivate = (Button) findViewById(R.id.buttonDeactivate);
		btnSendAlert = (Button) findViewById(R.id.buttonSendAlert);
		callingMsgTV.setVisibility(View.VISIBLE);
		btnCheckin.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				isAlreadyActivated = true;
				if(handler != null){ 
					handler.removeCallbacks(runnable);
				}
				Intent alertActivity = new Intent(FakeCallActivity.this,AlertDialogActivity.class);
				alertActivity.putExtra("callFrom", "checkinBtn");
				startActivity(alertActivity);		
			}
		});
		
		btnDeactivate.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				isAlreadyActivated = true;
				if(handler != null){ 
					handler.removeCallbacks(runnable);
				}
				Intent alertActivity = new Intent(FakeCallActivity.this,AlertDialogActivity.class);
				alertActivity.putExtra("callFrom", "deactivateBtn");
				startActivity(alertActivity);	
			}
		});
		
		btnSendAlert.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(handler != null){ 
					handler.removeCallbacks(runnable);
				}
				doActions();
				if(MainActivity.instance != null){
					MainActivity.instance.finish();
				}
			}
		});
	}	
	public void playSound(){		
	  /* int resID=getResources().getIdentifier("ringdefault", "raw", getPackageName());
        mediaPlayer=MediaPlayer.create(FakeCallActivity.this,resID);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();*/
		Intent svc =new Intent(this, MouthService.class);
		stopService(svc);
		
		Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }
        ringtone = RingtoneManager.getRingtone(FakeCallActivity.this, alarmUri);
        ringtone.play();
	}	
	public void playVibration(){
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		if (vibrator.hasVibrator()) {
			long[] pattern = {0,300,400,500};
			vibrator.vibrate(pattern, 0);
		}
	}	
	@Override
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();
		if(!isAlreadyActivated) 
			doActions();				
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if(!isAlreadyActivated)
			doActions();
	}
	
	public void cancelAlert(){
		if(vibrator != null)	vibrator.cancel();
		if(ringtone != null)	ringtone.stop();
		editor.putBoolean("isPlaySet", false);
          editor.commit();		
		if(screenEventReceiver != null){
			instance.unregisterReceiver(screenEventReceiver);
			screenEventReceiver = null;
		}		
	}		
	
	public void doActions(){
		if(!isUserTouched){
			cancelAlert();
		}
		alertActions.activateNotificationAlert(true);
		isAlreadyActivated = true;
		finish();
		if(!isScreenOn && FakeCallReceiver.instance != null && FakeCallReceiver.instance.wl != null)
    		FakeCallReceiver.instance.wl.release();
	}
	public void autoCancelAlert(){
		
		runnable = new  Runnable() {
            @Override
            public void run() {
            	if(!isAlreadyActivated){
	            	doActions();	            	
            	}else{
            		finish();
            	}
            }
        };      	
		runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		    	handler = new Handler();
		    	handler.postDelayed(runnable, 90000);
		    }
		});
	}	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try
		{
		mediaPlayer.stop();
		}
		catch(Exception e)
		{
			
		}
	}
}
