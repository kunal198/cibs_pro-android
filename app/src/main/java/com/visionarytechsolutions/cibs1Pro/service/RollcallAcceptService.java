package com.visionarytechsolutions.cibs1Pro.service;

import com.visionarytechsolutions.cibs1Pro.MainActivity;
import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.RollCallFragmentAccept;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class RollcallAcceptService extends Service {
	
	SharedPreferences sharedPreferences;
	public static RollcallAcceptService instance = null;
	Handler handler,handlerHelp;
	Runnable runnable,runnableHelp;
	boolean rollCallRequestAccepted = false, autoResponseSet = false;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		instance = this;
	}
	
	public void onDestroy(){
	    super.onDestroy();
	    instance = null;
	}
	
	public void onStart(Intent intent, int startId){
		autoResponseSet = false;
		autoAlert();
		autoSetHelp();
	}
	
	protected void onResume(){
		
	}
	
	protected void onPause(){
		
	}
	
	public void onStop(){
		
	}
	
	
	
	public void autoAlert(){		
		rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
		runnable = new  Runnable() {
            @Override
            public void run() {
            	if(rollCallRequestAccepted && !autoResponseSet && instance!=null){
        			try {
//        	    	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        				 Uri choice = Uri.parse("android.resource://" + getPackageName() + "/"+R.raw.rollcallchoice);
//        	    	    Ringtone r = RingtoneManager.getRingtone(instance, choice);
//        	    	    r.play();
        				MediaPlayer mediaPlayer= MediaPlayer.create(MainActivity.instance, R.raw.rollcallchoice);
        	        	mediaPlayer.start();
        	    	    Toast.makeText(getApplicationContext(), "Please Respond With Your Choice", Toast.LENGTH_LONG).show();
        	    	} catch (Exception e) {
        	    	    e.printStackTrace();
        	    	}
        			autoAlert();
        		}else{
        			stopSelf();
        		}
            }
        }; 				
    	handler = new Handler();
    	handler.postDelayed(runnable, 10000);		   
	}

	public void autoSetHelp(){		
		rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
		runnableHelp = new  Runnable() {
            @Override
            public void run() {
            	if(rollCallRequestAccepted && !autoResponseSet && instance!=null){
        			if(RollCallFragmentAccept.instance != null){
        				RollCallFragmentAccept.instance.setAutoHelp();
        			}
            		autoResponseSet = true;
        		}else{
        			stopSelf();
        		}
            }
        }; 				
    	handlerHelp = new Handler();
    	handlerHelp.postDelayed(runnableHelp, 60000);		   
	}
}
