package com.visionarytechsolutions.cibs1Pro.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.visionarytechsolutions.cibs1Pro.PresetSettingsFragment;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MouthService extends Service{

	private static final int sampleRate = 8000;
	private AudioRecord audio;
	private int  bufferSize;
	private double lastLevel = 0;
	private Thread thread;
	private static final int SAMPLE_DELAY = 75;
	private Context context;

	private Handler 	mHandler = new Handler();	
	private static final int POLL_INTERVAL = 500;
	boolean cancel = false;
	RecorderSingleton recorded = null;
	public static boolean  isServiceWorking = false;
	
	SharedPreferences sharedPreferences;
	Editor editor;
	int min = 0;

	List<Double> list = new ArrayList<Double>();

	// Create runnable thread to Monitor Voice


	private Runnable mPollTask = new Runnable() {
		public void run() {  
			
			try{Thread.sleep(SAMPLE_DELAY);}catch(InterruptedException ie){ie.printStackTrace();}
			readAudioBuffer();
			Log.e("Volume of voice out","leveleeeee"+lastLevel);
			//if(lastLevel >= 190){
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			String str = sdf.format(new Date());
			Log.e("timeeeee",str);
			//if(sharedPreferences.getString("previoustime","").equals(""))
			//{
			//editor.putString("previoustime", str).apply();
			//}
			//else
			//{
			
			try
			{
			String prvTime=sharedPreferences.getString("previoustime", "");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
			Date date1 = simpleDateFormat.parse(prvTime);
			Date date2 = simpleDateFormat.parse(str);

			long difference = date2.getTime() - date1.getTime(); 
			int days = (int) (difference / (1000*60*60*24));  
			int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60)); 
			 min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
			hours = (hours < 0 ? -hours : hours);
			Log.e("======= min"," :: "+min);
			/*if(min>=2)
			{
			//Toast.makeText(MouthService.this, "working", Toast.LENGTH_SHORT).show();
			}
			else
			{
			//Toast.makeText(MouthService.this, "less than two minutee", Toast.LENGTH_SHORT).show();
			}*/
			}
			catch(Exception e)
			{	
			}			
			//}
			/*if(lastLevel>=85&&sharedPreferences.getString("previoustime", "").equals(""))
			{
				
			}*/
			
			if((lastLevel>=160&&sharedPreferences.getString("previoustime","").equals(""))||(lastLevel >= 160&&min>=15)){
				if(mHandler != null){
					mHandler.removeCallbacks(mPollTask);
				}
				editor.putString("previoustime",str).apply();
				callForHelp();
			}else{
				mHandler.postDelayed(mPollTask, POLL_INTERVAL);
			}
		}
	};	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub

		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();
		//		super.onCreate();
		try {
			//			bufferSize = AudioRecord
			//					.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO,
			//							AudioFormat.ENCODING_PCM_16BIT);


			if(recorded == null){
				recorded = RecorderSingleton.getInstance();
			}

			bufferSize=	recorded.getBufferSize();

			isServiceWorking = true;

		} catch (Exception e) {
			android.util.Log.e("TrackingFlow", "Exception", e);
		}

		if(recorded.init()){
			recorded.start();
		}

		//		audio = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate,
		//					AudioFormat.CHANNEL_IN_MONO,
		//					AudioFormat.ENCODING_PCM_16BIT, bufferSize);
		//
		//			audio.startRecording();




		mHandler.postDelayed(mPollTask, POLL_INTERVAL);


		//		
		//		


	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return super.onStartCommand(intent, flags, startId);
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		//		isServiceWorking = false;

		if(recorded != null){
			recorded.stop();
		}

		if(mHandler != null){
			mHandler.removeCallbacks(mPollTask);
		}
		stopSelf();
	}

	private void readAudioBuffer() {

		//		audio.startRecording();
		try {

			short[] buffer = new short[bufferSize];

			int bufferReadResult = 1;

			if (recorded != null) {

				// Sense the voice...
				Log.e("Volume of bufferSize","bufferSize===="+recorded.getBufferSize());
				//				bufferReadResult = audio.read(buffer, 0, bufferSize);
				bufferReadResult = recorded.read(buffer);
				Log.e("Volume of bufferReadResult","bufferReadResult===="+bufferReadResult);

				double sumLevel = 0;
				for (int i = 0; i < bufferReadResult; i++) {
					sumLevel += buffer[i];
				}

				lastLevel = Math.abs((sumLevel / bufferReadResult));


				if(lastLevel == 0.0){

					list.add(lastLevel);
				}

				if(list.size() >= 4){
					list.clear();
					onDestroy();

					Intent I = new Intent(getApplicationContext(), MouthService.class);
					I.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startService(I);
					
					

				}

				//				if(lastLevel == 0.0){
					//					
				//					
				//					if(MouthService.isServiceWorking){
				//			    		
				//			    		PresetSettingsFragment.switchAudio.setChecked(true);
				//			    	}
				////					
				//////					
				////					
				////					
				//////					onDestroy();
				//////
				//////					Intent I = new Intent(getApplicationContext(), MouthService.class);
				//////					I.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				//////					startService(I);
				////
				////
				//				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callForHelp() {
		MySharedPreferences.getInstance().storeData(getApplicationContext(), "shake", "false");
		MySharedPreferences.getInstance().storeData(getApplicationContext(), "threshold", "true");
		Handler handler=new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				System.out.println("I'm in handler");
				Toast.makeText(getApplicationContext(), "CIBS Alert Activated.",
						Toast.LENGTH_LONG).show(); 
			}
		}, 100);             


		//stop();

		// Show alert when noise thersold crossed

		//        stopSelf();

		isServiceWorking = false;

		onDestroy();
		AlertActions alertActions=new AlertActions(getApplicationContext());
		alertActions.activateNotificationAlert(true);

		//		mSensor.stop();

	}


}
