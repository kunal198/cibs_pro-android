package com.visionarytechsolutions.cibs1Pro.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.service.ShakeListener.ShakeListener1;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

public class ShakeService extends Service{


	 SensorManager mSensorManager;
	private ShakeEventListener mSensorListener;
	private ShakeListener mSensorListener1;
	Sensor linearAcceleration;

	SharedPreferences sharedPreferences;
	AlertActions alertActions;
	int Count = 0;
	ShakeListener1 mListner;

	int count = 0;

	@Override
	public IBinder onBind(Intent intent){
		Log.v("ShakeService", "onBind");
		return null;
	}


	public void onCreate(){
		super.onCreate();
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		linearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // initialization of LINEAR_ACCELERATION


		mSensorListener1 = new ShakeListener(getApplicationContext()); 
		mSensorListener1.setOnShakeListener(new ShakeListener.ShakeListener1() {

			@Override
			public void onShake() {
				// TODO Auto-generated method stub
				sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
				Boolean subscription = false;
				try{
					//DateFormat df1=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");//foramt date

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
					String str1=formatter.format(Calendar.getInstance().getTime());
					Log.e("datee",str1);

					//String str1 = "2017-06-06 11:35:36";
					Date date1 = formatter.parse(str1);

					String str2 = sharedPreferences.getString("subscriptionDateTime","");
					Date date2 = formatter.parse(str2);

					if(date1.before(date2))
					{
						Log.e("dateee1111","date2 is Greater than my date1 "+str2+"     "+str1);
						subscription=true;
					}
					else
					{
						Log.e("dateee1111","date2 is less than my date1");
						subscription=false;
					}
				}catch (ParseException e1){
					e1.printStackTrace();
				}
				if((sharedPreferences.getBoolean("isShakeChecked", false))&&sharedPreferences.getBoolean("subscription",false)){
					//					
					//						alertActions = new AlertActions(getApplicationContext());
					//						alertActions.activateNotificationAlert(true);
					count = count +1;
					if(count == 1){
						Toast.makeText(getApplicationContext(), "Please shake two more time", 12345).show();
					}
					if(count == 2){
						Toast.makeText(getApplicationContext(), "Please shake one more time", 12345).show();
					}
//					Toast.makeText(getApplicationContext(), "asasas", 12345).show();
					if(count == 3){
						mSensorManager.unregisterListener(mSensorListener1);
						mSensorListener1.setOnShakeListener(null);
					
						mSensorListener1 = null;
						onDestroy();
						
						Handler handler=new Handler();
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
														         
						          System.out.println("I'm in handler");
								Toast.makeText(getApplicationContext(), "CIBS Alert Activated.",
										Toast.LENGTH_LONG).show(); 
							}
						}, 100);   
												
						final Vibrator vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
						vibe.vibrate(300);
	
												count = 0;
																	  									
					     MySharedPreferences.getInstance().storeData(getApplicationContext(), "shake", "true");
			               MySharedPreferences.getInstance().storeData(getApplicationContext(), "threshold", "false");	
			               
			              // Toast.makeText(getApplicationContext(), "SHAKE "+MySharedPreferences.getInstance().getData(getApplicationContext(), "shake"), Toast.LENGTH_SHORT).show();
						   // Toast.makeText(getApplicationContext(), "threshold "+MySharedPreferences.getInstance().getData(getApplicationContext(), "threshold"), Toast.LENGTH_SHORT).show();
												
						alertActions = new AlertActions(getApplicationContext());
						alertActions.activateNotificationAlert(true);	
												
					}

				}

			}
		});
	}
	public void onDestroy(){	
		super.onDestroy();
		Log.v("ShakeService", "onDestroy");
//		mSensorManager.unregisterListener(mSensorListener,linearAcceleration);
		stopSelf();
		
	}

	public void onStart(Intent intent, int startId){
		
//		mSensorManager.registerListener(mSensorListener1,
//				linearAcceleration,
//				SensorManager.SENSOR_DELAY_NORMAL);  
	}

	protected void onResume(){
//		mSensorManager.registerListener(mSensorListener,
//				linearAcceleration,
//				SensorManager.SENSOR_DELAY_NORMAL);
	}

	protected void onPause(){
//		mSensorManager.unregisterListener(mSensorListener);
	}
}
