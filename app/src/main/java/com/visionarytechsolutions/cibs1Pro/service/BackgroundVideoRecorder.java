package com.visionarytechsolutions.cibs1Pro.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Timer;
import java.util.TimerTask;

import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;
import com.visionarytechsolutions.cibs1Pro.utils.NotificationAlertActions;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

public class BackgroundVideoRecorder extends Service implements SurfaceHolder.Callback {

	private WindowManager windowManager;
	private SurfaceView surfaceView;
	@SuppressWarnings("deprecation")
	private Camera camera = null;
	private MediaRecorder mediaRecorder = null;
	String fileName = "CIBS_Record.mp4",audioFilePath="";
	boolean video = true, callPhone = false, recordNone = false;
	NotificationAlertActions notificationActions;
	SharedPreferences sharedPreferences;
	String imagePath,videoPath,threshold,shake,call,text,email;;
	Timer t;
	@Override
	public void onCreate() {
		MySharedPreferences.getInstance().storeData(getApplicationContext(), "shake", "false");
		MySharedPreferences.getInstance().storeData(getApplicationContext(), "threshold", "false");
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		recordNone = (!sharedPreferences.getBoolean("recordTypeVideo", false) && !sharedPreferences.getBoolean("recordTypeAudio", false));
		if(!recordNone){
			windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
			surfaceView = new SurfaceView(this);
			LayoutParams layoutParams = new WindowManager.LayoutParams(
					1, 1,
					WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
					WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT
			);
			layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
			windowManager.addView(surfaceView, layoutParams);
			surfaceView.getHolder().addCallback(this);
		}
	}
	// Method called right after Surface created (initializing and starting MediaRecorder)
	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {



		mediaRecorder = new MediaRecorder();

		//    	  mediaRecorder = new MediaRecorder();
		File myDirectory = new File(Environment.getExternalStorageDirectory(), "CIBS");
		if(!myDirectory.exists()) {
			myDirectory.mkdirs();
		}

		File dir = new File(myDirectory,"Records");
		deleteDirectory(dir);
		if (dir.exists()) {
			dir.delete();
		}
		dir.mkdirs();
		audioFilePath = dir.getAbsolutePath()+"/"+ fileName;
		Log.e("resultsssfinalss", "fianl"+audioFilePath);
		if(video){
			boolean found = false;
			int i;
			for (i=0; i< Camera.getNumberOfCameras(); i++) {
				Camera.CameraInfo newInfo = new Camera.CameraInfo();
				Camera.getCameraInfo(i, newInfo);
				if (newInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
					found = true;
					break;
				}
			}
			try{
				if(camera==null)
				{
					if(found){
						camera = Camera.open(i);

					}else{
						camera = Camera.open();
					}
				}
				camera.stopPreview();
				camera.unlock();

			}catch(Exception e){
				e.getCause();
			}
                /*if(camera==null)
                {
               	 try{
      				if(found){
      					camera = Camera.open(i);

      				}else{
      					camera = Camera.open();
      				}
      				camera.stopPreview();

      			}catch(Exception e){
      				e.getCause();
      			} 
                }*/


			mediaRecorder.setCamera(camera);
			mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mediaRecorder.setVideoSize(640,480);
//			mediaRecorder.setVideoFrameRate(30);
			mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
			mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
			mediaRecorder.setOutputFile(audioFilePath);

			//		
		}else{
			mediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
			mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			mediaRecorder.setAudioSamplingRate(96 * 1024);
			mediaRecorder.setAudioEncodingBitRate(1024 * 1024);
			mediaRecorder.setAudioChannels(2);
			mediaRecorder.setOutputFile(audioFilePath);

		}
		//        mediaRecorder.setOutputFile(audioFilePath);
		try {
			mediaRecorder.prepare();
			Thread.sleep(1000);
		}catch(Exception e){
			Intent intent = new Intent(getApplicationContext(), BackgroundVideoRecorder.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("callPhone", callPhone);
			startService(intent);
		}
		try{
			mediaRecorder.start();
		}catch(Exception e){
			e.getCause();
		}
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				stopSelf();
			}
			//},5000);
		}, 60000);
	}
	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			if (files == null) {
				return true;
			}
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	// Stop recording and remove SurfaceView
	@Override
	public void onDestroy() {
		recordNone = (!sharedPreferences.getBoolean("recordTypeVideo", false) && !sharedPreferences.getBoolean("recordTypeAudio", false));
		if(!recordNone){
			try
			{
				mediaRecorder.stop();
				mediaRecorder.reset();
				mediaRecorder.release();
				windowManager.removeView(surfaceView);
				if(video){
					camera.lock();
					camera.release();
				}
				notificationActions = new NotificationAlertActions(getApplicationContext());
				notificationActions.sendNotifications(callPhone,audioFilePath,getApplicationContext(),imagePath,videoPath,threshold,shake,call,text,email);
			}
			catch(Exception e)
			{

			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		imagePath=intent.getStringExtra("snapshotImagePath");
		videoPath=intent.getStringExtra("snapshotVideoPath");
		threshold=intent.getStringExtra("threshold");
		shake=intent.getStringExtra("shake");
		call=intent.getStringExtra("call");
		text=intent.getStringExtra("text");
		email=intent.getStringExtra("email");

		//intent.putExtra("snapshotImagePath", sharedPreferences.getString("snapshotImagePath", ""));
		// intent.putExtra("snapshotVideoPath", sharedPreferences.getString("snapshotVideoPath", ""));

		Log.e("romyImage","sd"+imagePath);
		Log.e("romyVideo","sd"+videoPath);

		recordNone = (!sharedPreferences.getBoolean("recordTypeVideo", false) && !sharedPreferences.getBoolean("recordTypeAudio", false));
		if(!recordNone){
			if(intent != null)
				callPhone = intent.getBooleanExtra("callPhone", false);
			else
				callPhone = true;

			if(mediaRecorder != null){
				mediaRecorder.release();
				mediaRecorder = null;
				windowManager.removeView(surfaceView);
				windowManager = null;
				surfaceView = null;
				if(video){
					camera.lock();
					camera.release();
					camera = null;
				}
				try
				{
					t.cancel();
				}
				catch(Exception e){}
				windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
				surfaceView = new SurfaceView(this);
				LayoutParams layoutParams = new WindowManager.LayoutParams(
						1, 1,
						WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
						WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
						PixelFormat.TRANSLUCENT
				);
				layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
				windowManager.addView(surfaceView, layoutParams);
				surfaceView.getHolder().addCallback(this);
			}
			sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
			video = sharedPreferences.getBoolean("recordTypeVideo", false);
			t = new Timer();
		}else{

			if(intent != null)
				callPhone = intent.getBooleanExtra("callPhone", false);
			else
				callPhone = true;
			notificationActions = new NotificationAlertActions(getApplicationContext());
			notificationActions.sendNotifications(callPhone,audioFilePath,getApplicationContext(),imagePath,videoPath,threshold,shake,call,text,email);
			stopSelf();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {}

	@Override
	public IBinder onBind(Intent intent) { return null; }

}
