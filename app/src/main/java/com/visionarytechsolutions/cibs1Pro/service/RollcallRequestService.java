package com.visionarytechsolutions.cibs1Pro.service;

import com.visionarytechsolutions.cibs1Pro.MainActivity;
import com.visionarytechsolutions.cibs1Pro.R;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class RollcallRequestService extends Service {

	SharedPreferences sharedPreferences;
	RollcallRequestService instance;
	Handler handler;
	Runnable runnable;
	boolean rollCallRequestReceived, isFirstTime = true;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		instance = this;
	}

	public void onDestroy() {
		super.onDestroy();
	}

	public void onStart(Intent intent, int startId) {
		rollCallRequestReceived = sharedPreferences.getBoolean("rollCallRequestReceived", false);
		if (rollCallRequestReceived)
			startNewActivity();
		else
			stopSelf();
	}

	protected void onResume() {

	}

	protected void onPause() {

	}

	public void onStop() {

	}

	public void startNewActivity() {
		rollCallRequestReceived = sharedPreferences.getBoolean("rollCallRequestReceived", false);
		if (rollCallRequestReceived) {
			try {
				if (isFirstTime) {
					Uri invitation = Uri.parse("android.resource://" + getPackageName() + "/"
							+ R.raw.rollcallinvitation);
					Ringtone r = RingtoneManager.getRingtone(instance, invitation);
					r.play();
					isFirstTime = false;
				}
				MediaPlayer mediaPlayer = MediaPlayer.create(MainActivity.instance, R.raw.rollcallinvitation);
				mediaPlayer.start();
				Toast.makeText(getApplicationContext(), "Please Respond to Rollcall Request", Toast.LENGTH_LONG)
						.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (MainActivity.instance == null) {
				Intent i = new Intent(instance, MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(i);
			} else {
				if (MainActivity.instance.alertDialog == null)
					MainActivity.instance.showRollCallAlertDialog();
			}
			autoAlert();
		} else {
			stopSelf();
		}
	}

	public void autoAlert() {
		rollCallRequestReceived = sharedPreferences.getBoolean("rollCallRequestReceived", false);
		runnable = new Runnable() {
			@Override
			public void run() {
				if (rollCallRequestReceived)
					startNewActivity();
				else {
					handler.removeCallbacks(runnable);
					stopSelf();
				}
			}
		};
		handler = new Handler();
		handler.postDelayed(runnable, 15000);
	}
}
