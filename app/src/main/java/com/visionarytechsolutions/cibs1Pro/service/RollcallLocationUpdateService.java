package com.visionarytechsolutions.cibs1Pro.service;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.visionarytechsolutions.cibs1Pro.MainActivity;
import com.visionarytechsolutions.cibs1Pro.fileUpload.NetworkOperations;
import com.visionarytechsolutions.cibs1Pro.utils.LocationActions;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.util.Pair;
import android.widget.Toast;

public class RollcallLocationUpdateService extends Service {
	
	SharedPreferences sharedPreferences;
	public static RollcallLocationUpdateService instance = null;
	Handler handler,handlerHelp;
	Runnable runnable,runnableHelp;
	boolean rollCallRequestAccepted = false, helpChecked = false;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		instance = this;
	}
	
	public void onDestroy(){
	    super.onDestroy();
	    instance = null;
	}
	
	public void onStart(Intent intent, int startId){
		helpChecked = false;
		autoAlert();
	}
	
	protected void onResume(){
		
	}
	
	protected void onPause(){
		
	}
	
	public void onStop(){
		
	}
	
	
	
	public void autoAlert(){		
		rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
		helpChecked = sharedPreferences.getBoolean("helpCheck", false);
		runnable = new  Runnable() {
            @Override
            public void run() {
            	if(rollCallRequestAccepted && helpChecked && instance!=null){
            		LocationActions locationActions = new LocationActions(instance);
    				Location location = locationActions.getCurrentLocation();				
    				try {
    					JSONObject locationJson = new JSONObject("{}");
    					if(location != null){
	    					locationJson.put("lat", location.getLatitude());					
	    					locationJson.put("lon", location.getLongitude());
    					}
    					changeRollcallUserLocation(locationJson.toString());
    				} catch (JSONException e) {
    					e.printStackTrace();
    				}
    				locationActions.cancelListener();
        			autoAlert();
        		}else{
        			stopSelf();
        		}
            }
        }; 				
    	handler = new Handler();
    	handler.postDelayed(runnable, 900000);		   
	}
	
	public void changeRollcallUserLocation(String status){
		if(checkNetwork()){		
			String rollCallData = sharedPreferences.getString("rollCallData", "{}");
			try {
				JSONObject dataJson = new JSONObject(rollCallData);
				String deviceId = dataJson.getString("deviceId");
				String phone = sharedPreferences.getString("phone", "");
				String email = sharedPreferences.getString("email", "");
				List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();
				parameters.add(new Pair<String, String>("regId", deviceId));
				parameters.add(new Pair<String, String>("phone", phone));
				parameters.add(new Pair<String, String>("email", email));
				parameters.add(new Pair<String, String>("status", status));
				parameters.add(new Pair<String, String>("cmd", "ChangeUserLocation"));
				String url = "http://www.checkinbesafe.com/webservices/cibs_changestatus.php";
				NetworkOperations nop = new NetworkOperations(url, parameters, MainActivity.instance, "changeuserlocation");
				nop.execute();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}else
			Toast.makeText(MainActivity.instance, "Please check your network connection", Toast.LENGTH_LONG).show();	
	}
	
	private boolean checkNetwork(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
    }
}
