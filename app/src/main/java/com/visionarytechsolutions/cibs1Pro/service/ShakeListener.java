package com.visionarytechsolutions.cibs1Pro.service;

import android.content.Context;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.widget.Toast;

public class ShakeListener implements SensorListener {
	  private static final int FORCE_THRESHOLD = 1900;
	  private static final int TIME_THRESHOLD = 220;
	  private static final int SHAKE_TIMEOUT = 500;
	  private static final int SHAKE_DURATION = 1000;
	  private static final int SHAKE_COUNT = 3;

	  private SensorManager mSensorMgr;
	  private float mLastX=-1.0f, mLastY=-1.0f, mLastZ=-1.0f;
	  private long mLastTime;
	  private ShakeListener1 mShakeListener;
	  private Context mContext;
	  private int mShakeCount = 0;
	  private long mLastShake;
	  private long mLastForce;

	  public interface ShakeListener1
	  {
	    public void onShake();
	  }

	  public ShakeListener(Context context) 
	  { 
	    mContext = context;
	    resume();
	  }

	  public void setOnShakeListener(ShakeListener1 listener)
	  {
	    mShakeListener = listener;
	  }

	  public void resume() {
	    mSensorMgr = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
	    if (mSensorMgr == null) {
	      throw new UnsupportedOperationException("Sensors not supported");
	    }
	    boolean supported = mSensorMgr.registerListener(this, SensorManager.SENSOR_ACCELEROMETER, SensorManager.SENSOR_DELAY_GAME);
	    if (!supported) {
	      mSensorMgr.unregisterListener(this, SensorManager.SENSOR_ACCELEROMETER);
	      throw new UnsupportedOperationException("Accelerometer not supported");
	    }
	  }

	  public void pause() {
	    if (mSensorMgr != null) {
	      mSensorMgr.unregisterListener(this, SensorManager.SENSOR_ACCELEROMETER);
	      mSensorMgr = null;
	    }
	  }

	  public void onAccuracyChanged(int sensor, int accuracy) { }

	  public void onSensorChanged(int sensor, float[] values) 
	  {
	    if (sensor != SensorManager.SENSOR_ACCELEROMETER) return;
	    long now = System.currentTimeMillis();

	    if ((now - mLastForce) > SHAKE_TIMEOUT) {
	      mShakeCount = 0;
	    }

	    if ((now - mLastTime) > TIME_THRESHOLD) {
	      long diff = now - mLastTime;
	      float speed = Math.abs(values[SensorManager.DATA_X] + values[SensorManager.DATA_Y] + values[SensorManager.DATA_Z] - mLastX - mLastY - mLastZ) / diff * 10000;
	     
//	      double acceleration = Math.sqrt(Math.pow(mLastX, 2) +
//                  Math.pow(mLastY, 2) +
//                  Math.pow(mLastZ, 2)) - SensorManager.GRAVITY_EARTH;
//          Log.d(APP_NAME, "Acceleration is " + acceleration + "m/s^2");
	      if (speed > FORCE_THRESHOLD) {
	        if ((now - mLastShake > SHAKE_DURATION)) {
	          mLastShake = now;
	          mShakeCount = 0;
	          if (mShakeListener != null) { 
	        	  mShakeListener.onShake(); 
//	        	  if(Round(mLastX,4)>10.0000){
////	                  Log.d("sensor", "X Right axis: " + x);
////	        		  mShakeListener.onShake(); 
////	                  Toast.makeText(mContext, "Right shake detected", Toast.LENGTH_SHORT).show();
//	              }
//	              else if(Round(mLastX,4)<-10.0000){
////	                  Log.d("sensor", "X Left axis: " + x);
//	            	  mShakeListener.onShake(); 
////	            	  Toast.makeText(mContext, "left shake detected", Toast.LENGTH_SHORT).show();
//	              }
//	        	  speed = 0f;
	           
	          }
	        }
	        mLastForce = now;
	      }
	      mLastTime = now;
	      mLastX = values[SensorManager.DATA_X];
	      mLastY = values[SensorManager.DATA_Y];
	      mLastZ = values[SensorManager.DATA_Z];
	    }
	  }
	  public static float Round(float Rval, int Rpl) {
	        float p = (float)Math.pow(10,Rpl);
	        Rval = Rval * p;
	        float tmp = Math.round(Rval);
	        return (float)tmp/p;
	        }

	}

