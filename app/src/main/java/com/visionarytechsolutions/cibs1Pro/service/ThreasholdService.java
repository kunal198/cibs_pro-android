package com.visionarytechsolutions.cibs1Pro.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.visionarytechsolutions.cibs1Pro.SoundMeter;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ThreasholdService extends Service{
	
	SharedPreferences sharedPreferences;
	AlertActions alertActions;
	
	    private double mThreshold=8.0;
	     
	    private PowerManager.WakeLock mWakeLock;

	    private Handler mHandler; 
	    
	    private static final int POLL_INTERVAL = 300;
	    private SoundMeter mSensor;
	    Runnable mSleepTask;
	    Runnable mPollTask;
	    Handler handler;
	@Override
	public IBinder onBind(Intent intent){
		Log.v("ShakeService", "onBind");
	    return null;
	}
	public void onCreate(){
	    super.onCreate();			     
	    Log.e("threashold---OnCreate","onSatart");
	}
	public void onDestroy(){	
		Log.v("ShakeService", "onDestroy");
	     super.onDestroy();
	}
	public void onStart(Intent intent, int startId){
		    Log.e("threashold---OnStart","onSatart");
		    alertActions=new AlertActions(getApplicationContext());
		    PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
		     mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");		     		     		    
		      mSensor = new SoundMeter();	      
		      mHandler = new Handler();
	            	   //start();	               
		      mSleepTask = new Runnable() {
		            public void run() {
		            	Log.e("Noise", "runnable mSleepTask");  
		          	  try
		          	  {
		   	            	   start();		                      
		          	  }
		          	  catch(Exception e)
		          	  {
		          		  e.printStackTrace();
		          	  }
		            }
		    }; 
              mHandler.postDelayed(mSleepTask, 1000);

		 /*   final Runnable r = new Runnable() {
                   public void run() {
               	    
               	    double amp = mSensor.getAmplitudeEMA();
	                    String detail=amp+"";
	                    int index=detail.indexOf(".");
	                                                            
	                    Log.e("amp","hh"+amp);
	                    Log.e("data",detail.substring(0, index));
	                    amp=Double.parseDouble(detail.substring(0, index));
	                     Log.e("amp11","hh"+amp);
	                    //Log.i("Noise", "runnable mPollTask");
	                     		                     
	                     
	                     
	                    if ((amp >= mThreshold)) {
	                          callForHelp();
	                          //Log.i("Noise", "==== onCreate ===");                         
	                    }                       
                   }
               };

               handler.postDelayed(r, POLL_INTERVAL);*/
		    
		    
		    // Create runnable thread to Monitor Voice
		      mPollTask = new Runnable() {
		            public void run() {           	
		          	  double amp = mSensor.getAmplitudeEMA();
		                    String detail=amp+"";
		                    int index=detail.indexOf(".");
		                                                            
		                    Log.e("amp","hh"+amp);
		                    Log.e("data",detail.substring(0, index));
		                    amp=Double.parseDouble(detail.substring(0, index));
		                     Log.e("amp11","hh"+amp);
		                    //Log.i("Noise", "runnable mPollTask");
		                     		                     		                     		                     
		                    if ((amp >= mThreshold)) {
		                          callForHelp();
		                          //Log.i("Noise", "==== onCreate ===");                         
		                    }                   
		                    // Runnable(mPollTask) will again execute after POLL_INTERVAL
		                    mHandler.postDelayed(mPollTask, POLL_INTERVAL);
		                 
		            }
		    };
		
	}	
	protected void onResume(){
		    Log.e("threashold---Resume","onSatart");
		/*mSensorManager.registerListener(mSensorListener,
		        mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
		        SensorManager.SENSOR_DELAY_UI);*/
	}
	protected void onPause(){
		    Log.e("threashold---Pause","onSatart");
	}
	
	 private void start() {
	 	    Log.e("Noise", "==== start ===");
			 try
			 {
			Log.e("sensorr11","sds"+mSensor);
	          mSensor.start();
	         Log.e("sensorr","sds");
	         if (!mWakeLock.isHeld()) {
	                 mWakeLock.acquire();
	         }
	         
	         //Noise monitoring start
	         // Runnable(mPollTask) will execute after POLL_INTERVAL
	         mHandler.postDelayed(mPollTask, POLL_INTERVAL);
			 }
			 catch(Exception e)
			 {
			e.printStackTrace();	 
			 }
	 }
	 
	 private void callForHelp() {
	        
	         //stop();
	   	 // Show alert when noise thersold crossed
			// if(switchStatus)
			//{	
		 
		/* final Thread t = new Thread() {
		        @Override
		        public void run() {
		            try {*/
		Boolean subscription=false;

		 try{
			 //DateFormat df1=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");//foramt date

			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
			 String str1=formatter.format(Calendar.getInstance().getTime());
			 Log.e("datee",str1);

			 //String str1 = "2017-06-06 11:35:36";
			 Date date1 = formatter.parse(str1);

			 String str2 = sharedPreferences.getString("subscriptionDateTime","");
			 Date date2 = formatter.parse(str2);

			 if(date1.before(date2))
			 {
				 Log.e("dateee1111","date2 is Greater than my date1");
				 subscription=true;
			 }
			 else
			 {
				 Log.e("dateee1111","date2 is less than my date1");
				 subscription=false;

			 }
		 }catch (ParseException e1){
			 e1.printStackTrace();
		 }
		 if(sharedPreferences.getBoolean("subscription",false)) {
			 Toast.makeText(getApplicationContext(), "CIBS Alert Activated", Toast.LENGTH_LONG).show();
			 alertActions.activateNotificationAlert(true);
		 }
		          /*	  
		            } finally {


		            }
		        }
		    };
		    t.start();*/
		 
		/*Runnable tt = new Runnable() {
	            public void run() {
	            	Log.e("Noise", "runnable mSleepTask");  
	          	  try
	          	  {
	          		  Toast.makeText(getApplicationContext(),"CIBS Alert Activated",Toast.LENGTH_LONG).show();
	  	       	     alertActions.activateNotificationAlert(true);		          	  }
	          	  catch(Exception e)
	          	  {
	          		  e.printStackTrace();
	          	  }
	            }
	    }; 
        mHandler.postDelayed(tt, 1000);*/
		 /*handler = new Handler(Looper.getMainLooper());
		 
		 handler.post(new Runnable() {

           @Override
           public void run() {
				//getApplicationContext().stopService(new Intent(getApplicationContext(), ThreasholdService.class));

				

           }
       });	 */
		 /*new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(),"CIBS Alert Activated",Toast.LENGTH_LONG).show();
 	       	     
				getApplicationContext().stopService(new Intent(getApplicationContext(), ThreasholdService.class));

				
				return null;
			}
		}.execute();
		*/
		

          /* handler.post(new Runnable() {

               @Override
               public void run() {
               	
               }
           });*/
		/* new Thread(new Runnable() {
	            @Override
	            public void run() {
	                

	            }
	   }).start();*/

	      // mHandler.removeCallbacks(mSleepTask);
	       //mHandler.removeCallbacks(mPollTask);
	       
	       
	       
	      // mSensor.stop();
	       //mRunning = false;          
	          
			// }
	   }
	 
	 public static Thread performOnBackgroundThread(final Runnable runnable) {
		    final Thread t = new Thread() {
		        @Override
		        public void run() {
		            try {
		                runnable.run();
		            } finally {

		            }
		        }
		    };
		    t.start();
		    return t;
		}
}
