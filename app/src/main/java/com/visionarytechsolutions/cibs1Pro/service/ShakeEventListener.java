package com.visionarytechsolutions.cibs1Pro.service;


import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;


/**
 * Listener that detects shake gesture.
 */
public class ShakeEventListener implements SensorEventListener {


  /** Minimum movement force to consider. */
  private static final int MIN_FORCE = 58;
  private static final float ERROR = (float) 7.0;

  /**
   * Minimum times in a shake gesture that the direction of movement needs to
   * change.
   */
  private static final int MIN_DIRECTION_CHANGE = 4;

  /** Maximum pause between movements. */
  private static final int MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE = 200;
  private float x1, x2, x3;

  /** Maximum allowed time for shake gesture. */
  private static final int MAX_TOTAL_DURATION_OF_SHAKE = 400;

  /** Time when the gesture started. */
  private long mFirstDirectionChangeTime = 0;

  /** Time when the last movement started. */
  private long mLastDirectionChangeTime;

  /** How many movements are considered so far. */
  private int mDirectionChangeCount = 0;

  /** The last x position. */
  private float lastX = 0;

  /** The last y position. */
  private float lastY = 0;

  /** The last z position. */
  private float lastZ = 0;

  /** OnShakeListener that is called when shake is detected. */
  private OnShakeListener mShakeListener;
  private boolean init;

  private Context context;
  int count=0;
  /**
   * Interface for shake gesture.
   */
  public interface OnShakeListener {

    /**
     * Called when shake gesture is detected.
     */
    void onShake();
  }

  public void setOnShakeListener(OnShakeListener listener) {
    mShakeListener = listener;
  }
  
  public ShakeEventListener(Context context) {
	this.context = context;
  }

  @Override
  public void onSensorChanged(SensorEvent se) {
    // get sensor data	  	 
	  	    float x,y,z;
	         x = se.values[0];
	         y = se.values[1];
	         z = se.values[2];
	        if (!init) {
	            x1 = x;
	            x2 = y;
	            x3 = z;
	            init = true;
	        } else {
	            float diffX = Math.abs(x1 - x);
	            float diffY = Math.abs(x2 - y);
	            float diffZ = Math.abs(x3 - z);
	            //Handling ACCELEROMETER Noise
	            if (diffX < ERROR) {
	                diffX = (float) 0.0;
	            }
	            if (diffY < ERROR) {
	                diffY = (float) 0.0;
	            }
	            if (diffZ < ERROR) {
	                diffZ = (float) 0.0;
	            }
	            x1 = x;
	            x2 = y;
	            x3 = z;
	            //Horizontal Shake Detected!
	            if (diffX > 20) {
	            //if (diffX > 20) {
	          	  SharedPreferences sharedPreferences = context.getSharedPreferences("CIBS", Context.MODE_PRIVATE);
	          	  
	          	  if(sharedPreferences.getBoolean("isShakeChecked", false))
	          	  {
	                //counter.setText("Shake Count : "+ count);
	                 count = count+1;	
	                 
            	       Toast.makeText(context, count+"", Toast.LENGTH_SHORT).show();

	                 if(count>=3)
	                 {		 	              
                          // MySharedPreferences.getInstance().storeData(context, "shake", "true");
                          // MySharedPreferences.getInstance().storeData(context, "threshold", "false");
	               	   
	               	     Toast.makeText(context, "CIBS Alert Activated", Toast.LENGTH_SHORT).show();
		 	               final Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
		 	     	     vibe.vibrate(150);
		 	     	     count=0;
		 	     	     AlertActions alertActions=new AlertActions(context);
		 	    	          alertActions.activateNotificationAlert(true);
	                 }	
	                 }                 
	            }}
    /*float x = se.values[0];
    float y = se.values[1];
    float z = se.values[2];
    

    // calculate movement
    float totalMovement = Math.abs(x + y + z - lastX - lastY - lastZ);
   // Toast.makeText(context, totalMovement+"  "+MIN_FORCE, Toast.LENGTH_SHORT).show();

    if (totalMovement > MIN_FORCE) {
    	final Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
	    vibe.vibrate(150);
	      
      // get time
      long now = System.currentTimeMillis();

      // store first movement time
      if (mFirstDirectionChangeTime == 0) {
        mFirstDirectionChangeTime = now;
        mLastDirectionChangeTime = now;
      }

      // check if the last movement was not long ago
      long lastChangeWasAgo = now - mLastDirectionChangeTime;
      if (lastChangeWasAgo < MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE) {

        // store movement data
        mLastDirectionChangeTime = now;
        mDirectionChangeCount++;

        // store last sensor data 
        lastX = x;
        lastY = y;
        lastZ = z;

        // check how many movements are so far
        if (mDirectionChangeCount >= MIN_DIRECTION_CHANGE) {

          // check total duration
          long totalDuration = now - mFirstDirectionChangeTime;

          if (totalDuration < MAX_TOTAL_DURATION_OF_SHAKE) {
            mShakeListener.onShake();
            resetShakeParameters();
          }
        }

      } else {
        resetShakeParameters();
      }
    }*/
  }

  /**
   * Resets the shake parameters to their default values.
   */
  private void resetShakeParameters() {
    mFirstDirectionChangeTime = 0;
    mDirectionChangeCount = 0;
    mLastDirectionChangeTime = 0;
    lastX = 0;
    lastY = 0;
    lastZ = 0;
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

}
