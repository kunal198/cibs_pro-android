package com.visionarytechsolutions.cibs1Pro;

import org.json.JSONException;
import org.json.JSONObject;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

public class QuickSetupFragment extends Fragment {	
	Button btnNext;
	View rootView;
	CheckBox chkQuickMeeting,chkTrip,chkOnceDay;
	SharedPreferences sharedPreferences;
	Editor editor;
	AlertActions alertActions;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) { 		
          rootView = inflater.inflate(R.layout.activity_quick_setup_options, container, false);
		btnNext=(Button)rootView.findViewById(R.id.btnNext);
          alertActions = new AlertActions(getActivity().getApplicationContext()); 
		//btnNext.setVisibility(View.GONE);
		btnNext.setText("Save");
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();				
		chkQuickMeeting=(CheckBox)rootView.findViewById(R.id.chkQuickMeeting);
		chkTrip=(CheckBox)rootView.findViewById(R.id.chkTrip);
		chkOnceDay=(CheckBox)rootView.findViewById(R.id.chkOnce);	
		btnNext.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
			     Toast.makeText(getActivity(), "Quick Setting is saved", Toast.LENGTH_SHORT).show();
			     if(chkOnceDay.isChecked()||chkTrip.isChecked()||chkQuickMeeting.isChecked())
				{
	          	JSONObject timerJSON = new JSONObject();
	          	JSONObject radiusJSON = new JSONObject();         	         	         	
				try {				
					if(chkOnceDay.isChecked())
					{
						timerJSON.put("value", 24*60*60);
						//timerJSON.put("value", 10);
						radiusJSON.put("position",2);
						radiusJSON.put("value", 1000);	
					}
					else if(chkTrip.isChecked())
					{
					timerJSON.put("value", 60*60);
					//timerJSON.put("value", 10);
					radiusJSON.put("position",2);
					radiusJSON.put("value", 1000);
					}
					
					else if(chkQuickMeeting.isChecked())
					{
						timerJSON.put("value", 30*60);
						//timerJSON.put("value", 10);
						radiusJSON.put("position",2);
						radiusJSON.put("value", 1000);	
					}
					editor.putString("checkinTimer", timerJSON.toString());
					editor.putString("safeLocationRadius", radiusJSON.toString());
					editor.putBoolean("recordTypeVideo", false);
					editor.putBoolean("recordTypeAudio", true);
					editor.putBoolean("isTextChecked", true);
					editor.putBoolean("isCallChecked", true);
					editor.putBoolean("isVibrateChecked", true);
					editor.putBoolean("isRingtoneChecked", true);
					editor.putBoolean("isEmailChecked", true);					
					editor.putBoolean("isShakeChecked", true);			

					editor.commit();				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}        	
	          	boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
				boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
				if((!notificationAlertSet && !alertSet) || !alertSet)
					alertActions.activateFakeCallAlert(false);										
	          }					    	 											
			if(chkQuickMeeting.isChecked())
			{
	    		   editor.putBoolean("QuickMeeting", true).apply();	    		   	    		   
			}
			else
			{
	    		   editor.putBoolean("QuickMeeting", false).apply();	
			}
			if(chkTrip.isChecked())
			{
	    		   editor.putBoolean("Trip", true).apply();	
			}
			else
			{
	    		   editor.putBoolean("Trip", false).apply();	
			}
			if(chkOnceDay.isChecked())
			{
	    		   editor.putBoolean("OnceDay", true).apply();	
			}
			else
			{
	    		   editor.putBoolean("OnceDay", false).apply();	
			}
			}
		});
		if(sharedPreferences.getBoolean("QuickMeeting", false))
		{
			chkQuickMeeting.setChecked(true);	
		}
		if(sharedPreferences.getBoolean("Trip", false))
		{
			chkTrip.setChecked(true);	
			
		}
		if(sharedPreferences.getBoolean("OnceDay", false))
		{
			chkOnceDay.setChecked(true);	
		}						
		chkQuickMeeting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkTrip.setChecked(false);
		    		   chkOnceDay.setChecked(false);
		    		  /* editor.putBoolean("QuickMeeting", true).apply();
		    		   editor.putBoolean("Trip", false).apply();
		    		   editor.putBoolean("OnceDay", false).apply();		*/    		   
		    	   }
		       }
		   }
		);     
		chkTrip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkQuickMeeting.setChecked(false);
		    		   chkOnceDay.setChecked(false);		    		   
		    		  /* editor.putBoolean("QuickMeeting", false).apply();
		    		   editor.putBoolean("Trip", true).apply();
		    		   editor.putBoolean("OnceDay", false).apply();*/
		    	   }
		       }
		   }
		);     		
		chkOnceDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   if(isChecked)
		    	   {
		    		   chkTrip.setChecked(false);
		    		   chkQuickMeeting.setChecked(false);		    		   
		    		   /*editor.putBoolean("QuickMeeting", false).apply();
		    		   editor.putBoolean("Trip", false).apply();
		    		   editor.putBoolean("OnceDay", true).apply();*/
		    	   }

		       }
		   }
		);     						
        return rootView;
    }	
}
     