package com.visionarytechsolutions.cibs1Pro;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

/*import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;*/
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ServerUtilities;
import com.visionarytechsolutions.cibs1Pro.receiver.FakeCallReceiver;
import com.visionarytechsolutions.cibs1Pro.service.MouthService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;
import com.visionarytechsolutions.cibs1Pro.utils.MySharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CheckInRequestFragment extends Fragment implements
		LocationListener,BillingProcessor.IBillingHandler {

	LocationManager locationManager;
	Location currentLocation;
	SharedPreferences sharedPreferences;
	String response = null;
	View rootView;
	Button btnCheckin, btnDeactivate, btnSendAlert, btnWhereIam;
	RelativeLayout mainLayout;
	// NotificationAlertActions notificationAlertActions;
	AlertActions alertActions;
	public static String packageUpdate="";
	public static Boolean booelanPackageUpdate=false;
	SharedPreferences.Editor editor;

	String productPackage="";
	Button btnShare;
	String message = "";
	BillingProcessor bp;
	CharSequence options[] = new CharSequence[]{"Default Message",
			"Custom Message"};

	public CheckInRequestFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		rootView = inflater
				.inflate(R.layout.fragment_checkin, container, false);
		//bp = new BillingProcessor(getActivity(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsK3jTEnMiZ1+Ocu9t8fG8Qk6uuzQVOm3J2cDUykdk/ysW5CNxLR2nwh75zwZEgC5MPVVOV0qcnaw8xgL9TM5463kYa//fUGoe6KxjsIhHSuVT8UeSNZe8NKqHEvLV/BQat3BIzAhoeN+F3WNqJN5FLKzvW5vi9pQGr2eQjbBbcbOVI0QtGRwVHEU4v/Vwe9C/xl+bbdh0XZXtViwvwFmaUS6AKwjG1GhvWN5L9vwZeTCrARyyzZCm0VaQFvo9MVAm/nAsZnP7xx1xim9TleEVq4we8IF2C8m0SflOl7O9NM1gBIDBFJWvtRjomrzHLYM0Q3FkP7m2p9eHwMs6o2RawIDAQAB", this);
		bp = BillingProcessor.newBillingProcessor(getActivity(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsK3jTEnMiZ1+Ocu9t8fG8Qk6uuzQVOm3J2cDUykdk/ysW5CNxLR2nwh75zwZEgC5MPVVOV0qcnaw8xgL9TM5463kYa//fUGoe6KxjsIhHSuVT8UeSNZe8NKqHEvLV/BQat3BIzAhoeN+F3WNqJN5FLKzvW5vi9pQGr2eQjbBbcbOVI0QtGRwVHEU4v/Vwe9C/xl+bbdh0XZXtViwvwFmaUS6AKwjG1GhvWN5L9vwZeTCrARyyzZCm0VaQFvo9MVAm/nAsZnP7xx1xim9TleEVq4we8IF2C8m0SflOl7O9NM1gBIDBFJWvtRjomrzHLYM0Q3FkP7m2p9eHwMs6o2RawIDAQAB", this); // doesn't bind
		bp.initialize();

        DateFormat df1=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");//foramt date
        //
        //Log.e("datee",date);

	//	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//05-25 10:46:50.312 3013-3013/com.visionarytechsolutions.cibsPro E/dateee1111: date2 is Greater than my date1     2017-05-25 18:05:50

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
			String str1=df1.format(Calendar.getInstance().getTime());
			Log.e("datee",str1);
			str1="2017-05-25 18:05:48";

			//String str1 = "2017-06-06 11:35:36";
			Date date1 = formatter.parse(str1);


			String str2 = "2017-03-01 15:37:28";
			Date date2 = formatter.parse(str2);

		/*	if (date1.compareTo(date2)<0)
			{
				Log.e("dateee","date2 is Greater than my date1");
			}*/
			if(date1.before(date2))
			{
				Log.e("subs","subs revenn");
			}
			else
			{
				Log.e("subs","subs expire");

			}
		}catch (ParseException e1){
			e1.printStackTrace();
		}

		//bp.subscribe(getActivity(), "YOUR SUBSCRIPTION ID FROM GOOGLE PLAY CONSOLE HERE");
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		editor=sharedPreferences.edit();
		try {

			if (!sharedPreferences.getString("userId", "").isEmpty()) {

				AsyncTask<Void, Void, Void> checkExpire = new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						// Register on our server
						// On server creates a new user
						response = ServerUtilities.CheckExpiry(getActivity(), sharedPreferences.getString("userId", ""));
						//response = ServerUtilities.CheckExpiry(getActivity(), "1");
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						try {
							JSONObject obj = new JSONObject(response);
							editor.putString("subscriptionDateTime", obj.getString("expiry_date")).apply();
							Log.e("expireeeee", obj.getString("expiry_date"));

							if (obj.getString("success").equals("0")) {
								editor.putBoolean("subscription", false).apply();
								showInAppPurchaseDialog();
							}
							else
							{
								editor.putBoolean("subscription", true).apply();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
				checkExpire.execute(null, null, null);
			}
			}
		catch (Exception e) {
			e.printStackTrace();
		}
		/*Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }
        Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), alarmUri);
        ringtone.play();*/
        
       
        /*try {
		JSONObject obj=new JSONObject("{'phone':7878787878,'email':,'status':Rejected}");
	} catch (JSONException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
		btnCheckin = (Button) rootView.findViewById(R.id.buttonCheckin);
		btnDeactivate = (Button) rootView.findViewById(R.id.buttonDeactivate);
		btnSendAlert = (Button) rootView.findViewById(R.id.buttonSendAlert);
		btnWhereIam = (Button) rootView.findViewById(R.id.buttonWhereIam);
		// Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" +
		// "+919041671859"));
		// intent.putExtra("sms_body", "message");
		// startActivity(intent);
		mainLayout = (RelativeLayout) rootView
				.findViewById(R.id.checkin_mainLayout);

		alertActions = new AlertActions(getActivity().getApplicationContext());
		locationManager = (LocationManager) getActivity().getSystemService(
				Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, this);
		currentLocation = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (currentLocation == null) {
			try {
				locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, this);
				currentLocation = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			} catch (Exception e) {
				Log.d("Exception", e.toString());
			}
		}
		btnCheckin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent alertActivity = new Intent(getActivity()
						.getApplicationContext(), AlertDialogActivity.class);
				alertActivity.putExtra("callFrom", "checkinBtn");
				startActivity(alertActivity);
				if (FakeCallReceiver.instance != null) {
					FakeCallReceiver.instance.interacted = true;
				}
			}
		});
		btnDeactivate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*SharedPreferences sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
		        Editor editor = sharedPreferences.edit();
		        editor.putBoolean("alertClick", false).apply();*/
				Intent alertActivity = new Intent(getActivity()
						.getApplicationContext(), AlertDialogActivity.class);
				alertActivity.putExtra("callFrom", "deactivateBtn");
				startActivity(alertActivity);
				if (FakeCallReceiver.instance != null) {
					FakeCallReceiver.instance.interacted = true;
				}
			}
		});
		btnSendAlert.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				MySharedPreferences.getInstance().storeData(getActivity(), "shake", "false");
				MySharedPreferences.getInstance().storeData(getActivity(), "threshold", "false");
				if (MouthService.isServiceWorking) {
					Intent svc = new Intent(getActivity(), MouthService.class);
					getActivity().stopService(svc);
				}
				//Editor editor = sharedPreferences.edit();
				alertActions.activateNotificationAlert(true);
				Toast.makeText(getActivity().getApplicationContext(),
						"CIBS alert activated ", Toast.LENGTH_LONG).show();
				getActivity().finish();
				
		        /* if(!sharedPreferences.getBoolean("alertClick", false))
		        {
				alertActions.activateNotificationAlert(true);
				// Toast.makeText(getActivity().getApplicationContext(),
				// "CIBS alert has been sent", Toast.LENGTH_LONG).show();
				if (MainActivity.instance != null) {
					//MainActivity.instance.finish();
					Toast.makeText(getActivity().getApplicationContext(),
							 "CIBS alert activated ", Toast.LENGTH_LONG).show();	
				}
				if (FakeCallReceiver.instance != null) {
					FakeCallReceiver.instance.interacted = true;
				}				
		          editor.putBoolean("alertClick", true).apply();
		          }		        
		        else
		        {
		         Toast.makeText(getActivity().getApplicationContext(),
					 "CIBS alert already activated", Toast.LENGTH_LONG).show();	
		        }*/
			}
		});

		btnWhereIam.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentLocation != null) {
					Intent i = new Intent(getActivity(), WhereIam.class);
					getActivity().startActivity(i);
				} else {
					Toast.makeText(
							getActivity(),
							"Sorry, your location is not found, please turn on Location(GPS) services!",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		return rootView;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		currentLocation = location;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	void showInAppPurchaseDialog() {
		try
		{
		new AlertDialog.Builder(getActivity())
				.setTitle("In App Purchase Alert !")
				.setMessage("Please choose payment").setCancelable(false)

				.setPositiveButton("Pay 1 year($30)", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						Log.e("no", "yess");
						bp.subscribe(getActivity(), "one_year");
						productPackage="1";
						packageUpdate="1";
						booelanPackageUpdate=true;

						//updatePackage("1");
						//ServerUtilities.updatePackage(getActivity(), sharedPreferences.getString("userId", ""), "1");
					}
				})
				.setNegativeButton("Pay 1 month($2.99)", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						Log.e("yess", "yess");
						//bp.subscribe(getActivity(), "one_month");
						bp.subscribe(getActivity(), "one_month");
						packageUpdate="0";
						booelanPackageUpdate=true;

						productPackage="0";
						//updatePackage("0");




					}
				})
				.show();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}}

	/*void updatePackage(final String packageUpdate) {
		try {

			AsyncTask<Void, Void, Void> updatePackage = new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					// Register on our server
					// On server creates a new user
					response = ServerUtilities.updatePackage(getActivity(), sharedPreferences.getString("userId", ""), packageUpdate);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
					try {
						JSONObject obj = new JSONObject(response);
						if (obj.getString("success").equals("0")) {
							showInAppPurchaseDialog();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			};
			updatePackage.execute(null, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	@Override
	public void onProductPurchased(String productId, TransactionDetails details) {
		Toast.makeText(getActivity(),"purchase "+productId,Toast.LENGTH_SHORT).show();


	}
	@Override
	public void onPurchaseHistoryRestored() {

	}
	@Override
	public void onBillingError(int errorCode, Throwable error) {
		Toast.makeText(getActivity(),"onBillingError",Toast.LENGTH_SHORT).show();
	}
	@Override
	public void onBillingInitialized() {

	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!bp.handleActivityResult(requestCode, resultCode, data)){
			super.onActivityResult(requestCode, resultCode, data);
			Toast.makeText(getActivity(),"purchase result ",Toast.LENGTH_SHORT).show();

			//if(productId.equals("one_month"))
			//{
				//updatePackage(productPackage);
			//}
		//	else if(productId.equals("one_year"))
			//{
				//updatePackage("1");
			//}
		}else {

		}


	}

	@Override
	public void onDestroy() {
		if (bp != null)
			bp.release();

		super.onDestroy();
	}
}
