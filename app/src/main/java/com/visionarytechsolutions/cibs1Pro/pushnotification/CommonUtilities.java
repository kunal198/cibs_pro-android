package com.visionarytechsolutions.cibs1Pro.pushnotification;

import android.content.Context;
import android.content.Intent;
 
public final class CommonUtilities {
     
    // give your server registration url here
    public static final String SERVER_URL = "http://www.checkinbesafe.com/webservices/cibs_register.php"; 
    
    public static final String SEND_EMAIL_URL = "http://www.checkinbesafe.com/webservices/send_mail_android.php";

    public static final String CHECK_EXPIRY_URL = "http://www.checkinbesafe.com/webservices/check_expiry.php";


    public static final String UPDATE_PACKAGE_URL = "http://www.checkinbesafe.com/webservices/update_expiry.php";


    // Google project id
   // public static final String SENDER_ID = "942868400148";
     //public static final String SENDER_ID = "942868400148";
    public static final String SENDER_ID = "132070808055";


    /**
     * Tag used on log messages.
     */
    public static final String TAG = "CIBS GCM";
 
    public static final String DISPLAY_MESSAGE_ACTION =
            "com.visionarytechsolutions.cibs1Pro.DISPLAY_MESSAGE";
 
    public static final String EXTRA_MESSAGE = "message";
 
    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
