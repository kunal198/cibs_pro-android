package com.visionarytechsolutions.cibs1Pro;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.visionarytechsolutions.cibs1Pro.GCM.GCMRegistrar;
import com.visionarytechsolutions.cibs1Pro.fileUpload.NetworkOperations;
import com.visionarytechsolutions.cibs1Pro.pushnotification.AlertDialogManager;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ConnectionDetector;
import com.visionarytechsolutions.cibs1Pro.pushnotification.ServerUtilities;
import com.visionarytechsolutions.cibs1Pro.pushnotification.WakeLocker;
import com.visionarytechsolutions.cibs1Pro.utils.GlobalClass;
import com.visionarytechsolutions.cibs1Pro.utils.IabHelper;
import com.visionarytechsolutions.cibs1Pro.utils.IabResult;
import com.visionarytechsolutions.cibs1Pro.utils.Purchase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.EXTRA_MESSAGE;
import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.SENDER_ID;

public class SignUpFragment extends Fragment {
		
	EditText etFirstName,etLastName,etPassword,etEmail,etPhone;
	String firstName,lastName,password,email,phone,regId;
	SharedPreferences sharedPreferences;
	Editor editor;
	public static SignUpFragment instance = null;
	RelativeLayout mainLayout;
	Intent intent;
	Button btnContinue;
	View rootView;
	boolean fromFirstActivity = false;
	AlertDialogManager alert = new AlertDialogManager();
     ConnectionDetector cd;
     AsyncTask<Void, Void, Void> mRegisterTask;
	IInAppBillingService mService;
	IabHelper mHelper;

	public SignUpFragment(boolean fromFirstActivity){
		this.fromFirstActivity = fromFirstActivity;
	}
	
	public SignUpFragment(){
		this.fromFirstActivity = false;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();
		instance = this;
		Intent serviceIntent =
				new Intent("com.android.vending.billing.InAppBillingService.BIND");
		serviceIntent.setPackage("com.android.vending");
		getActivity().bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

		/*mHelper = new IabHelper(getActivity(), "base64EncodedPublicKey");

		mHelper.launchPurchaseFlow(getActivity(), "888", 10001,
				mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");*/

		/*try {
			Bundle skuDetails = mService.getSkuDetails(3,
                    getActivity().getPackageName(), "inapp", querySkus);
		} catch (RemoteException e) {
			e.printStackTrace();
		}*/
		/* int resID=getResources().getIdentifier("ringdefault", "raw", getActivity().getPackageName());
	        MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(),resID);
	        mediaPlayer.start();	*/
	/*	int resID=getResources().getIdentifier("ringdefault", "raw", getActivity().getPackageName());
	        MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(),resID);
	        mediaPlayer.start();	*/
				    
        rootView = inflater.inflate(R.layout.fragment_signup, container, false);       
        cd = new ConnectionDetector(getActivity().getApplicationContext());                
        etFirstName = (EditText) rootView.findViewById(R.id.etFirstName);
        etLastName = (EditText) rootView.findViewById(R.id.etLastName);
        etPassword = (EditText) rootView.findViewById(R.id.etPassword);
        etPhone = (EditText) rootView.findViewById(R.id.etPhone);
        etEmail = (EditText) rootView.findViewById(R.id.etEmail);
        btnContinue = (Button) rootView.findViewById(R.id.btnContinue);               
        if(sharedPreferences.getBoolean("isDataEntered", false))
        	btnContinue.setText("Save");       
        mainLayout = (RelativeLayout) rootView.findViewById(R.id.signup_mainLayout);       
        setValues();
        
        btnContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				String str = sdf.format(new Date());
				Log.e("timeeeee",str);
				
				
				/*if(sharedPreferences.getString("previoustime","").equals(""))
				{
				editor.putString("previoustime", str).apply();
				}
				else
				{
				String prvTime=sharedPreferences.getString("previoustime", "");
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
				try
				{
				Date date1 = simpleDateFormat.parse(prvTime);
				Date date2 = simpleDateFormat.parse(str);

				long difference = date2.getTime() - date1.getTime(); 
				int days = (int) (difference / (1000*60*60*24));  
				int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60)); 
				int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
				hours = (hours < 0 ? -hours : hours);
				Log.e("======= min"," :: "+min);
				if(min>=2)
				{
				Toast.makeText(getActivity(), "working", Toast.LENGTH_SHORT).show();
				editor.putString("previoustime","").apply();
				}
				else
				{
				Toast.makeText(getActivity(), "less than two minutee", Toast.LENGTH_SHORT).show();
				}
				}
				catch(Exception e)
				{
					
				}
				
				}*/
				
				
				
				//if(sharedPreferences.getString("previoustime", ""))
				
				
				 // Check if Internet present
		        if (!cd.isConnectingToInternet()) {
		            // Internet Connection is not present
		            alert.showAlertDialog(getActivity(),
		                    "Internet Connection Error",
		                    "Please connect to working Internet connection", false);
		            // stop executing code by return
		        }else
		        	btContinueClicked();
			}
		});
        
        return rootView;
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	
	public void btContinueClicked(){    
		if(validFields()){
			if(networkAvailable()){			
				 if(fromFirstActivity){
			        Log.e("true1","true1");
		    		saveValues();

					 final Context context = getActivity().getApplicationContext();
					 mRegisterTask = new AsyncTask<Void, Void, Void>() {

						 @Override
						 protected Void doInBackground(Void... params) {
							 // Register on our server
							 // On server creates a new user
							// ServerUtilities.register(context, firstName+" "+lastName, phone, email, regId);

							 ServerUtilities.register(context, firstName+" "+lastName, phone, email, sharedPreferences.getString("firebaseId",""));
							 return null;
						 }
						 @Override
						 protected void onPostExecute(Void result) {
							 mRegisterTask = null;
							 Log.e("idddsfcmm",sharedPreferences.getString("firebaseId",""));
							 FirstActivity.instance.setScreen();
							 // Toast.makeText(getActivity().getApplicationContext(), regId, Toast.LENGTH_LONG).show();
						 }
					 };
					 mRegisterTask.execute(null, null, null);


		    		//registerWithGcm();
		    		//FirstActivity.instance.setScreen();
		    	}else{
		    		Intent confirmActivity = new Intent(getActivity().getApplicationContext(),AlertDialogConfirm.class);
		    		confirmActivity.putExtra("fromMain", false);
					startActivityForResult(confirmActivity, 1);	
		    	}
			}else{
				Toast.makeText(getActivity(), "Please connect to Internet", Toast.LENGTH_LONG).show();
			}
		}}


	public boolean validFields(){
		firstName = etFirstName.getText().toString();
    	lastName = etLastName.getText().toString();
    	password = etPassword.getText().toString();
    	phone = etPhone.getText().toString();
    	email = etEmail.getText().toString(); 
    	
		boolean validMobile = false, validPwd = false, validFirstName = false, validLastName = false, validEmail = false;

		if (!(firstName.length() > 0)) {
			etFirstName.setError("Should't Be empty");
		} else
			validFirstName = true;

		if (!(lastName.length() > 0)) {
			etLastName.setError("Should't Be empty");
		} else
			validLastName = true;

		if (!(password.length() > 0)) {
			etPassword.setError("Should't Be empty");
		} 
		else if(password.length()!=4)
		{
			etPassword.setError("Your password should be in 4 digits");	
		}
		else
			validPwd = true;

		String mobilePattern = "[0-9]{10}";
		if (!phone.matches(mobilePattern)) {
			etPhone.setError("Invalid Phone No");
		} else
			validMobile = true;

		//if (email.length() > 0) {
		if(email.length()>0)
		{
			String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(email);
			if (!matcher.matches()) {
				etEmail.setError("Invalid Email");
				validEmail=false;
			} else
				validEmail = true;

		}
		else
		{
			validEmail = true;
		}
		//} else {
			//etEmail.setError("Should't Be empty");
		//}

		if (validFirstName && validMobile && validPwd && validLastName&&validEmail) 
			return true;
		else
			return false;
	}
	
	private boolean networkAvailable(){		
		boolean wifiNetworkAvailable = false;
		boolean mobileNetworkAvailable = false;
		ConnectivityManager conManager = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
		for(NetworkInfo netInfo : networkInfo){
			if(netInfo.getTypeName().equalsIgnoreCase("WIFI"))
				if(netInfo.isConnected())
					wifiNetworkAvailable = true;
			if(netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
				if(netInfo.isConnected())
					mobileNetworkAvailable = true;
		}
		return wifiNetworkAvailable||mobileNetworkAvailable;		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK){
			saveValues();
			updateValues();
		}else if(resultCode == Activity.RESULT_CANCELED){
			Toast.makeText(getActivity().getApplicationContext(), "Your change(s) has been discarded", Toast.LENGTH_LONG).show();
			setValues();
		}
	}
	
	public void registerWithGcm(){
		
		GCMRegistrar.checkDevice(getActivity());
        
        GCMRegistrar.checkManifest(getActivity());
        
        getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
        
        regId = GCMRegistrar.getRegistrationId(getActivity());

	 // Check if regid already presents
	    if (regId.equals("")) {
	        // Registration is not present, register now with GCM           
	        GCMRegistrar.register(getActivity(), SENDER_ID);

	    } else {
	        // Device is already registered on GCM
	        if (GCMRegistrar.isRegisteredOnServer(getActivity())) {
	            // Skips registration.              
	            Toast.makeText(getActivity().getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
	        } else {
	            // Try to register again, but not in the UI thread.
	            // It's also necessary to cancel the thread onDestroy(),
	            // hence the use of AsyncTask instead of a raw thread.
	                final Context context = getActivity().getApplicationContext();
	                mRegisterTask = new AsyncTask<Void, Void, Void>() {
	                	
	                    @Override
	                    protected Void doInBackground(Void... params) {
	                        // Register on our server
	                    // On server creates a new user

							ServerUtilities.register(context, firstName+" "+lastName, phone, email, regId);
	                        return null;
	                    }
	 
	                    @Override
	                    protected void onPostExecute(Void result) {
	                        mRegisterTask = null;
							Toast.makeText(getActivity().getApplicationContext(), regId, Toast.LENGTH_LONG).show();
						}
	                };
	                mRegisterTask.execute(null, null, null);
	            }
	        }
	}
		
	public void saveValues(){		
    	
    	if(sharedPreferences.getBoolean("isDataEntered", false))
    		Toast.makeText(getActivity().getApplicationContext(), "Saved Successfully", Toast.LENGTH_LONG).show();
    	
    	editor.putString("firstName", firstName);
    	editor.putString("lastName", lastName);
    	editor.putString("password", password);
    	editor.putString("phone", phone);
    	editor.putString("email", email);
    	editor.putBoolean("isDataEntered", true);

		editor.putString(GlobalClass.KEY_LOGGED_IN_USER_EMAIL, email);

		editor.commit();
	}	
	public void updateValues(){
//		http://www.checkinbesafe.com/webservices/cibs_register.php
		Log.e("updateee","updatee");
		List<Pair<String, String>> parameters = new ArrayList<Pair<String, String>>();
		parameters.add(new Pair<String, String>("user_id", sharedPreferences.getString("userId","")));

		parameters.add(new Pair<String, String>("name", firstName +" "+lastName));
		parameters.add(new Pair<String, String>("phone", phone));
		parameters.add(new Pair<String, String>("email", email));
		parameters.add(new Pair<String, String>("device_type", "android"));
		String url = "http://www.checkinbesafe.com/webservices/cibs_update_user.php";
		NetworkOperations nop = new NetworkOperations(url, parameters, getActivity(), "update");
		nop.execute();
	}	
	public void setValues(){
		if(sharedPreferences.getBoolean("isDataEntered", false)){
        	etFirstName.setText(sharedPreferences.getString("firstName", ""));
        	etLastName.setText(sharedPreferences.getString("lastName", ""));
        	etPassword.setText(sharedPreferences.getString("password", ""));
        	etPhone.setText(sharedPreferences.getString("phone", ""));
        	etEmail.setText(sharedPreferences.getString("email", ""));
        }
	}
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            WakeLocker.acquire(context);                       
            Toast.makeText(context, ""+newMessage, Toast.LENGTH_LONG).show();             
            // Releasing wake lock
            WakeLocker.release();
        }
    };
	@Override
	public void onResume() {
		super.onResume();
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	public void getUsername() {
		AccountManager manager = AccountManager.get(getActivity());
		Account[] accounts = manager.getAccountsByType("com.google");
		List<String> username = new LinkedList<String>();

		for (Account account : accounts) {
		    username.add(account.name);
		}
		Log.e("detailss",username.get(0));
}
	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name,
									   IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
		}
	};
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mService != null) {
			getActivity().unbindService(mServiceConn);
		}
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
			= new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase)
		{
			if (result.isFailure()) {
				Log.d("TAG", "Error purchasing: " + result);
				return;
			}
			else if (purchase.getSku().equals("SKU_GAS")) {
				// consume the gas and update the UI
			}
			else if (purchase.getSku().equals("SKU_PREMIUM")) {
				// give user access to premium content and update the UI
			}
		}
	};

}
