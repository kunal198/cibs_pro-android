package com.visionarytechsolutions.cibs1Pro;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;

public class FirstActivity extends Activity {
	
	SharedPreferences sharedPreferences;
	Editor editor;
	AlertActions alertActions;	
	ActionBarDrawerToggle mDrawerToggle;
	DrawerLayout mDrawer;
	ActionBar mActionBar;
	Intent intent;
	public static FirstActivity instance = null;
	int position = 0,startPosition = -1;	
	boolean allCompleted = false;
	Fragment fragment = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);		
			Log.e("frstt","frstt"+getPhoneName());
		}catch(Exception e){
			super.onCreate(null);
			e.printStackTrace();
		}		
		sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        alertActions = new AlertActions(getApplicationContext());	
        instance = this;
		setContentView(R.layout.activity_first);		
		getActionBar().hide();
		if(sharedPreferences.getBoolean("QuickMeeting", false))
		{
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}			
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();	
			}
		}
		else if(sharedPreferences.getBoolean("Trip", false))
		{
		
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}		
			else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false)){
	        	position = 1;
                setFragment();
			}
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();		
			}						
		}
		
		else if(sharedPreferences.getBoolean("OnceDay", false))
		{
		
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}		
			else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false)){
	        	position = 1;
                setFragment();
			}
			else if(!sharedPreferences.getBoolean("isSnapShotTaken", false))
			{
				position=3;
				setFragment();
			}
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();		
			}}		
		else
		{
		setScreen();
		}
	}
	
	public void setScreen(){
		if(sharedPreferences.getBoolean("QuickMeeting", false))
		{
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}			
			else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false))
			{
				position=1;
				setFragment();
			}
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();	
			}
		}
		else if(sharedPreferences.getBoolean("Trip", false))
		{
		
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}		
			else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false)){
	        	position = 1;
                setFragment();
			}
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();		
			}						
		}
		else if(sharedPreferences.getBoolean("OnceDay", false))
		{
		
			if(!sharedPreferences.getBoolean("isDataEntered", false))
			{
				position=0;
				setFragment();	
			}		
			else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false)){
	        	position = 1;
                setFragment();
			}
			else if(!sharedPreferences.getBoolean("isSnapShotTaken", false))
			{
				position=3;
				setFragment();
			}
			else if(!sharedPreferences.getBoolean("isContactsListCompleted", false))
			{
				position=2;
				setFragment();
			}
			else
			{
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();		
			}						
		}
		else
		{		
		if(!sharedPreferences.getBoolean("isDataEntered", false)){
			position = 0;
        }
        
        else if(!sharedPreferences.getBoolean("isSafeLocationCompleted", false)){
        	position = 1;
        }
        
        else if(!sharedPreferences.getBoolean("isContactsListCompleted", false)){
        	position = 2;
        }
        
        else if(!sharedPreferences.getBoolean("isSnapShotTaken", false)){
        	position = 3;
        }
        else if(!sharedPreferences.getBoolean("isSettingsCompleted", false)){
        	position = 4;
        }else{
        	allCompleted = true;
        }
		
		if(allCompleted){
			startActivity(new Intent(getApplicationContext(), MainActivity.class));
			finish();
		}else{		
			setFragment();
		}
		
		
		}
	}
	
	public void setFragment(){
		
		if(startPosition == -1)
			startPosition = position;
		
		switch (position) {
		
		case 0:	fragment = new SignUpFragment(true);
				break;
				
		case 1:	fragment = new SafeLocationsFragment(true);
				break;
				
		case 2:	fragment = new ContactsListFragment(true);
				break;
				
		case 3:	fragment = new MySelfieFragment(true);
				break;
				
		case 4:	fragment = new PresetSettingsFragment(true);
				break;

		default:
			break;
		}
			
	   FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.activity_first_fragmentcontainer, fragment);
        fragmentTransaction.commit();
		
	}
	
	@Override
	public void onBackPressed() {
		
		if(startPosition == position)
			super.onBackPressed();
		else{
			position -=1;
			setFragment();
		}
	}
	
	 public String getPhoneName() {  
	        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
	        String deviceName = myDevice.getName();     
	        return deviceName;
	    }
}
