package com.visionarytechsolutions.cibs1Pro;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.visionarytechsolutions.cibs1Pro.service.RollcallRequestService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import static com.visionarytechsolutions.cibs1Pro.pushnotification.CommonUtilities.displayMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData().toString());

        sharedPreferences = getApplicationContext().getSharedPreferences("CIBS", Context.MODE_PRIVATE);

        //String message = intent.getExtras().getString("message");
        String message = remoteMessage.getData().toString();

        Log.e(TAG, "Received message" + message);
        displayMessage(getApplicationContext(), message);
        sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        JSONObject obj = null;
        try {
            obj=new JSONObject(message);
            message=obj.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // notifies user
        if (message.equalsIgnoreCase("RollCallRequest")) {
            boolean rollCallRequestAccepted = sharedPreferences.getBoolean("rollCallRequestAccepted", false);
            boolean rollcallActivated = sharedPreferences.getBoolean("rollcallActivated", false);
            if (!rollCallRequestAccepted && !rollcallActivated) {

                //JSONObject obj=new
                String data=null;
                try {
                    data = obj.getString("rollcallData");

                }
                catch (Exception e)
                {

                }
                //String data = "";

                //Log.e(TAG, "datadeatilss" + data);


                editor.putBoolean("rollCallRequestReceived", true);
                editor.putString("rollCallData", data);
                editor.commit();
                Intent rollcallService = new Intent(getApplicationContext(), RollcallRequestService.class);
                rollcallService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startService(rollcallService);

            } else {
                if (MainActivity.instance != null) {
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        public void run() {
                            MainActivity.instance.changeRollcallStatus("InOtherRollCall");
                        }
                    });
                }
            }
        } else if (message.equalsIgnoreCase("ChangeStatus")) {
            Log.e("in status", "status");
            //String data = intent.getExtras().getString("rollcallData");


            //  String data = "";

            String data=null;
            try {
                data = obj.getString("rollcallData");

            }
            catch (Exception e)
            {

            }

            Log.e("roll call data", data);
            String phone = "", email = "", status = "", key = "";
            try {
                JSONObject dataJson = new JSONObject(data);
                phone = dataJson.getString("phone");
                email = dataJson.getString("email");
                status = dataJson.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //03-09 10:36:22.452: E/roll call data(24010): {'phone':8888888888,'email':ram@gmail.com,'status':Accepted}

            //03-09 10:36:22.465: E/jasonn(24010): {"":{"status":"Accepted","color":"red"}}

            sharedPreferences = getSharedPreferences("CIBS", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            JSONObject contactsDeJSON = new JSONObject();
            JSONObject contactJSON = new JSONObject();
            try {
                contactsDeJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
                Log.e("jsonndata", contactsDeJSON + "");
                @SuppressWarnings("rawtypes")
                Iterator it = contactsDeJSON.keys();
                while (it.hasNext()) {
                    key = it.next().toString();
                    Log.e("keysss", "hjh" + key);
                    contactJSON = new JSONObject(contactsDeJSON.getString(key));
                    String phone1 = contactJSON.getString("phone");
                    String email1 = contactJSON.getString("email");
                    if (phone1.equalsIgnoreCase(phone) || email1.equalsIgnoreCase(email)) break;
                }
                contactJSON.put("status", status);
                if (status.equalsIgnoreCase("reached") || status.equalsIgnoreCase("safe"))
                    contactJSON.put("color", "green");
                else if (status.equalsIgnoreCase("inroute"))
                    contactJSON.put("color", "yellow");
                else
                    contactJSON.put("color", "red");
                contactsDeJSON = contactsDeJSON.put(key, contactJSON);
                editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());
                editor.commit();
                if (MainActivity.instance != null) {
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RollCallFragmentDeact.instance != null)
                                RollCallFragmentDeact.instance.loadContacts();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (message.equalsIgnoreCase("ChangeUserLocation")) {
            // String data = intent.getExtras().getString("rollcallData");
            // String data = "";


            String data=null;
            try {
                data = obj.getString("rollcallData");

            }
            catch (Exception e)
            {

            }

            Log.e("location daat", data);
            String phone = "", email = "", status = "", key = "";
            try {
                JSONObject dataJson = new JSONObject(data);
                phone = dataJson.getString("phone");
                email = dataJson.getString("email");
                status = dataJson.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sharedPreferences = getApplicationContext().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            JSONObject contactsDeJSON = new JSONObject();
            JSONObject contactJSON = new JSONObject();
            try {
                contactsDeJSON = new JSONObject(sharedPreferences.getString("rollcallDeContactsJSON", "{}"));
                @SuppressWarnings("rawtypes")
                Iterator it = contactsDeJSON.keys();
                while (it.hasNext()) {
                    key = it.next().toString();
                    contactJSON = new JSONObject(contactsDeJSON.getString(key));
                    String phone1 = contactJSON.getString("phone");
                    String email1 = contactJSON.getString("email");
                    if (phone1.equalsIgnoreCase(phone) || email1.equalsIgnoreCase(email)) break;
                }
                contactJSON.put("location", status);
                contactsDeJSON = contactsDeJSON.put(key, contactJSON);
                editor.putString("rollcallDeContactsJSON", contactsDeJSON.toString());
                editor.commit();
                if (MainActivity.instance != null) {
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RollCallFragmentDeact.instance != null)
                                RollCallFragmentDeact.instance.loadContacts();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (message.equalsIgnoreCase("ChangeLocation")) {
            // String data = intent.getExtras().getString("rollcallData");
            // String data = "";
            String data=null;
            try {
                data = obj.getString("rollcallData");

            }
            catch (Exception e)
            {

            }

            editor.putString("rollCallData", data);
            editor.commit();
            try {
                if (MainActivity.instance != null) {
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RollCallFragmentAccept.instance != null)
                                RollCallFragmentAccept.instance.setLocation();
                        }
                    });
                }
            } catch (Exception e) {
            }
        } else if (message.equalsIgnoreCase("DeactivateRollcall")) {
            try {
                if (MainActivity.instance != null) {
                    MainActivity.instance.runOnUiThread(new Runnable() {
                        public void run() {
                            if (RollCallFragmentAccept.instance != null)
                                RollCallFragmentAccept.instance.deactivateRollcall();
                            editor.putBoolean("rollCallRequestReceived", false);
                            editor.commit();
                            if (MainActivity.instance.alertDialog != null)
                                MainActivity.instance.alertDialog.dismiss();
                        }
                    });
                }
            } catch (Exception e) {
            }
        } else
            generateNotification(getApplicationContext(), message);
    }
    private static void generateNotification(Context context, String message) {

        try {
            JSONObject obj=new JSONObject(message);
            message=obj.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            if (message.contains("registering")) {
                message = "Thank you for registering with us";
            } else
            {
                message = "User Updated";
            }
        }
        //{message=Thank you for registering with us}

        int icon = R.drawable.app_icon;
        long when = System.currentTimeMillis();
        Intent notificationIntent = new Intent(context, FirstActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        String title = context.getString(R.string.app_name);
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(context)
                .setTicker(message)
                .setSmallIcon(icon)
                .setWhen(when)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(intent).build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);
    }
}
