package com.visionarytechsolutions.cibs1Pro;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.visionarytechsolutions.cibs1Pro.R;
import com.visionarytechsolutions.cibs1Pro.adapter.ContactItemAdapter;
import com.visionarytechsolutions.cibs1Pro.service.ShakeService;
import com.visionarytechsolutions.cibs1Pro.utils.AlertActions;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ContactsListFragment extends Fragment {
	
	ListView contactListView;
	SharedPreferences sharedPreferences;
	Editor editor;
	ArrayList<String> contactsArrayList = new ArrayList<String>();
	JSONObject contactsJSON = new JSONObject();
	public static ContactsListFragment instance = null;
	RelativeLayout mainLayout;
	Button btnContinue,btnAddManually,fromContactBtn;
	boolean fromFirstActivity = false;
	View rootView;
	Button btnInfo;
	AlertActions alertActions;
	public ContactsListFragment(boolean fromFirstActivity){
		this.fromFirstActivity = fromFirstActivity;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();

          instance = this;        
          rootView = inflater.inflate(R.layout.fragment_contacts_list, container, false);
          alertActions = new AlertActions(getActivity().getApplicationContext()); 
          contactListView = (ListView) rootView.findViewById(R.id.contactList);
		mainLayout = (RelativeLayout) rootView.findViewById(R.id.contacts_mainLayout);
		btnContinue = (Button) rootView.findViewById(R.id.btnContinue);
		btnAddManually = (Button) rootView.findViewById(R.id.btnAddManually);
		fromContactBtn = (Button) rootView.findViewById(R.id.fromContactBtn);
		btnInfo = (Button) rootView.findViewById(R.id.btnInfo);
		
		sharedPreferences = getActivity().getSharedPreferences("CIBS", Context.MODE_PRIVATE);
          editor = sharedPreferences.edit();
          Log.e("asas","k"+sharedPreferences.getString("detailsfinals111", ""));

         		
		if(!fromFirstActivity)
			btnContinue.setVisibility(View.GONE);
		
		try {
			contactsJSON = new JSONObject(sharedPreferences.getString("contactsJSON", "{}"));
		} catch (JSONException e) {
			Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
    	@SuppressWarnings("rawtypes")
		Iterator it = contactsJSON.keys();
        while(it.hasNext())
        	contactsArrayList.add(it.next().toString());
          contactListView.setAdapter(new ContactItemAdapter(contactsJSON, getActivity().getApplicationContext(), ContactsListFragment.this, contactsArrayList));
        
        contactListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {				
				showContactAlert(position);
			}
		});
        
        btnAddManually.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showContactAlert(-1);
			}
		});
        
        fromContactBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_PICK,Uri.parse("content://contacts"));
				intent.setType(Phone.CONTENT_TYPE);
		        startActivityForResult(intent, 1); 
			}
		});       
        btnContinue.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				int contactsCount = contactsArrayList.size();
				if(contactsCount>=1){
					editor.putBoolean("isContactsListCompleted", true);
			    	editor.commit();
			    	
			    	/*if(sharedPreferences.getBoolean("QuickMeeting", false))
			    	{
			    	
					startActivity(new Intent(getActivity(), MainActivity.class));
			    	}*/
			    	 if(fromFirstActivity)
			    	 {
			    		FirstActivity.instance.setScreen();
			    		
			    		if(sharedPreferences.getBoolean("Trip", false)||sharedPreferences.getBoolean("OnceDay", false)||sharedPreferences.getBoolean("QuickMeeting", false))
			          {        
			          	JSONObject timerJSON = new JSONObject();
			          	JSONObject radiusJSON = new JSONObject();         	         	         	
						try {						
							editor.putBoolean("isShakeChecked", true);
							editor.commit();
							getActivity().startService(new Intent(getActivity(), ShakeService.class));

							if(sharedPreferences.getBoolean("OnceDay", false))
							{
								timerJSON.put("value", 24*60*60);
								//timerJSON.put("value", 10);
								radiusJSON.put("position",2);
								radiusJSON.put("value", 1000);	
							}
							else if(sharedPreferences.getBoolean("Trip", false))
							{
							timerJSON.put("value", 60*60);
							//timerJSON.put("value", 10);
							radiusJSON.put("position",2);
							radiusJSON.put("value", 1000);
							}
							else if(sharedPreferences.getBoolean("QuickMeeting", false))
							{
								timerJSON.put("value", 30*60);
								//timerJSON.put("value", 10);
								radiusJSON.put("position",2);
								radiusJSON.put("value", 1000);	
							}							
							editor.putString("checkinTimer", timerJSON.toString());
							editor.putString("safeLocationRadius", radiusJSON.toString());
							editor.putBoolean("recordTypeVideo", false);
							editor.putBoolean("recordTypeAudio", true);
							editor.putBoolean("isTextChecked", true);
							editor.putBoolean("isCallChecked", true);
							editor.putBoolean("isVibrateChecked", true);
							editor.putBoolean("isRingtoneChecked", true);
							editor.putBoolean("isEmailChecked", true);							
							editor.commit();
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}        	
			          	boolean notificationAlertSet = sharedPreferences.getBoolean("notificationAlertSet", false);
						boolean alertSet = sharedPreferences.getBoolean("alertSet", false);
						if((!notificationAlertSet && !alertSet) || !alertSet)
							alertActions.activateFakeCallAlert(false);													          }		
			    		
			    	 }			 
				}else{
					Toast.makeText(getActivity().getApplicationContext(), "Please add atleast one contact", Toast.LENGTH_LONG).show();
				}
			}
		});
        
        btnInfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog();
			}
		});
         
        return rootView;
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			mainLayout.setBackgroundResource(R.drawable.background_land);
		else
			mainLayout.setBackgroundResource(R.drawable.background_port);
	}
	@SuppressLint("InflateParams") public void showContactAlert(final int position){
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());		
		alert.setTitle("Contact Information");
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          View contactAlert = inflater.inflate(R.layout.contact_form, null);
		final EditText fullNameEdt = (EditText) contactAlert.findViewById(R.id.contact_fullNameEdt);
		final EditText emailAddressEdt = (EditText) contactAlert.findViewById(R.id.contact_emailEdt);
		final EditText PhoneNumberEdt = (EditText) contactAlert.findViewById(R.id.contact_phoneNumberEdt);
		alert.setView(contactAlert);
		
		if(position>=0){
			fullNameEdt.setText(contactsArrayList.get(position));
			fullNameEdt.setEnabled(false);
			JSONObject contactJSON;
			try {
				contactJSON = new JSONObject(contactsJSON.getString(contactsArrayList.get(position)));
				emailAddressEdt.setText(contactJSON.getString("email"));
				PhoneNumberEdt.setText(contactJSON.getString("phone"));
			} catch (JSONException e) {
				Log.d("Contact:", e.toString());
			}			
		}

		alert.setNeutralButton("Save Contact", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String fullName = fullNameEdt.getText().toString();
				String emailAddress = emailAddressEdt.getText().toString();
				String PhoneNumber = PhoneNumberEdt.getText().toString();
				if(PhoneNumber.length()<10){
					Toast.makeText(getActivity(), "Phone Number should be above 10 digits", Toast.LENGTH_LONG).show();
				}else{
					if(fullName.equalsIgnoreCase("")){
						Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();					
					}
					else if(emailAddress.equalsIgnoreCase("") && PhoneNumber.equalsIgnoreCase("")){
						Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();
					}
					else{
			           try {
			        	   JSONObject contactJSON = new JSONObject();
			        	   contactJSON.put("email", emailAddress);
			        	   contactJSON.put("phone", PhoneNumber);
			        	   if(contactsJSON.length()==0)
			        		   editor.putString("firstContact", fullName);
			        	   contactsJSON = contactsJSON.put(fullName, contactJSON);
						   editor.putString("contactsJSON", contactsJSON.toString());
						   editor.commit();
			           } catch (JSONException e) {
							Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			           }
			           updateContactList();
			           if(position == -1)
			        	   Toast.makeText(getActivity().getApplicationContext(), "Contact Added Successfully", Toast.LENGTH_LONG).show();			        
						dialog.cancel();
					}
				}
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		alert.setCancelable(false);
		alert.show();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	      if (data != null) {
	          Uri uri = data.getData();
	          if (uri != null) {
	              Cursor c = null;
	              try {
	                  c = getActivity().getContentResolver().query(uri, new String[]{ 
	                              ContactsContract.CommonDataKinds.Phone.NUMBER,ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
	                          null, null, null);         

	                  if (c != null && c.moveToFirst()) {
	                      String PhoneNumber = c.getString(0);
	                      String fullName = c.getString(1);
	                      String emailAddress= "";                      
	                      
	                      Cursor emails = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,
	                    		  ContactsContract.CommonDataKinds.Email.CONTACT_ID+ " = " + c.getString(2), null, null);
	                      while (emails.moveToNext()) {
                              emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                           }                           
                          emails.close();	                      
	                      
	                      PhoneNumber= PhoneNumber.replaceAll("\\s+","");
	                      PhoneNumber= PhoneNumber.replaceAll("\\(","");
	                      PhoneNumber= PhoneNumber.replaceAll("\\)","");
	                      PhoneNumber= PhoneNumber.replaceAll("\\-","");
	                      if(PhoneNumber.length()<10){
	      					Toast.makeText(getActivity(), "Phone Number should be above 10 digits", Toast.LENGTH_LONG).show();
		      			  }else{
		                      if(fullName.equalsIgnoreCase("")){
		      					  Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();					
		      				  }
		      				  else if(emailAddress.equalsIgnoreCase("") && PhoneNumber.equalsIgnoreCase("")){
		      					  Toast.makeText(getActivity().getApplicationContext(), "Please Enter atleast Name & Email/Phone", Toast.LENGTH_LONG).show();
		      				  }else{
		     		            try {
		     		        	   JSONObject contactJSON = new JSONObject();
		     		        	   contactJSON.put("email", emailAddress);
		     		        	   PhoneNumber=PhoneNumber.substring(PhoneNumber.length()-10);
		     		        	   contactJSON.put("phone", PhoneNumber);
		     		        	   if(contactsJSON.length()==0)
		     		        		   editor.putString("firstContact", fullName);
		     		        	   contactsJSON = contactsJSON.put(fullName, contactJSON);
		     					   editor.putString("contactsJSON", contactsJSON.toString());
		     					   editor.commit();
		     					} catch (JSONException e) {
		     						Toast.makeText(getActivity().getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		     					}
		     		            updateContactList();
		     		            Toast.makeText(getActivity().getApplicationContext(), "Contact Added Successfully", Toast.LENGTH_LONG).show();
		      			  }
	     				}	                      
	                  }
	              } finally {
	                  if (c != null) {
	                      c.close();
	                  }
	              }
	          }
	      }
	  }
	
	public void removeContactInfo(int position){
		String firstContact = sharedPreferences.getString("firstContact", "");
		contactsJSON.remove(contactsArrayList.get(position));
		if(firstContact.equalsIgnoreCase(contactsArrayList.get(position))){
			contactsArrayList.remove(position);
			if(contactsArrayList.size()>0)
				editor.putString("firstContact", contactsArrayList.get(0));
		}
    	editor.putString("contactsJSON", contactsJSON.toString());
		editor.commit();
		updateContactList();
    	Toast.makeText(getActivity().getApplicationContext(), "Contact removed Successfully", Toast.LENGTH_LONG).show();
    }
	
	public void updateContactList(){
		@SuppressWarnings("rawtypes")
		Iterator it = contactsJSON.keys();
    	    contactsArrayList.removeAll(contactsArrayList);
        while(it.hasNext())
        	contactsArrayList.add(it.next().toString());
        contactListView.setAdapter(new ContactItemAdapter(contactsJSON, getActivity().getApplicationContext(), ContactsListFragment.this, contactsArrayList));    	
	}
	void showDialog()
	{
		new AlertDialog.Builder(getActivity())
        .setTitle("CIBS PRO")
        .setMessage("This page allows you to setup Contacts that will receive notification if you fail to check in at your set interval or if an alert is triggered.  Please notify users that they will be part of your Safety Network, so they will be prepared to react when needed.  You can add users manually or from your contact list.  Emergency contacts will receive the following based on your setup; 1. CIBS Selfie or video, 2. Voice or Video recording, 3. Last Check in Location and Current missed Check In location")
        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
        })
        .show();	
	}
}
